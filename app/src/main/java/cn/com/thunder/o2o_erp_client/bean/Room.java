package cn.com.thunder.o2o_erp_client.bean;

/**
 * @author qiuyanlong(email:276644135@qq.com)
 * @date 2017-03-20 15:42
 * @package cn.com.thunder.o2o_erp_client.bean
 */
public class Room {
    public String timeInterval; //时段
    public String timeLength; //时长
    public String timeUnitPrice; //时间单价
    public String timeSubtotal; //时段小计
} 