package cn.com.thunder.o2o_erp_client.bean;

/**
 * @author qiuyanlong(email:276644135@qq.com)
 * @date 2017-05-19 16:37
 * @package cn.com.thunder.o2o_erp_client.bean
 */
public class LKStb {
    public String address;  // mac地址
    public String ip;      //ip地址
    public int state;  //0 关闭态 , 1 开启态 , 2 关机或重启态

    public LKStb() {
        super();
    }

    public LKStb(String address, String ip,int state) {
        super();
        this.address = address;
        this.ip = ip;
        this.state = state;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int hashCode() {  //复写hashCode()
        return address.hashCode();
    }

    public boolean equals(Object obj) {  //复写equals

        if (this == obj) return true;

        if (!(obj instanceof LKStb)) //instanceof运算符是用来在运行时指出对象是否是特定类的一个实例,返回值是一个布尔类型。这里是判断obj对象是否是Person类的一个实例。
            throw new ClassCastException("类型错误");  //输入类型错误

        LKStb stb = (LKStb) obj;//强制转换
        return this.address.equals(stb.address); //address一致为同一元素
    }

    @Override
    public String toString() {
        return "LKStb{" +
                "address='" + address + '\'' +
                ", ip='" + ip + '\'' +
                ", state=" + state +
                '}';
    }
}