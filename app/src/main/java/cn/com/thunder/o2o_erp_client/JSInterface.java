package cn.com.thunder.o2o_erp_client;

import android.content.Context;
import android.webkit.JavascriptInterface;

import cn.com.thunder.o2o_erp_client.utils.SysUtils;
import cn.com.thunder.o2o_erp_client.utils.ToastUtils;

/**
 * 桥接类，供Javascript调用
 */

public class JSInterface {
    private static final String TAG = "JSInterface";
    private ThunderApplication app;
    private Context mContext;

    private JSMessage jsMessage;


    public interface JSMessage {
        void showSecondScreen(boolean display, String content);

        void printNotes(String data);

        void openBox();

        String getSTBState();

        void setSTBState(String content);
    }

    public JSInterface(Context context) {
        mContext = context;
    }


    public JSInterface(Context context, JSMessage jsMessage) {
        mContext = context;
        this.jsMessage = jsMessage;
        app = ThunderApplication.getApp();
    }

    @JavascriptInterface
    public String test(String data) {
        return "hello " + data;
    }

    /**
     * 开启钱箱
     */
    @JavascriptInterface
    public void openBox() {
        if (jsMessage == null) {
            ToastUtils.popUpToast("页面尚未初始化完毕，暂无法使用外设");
        } else {
            jsMessage.openBox();
        }
    }

    /**
     * 打印小票
     */
    @JavascriptInterface
    public void printNotes(String data) {
        SysUtils.LogE(TAG,"data = " + data);
        if (jsMessage == null) {
            ToastUtils.popUpToast("页面尚未初始化完毕，暂无法使用外设");
        } else if (data == null) {
            ToastUtils.popUpToast("打印数据异常，请检查打印数据是否正确。");
        }else {
            jsMessage.printNotes(data);
        }
    }

    /**
     * 显示分屏内容
     *
     * @param display true 分屏显示指定内容。false 主屏，分屏，显示相同内容
     * @param content  显示的内容地址
     */
    @android.webkit.JavascriptInterface
    public void showSplitScreen(boolean display, String content) {
        if (jsMessage == null) {
            ToastUtils.popUpToast("页面尚未初始化完毕，暂无法使用外设");
        } else {
            jsMessage.showSecondScreen(display, content);
            SysUtils.LogE(TAG, "  Android 方法被JS调用了。 显示分屏内容 ：" + display);
        }
    }


    /**
     * 获取雷客机顶盒信息
     * 此方法暂时无用。以后端数据为准。
     */
    @JavascriptInterface
    public String getSTBState() {
//        if (jsMessage == null) {
//            ToastUtils.popUpToast("页面尚未初始化完毕，暂无法使用外设");
//        }
//        SysUtils.LogE(TAG, "Android 获取雷客机顶盒信息");
//        return jsMessage.getSTBState();
        return "";
    }

    /**
     * 控制指定的机顶盒 (添加雷石机顶盒 字段内容会增加IP 或 STB_ID 字段。通过设置IP 或 STB_ID选择控制对应的机顶盒)
     * 参数 ： Json格式的字符串
     * {
     *      "data": {
     *      "mac": "001122334455",       //mac地址
     *      "command": "056"               //操作类型 (开台：056、续台：057、关台：058、关机：059、重启：060)
     *      "time":  60                   //时长（倒计时开台，一直开台，续台时间）
     *      "type":  1                    //开台类型
     * }
     */
    @JavascriptInterface
    public void setSTBState(String message) {
        if (jsMessage == null) {
            ToastUtils.popUpToast("页面尚未初始化完毕，暂无法使用外设");
        } else {
            SysUtils.LogE(TAG, "Android 控制指定的机顶盒" + message);
            jsMessage.setSTBState(message);
        }
    }

}
