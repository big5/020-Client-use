package cn.com.thunder.o2o_erp_client;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

/**
 * This Activity appears as a dialog. It lists any paired devices and
 * devices detected in the area after discovery. When a device is chosen
 * by the user, the MAC address of the device is sent back to the parent
 * Activity in the result Intent.
 */
public class BluetoothDeviceListActivity extends Activity {

    private static final String TAG = "DeviceListActivity";
    public static String EXTRA_DEVICE_ADDRESS = "device_address";
    private static final String THUNDER_PRINTER = "T802_BT_Printer";

    // Member fields
    private ArrayAdapter<String> mPairedDevicesArrayAdapter;

    private BluetoothAdapter mBtAdapter;

    private LinearLayout layout_listView;
    private ListView pairedListView;
    private TextView hint;
    private ImageView exit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.device_list);

        // Set result CANCELED incase the user backs out
        setResult(Activity.RESULT_CANCELED);
        mPairedDevicesArrayAdapter = new ArrayAdapter<>(this, R.layout.device_name);

        // Get the local Bluetooth adapter
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();

        pairedListView = (ListView) findViewById(R.id.paired_devices);

        pairedListView.setAdapter(mPairedDevicesArrayAdapter);
        pairedListView.setOnItemClickListener(mDeviceClickListener);
        layout_listView = (LinearLayout) findViewById(R.id.layout_listView);
        hint = (TextView) findViewById(R.id.searching_hint);
        hint.setText(getResources().getText(R.string.searching_device).toString());
        exit = (ImageView) findViewById(R.id.exit_search);
        exit.setVisibility(View.GONE);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private BroadcastReceiver mSearchReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "action  name = " + action);
            if (action.equals(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)) {
                //搜索结束跳转到蓝牙界面，并列表显示搜到的蓝牙。
                Log.i(TAG, "   ACTION_DISCOVERY_FINISHED   ");
                if (mPairedDevicesArrayAdapter.getCount() == 0) {
                    hint.setText(getResources().getText(R.string.none_found).toString());
                }else {
                    hint.setText(getResources().getText(R.string.searching_over).toString());
                }
                exit.setVisibility(View.VISIBLE);
            } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {  //发现蓝牙
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // If it's already paired, skip it, because it's been listed already
                //此处需要验证，即使蓝牙设备找到，但是有可能未识别设备名称，有可能会报空指针异常
                if (TextUtils.isEmpty(device.getName()) || TextUtils.isEmpty(device.getAddress())) {
                    Log.d(TAG, "  BluetoothDevice.ACTION_FOUND   found device is return null ! ");
                    return;
                }
                String itemName = device.getName()
                        + " ( " + getResources().getText(device.getBondState() == BluetoothDevice.BOND_BONDED ? R.string.has_paired : R.string.not_paired) +" )"
                        + "\n" + device.getAddress();


                if (device.getName().equals(THUNDER_PRINTER)) {
                    if(layout_listView.getVisibility() == View.INVISIBLE){
                        layout_listView.setVisibility(View.VISIBLE);
                    }
                    mPairedDevicesArrayAdapter.remove(itemName);
                    mPairedDevicesArrayAdapter.add(itemName);
                    pairedListView.setEnabled(true);
                }
            }
        }
    };

    @Override
    protected void onStop() {
        super.onPause();
        // Make sure we're not doing discovery anymore
        if (mBtAdapter != null && mBtAdapter.isDiscovering()) {
            mBtAdapter.cancelDiscovery();
        }
        // Unregister broadcast listeners
        unregisterReceiver(mSearchReceiver);
    }

    @Override
    protected void onStart() {
        super.onResume();
        //本机蓝牙状态广播 , 搜索完毕需要unregister
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_FOUND);                         //发现蓝牙
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);           //搜索结束蓝牙
        registerReceiver(mSearchReceiver, filter);
        doPrinterDiscovery();
    }

    /**
     * 开启蓝牙搜索，查找蓝牙设备
     * Start device discover
     */
    private void doPrinterDiscovery() {
        Log.d(TAG, " doDiscovery() ");
        mPairedDevicesArrayAdapter.clear();
        // Request discover from BluetoothAdapter
        if(mBtAdapter == null){
            mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        }
        mBtAdapter.startDiscovery();
    }

    private void returnToPreviousActivity(String address)
    {
        // Create the result Intent and include the MAC address
        Intent intent = new Intent();
        intent.putExtra(EXTRA_DEVICE_ADDRESS, address);

        // Set result and finish this Activity
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
    // The on-click listener for all devices in the ListViews
    private AdapterView.OnItemClickListener mDeviceClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {
            // Get the device MAC address, which is the last 17 chars in the View
            String info = ((TextView) v).getText().toString();
            String address = info.substring(info.length() - 17);
            returnToPreviousActivity(address);
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return keyCode == KeyEvent.KEYCODE_BACK;
    }
}