package cn.com.thunder.o2o_erp_client.bean;

import java.util.List;

/**
 * @author qiuyanlong(email:276644135@qq.com)
 * @date 2017-03-20 13:13
 * @package cn.com.thunder.o2o_erp_client.bean
 */
public class Bills {

    public int printFrequency; //打印次数
    public String titleType;     //小票类型
    public String flowingBill;  //流水号
    public String orderNumber;  //订单号
    public String originalBill; //原流水号
    public String netReceivables; //网络订单号。
    public String roomName;     //房台
    public String OperationType; //操作类型


    public String openTime;  //开台时间
    public String backTime;  //退单时间
    public String checkoutTime; //结账时间
    public String roomPackage; //房间套餐
    public String minConsumption; //最低消费。


    public List<Room> rooms ; //包房消费信息
    public String roomTotalPay ; //包房消费总计

    public List<Beer> beers ; //酒水消费信息
    public String beerTotalPay ; //包房消费总计

    public String totalPay; //总计
    public String hasPayMoney; //已付
    public String advancePayment;//预付
    public String discount; //优惠
    public String percent; //打折
    public String realIncome; // 实收
    public String shouldIncome; //应收
    public String shouldRefund; //应退
    public String rounding; //抹零
    public String refund; // 找零


    public int isPayOrRefund; // 1、支付方式 ; 2、退款方式
    public int typePayOrRefund; //方式  0、不显示 / 1、微信 / 2、支付宝 / 3、POS  / 4、会员卡  / 5、现金

    public String reMarks; //备注。
    public int autograph; //签名。


}