package cn.com.thunder.o2o_erp_client;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.alipay.euler.andfix.patch.PatchManager;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.cache.CacheMode;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.beta.Beta;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import cn.com.thunder.o2o_erp_client.utils.SysUtils;

/**
 * @author qiuyanlong(email:276644135@qq.com)
 * @date 2017-04-13 16:01
 * @package cn.com.thunder.o2o_erp_client
 */
public class ThunderApplication extends Application{
    private static final String TAG = "Application";
    public static ThunderApplication app;
    public static ThunderApplication getApp() {
        return app;
    }

    public static int mMainThreadId;
    private static SharedPreferences pref;

    public static boolean debugLog = true;

    @Override
    public void onCreate() {
        super.onCreate();
        mMainThreadId = android.os.Process.myTid();
        pref  = getSharedPreferences("ERP_SETTING", Context.MODE_PRIVATE);
        app = this;
        initOkGo();
        CrashHandler crashHandler = CrashHandler.getInstance();
        crashHandler.init(getApplicationContext());

        // 这里实现SDK初始化，appId替换成你的在Bugly平台申请的appId

        // 调试时，将第三个参数改为true
//        Bugly.init(this, "f880283089", true);
    }

    public static SharedPreferences getParamsManager() {
        return pref;
    }

    /**
     * 初始化网络请求框架OkGo
     */
    private void initOkGo(){

        //必须调用初始化
        OkGo.init(this);

        //以下设置的所有参数是全局参数,同样的参数可以在请求的时候再设置一遍,那么对于该请求来讲,请求中的参数会覆盖全局参数
        //好处是全局参数统一,特定请求可以特别定制参数
        try {
            //以下都不是必须的，根据需要自行选择,一般来说只需要 debug,缓存相关,cookie相关的 就可以了
            OkGo.getInstance()

                    // 打开该调试开关,打印级别INFO,并不是异常,是为了显眼,不需要就不要加入该行
                    // 最后的true表示是否打印okgo的内部异常，一般打开方便调试错误
                    .debug("OkGo", Level.INFO, true)

                    //如果使用默认的 60秒,以下三行也不需要传
                    .setConnectTimeout(OkGo.DEFAULT_MILLISECONDS)  //全局的连接超时时间
                    .setReadTimeOut(OkGo.DEFAULT_MILLISECONDS)     //全局的读取超时时间
                    .setWriteTimeOut(OkGo.DEFAULT_MILLISECONDS)    //全局的写入超时时间

                    //可以全局统一设置缓存模式,默认是不使用缓存,可以不传,具体其他模式看 github 介绍 https://github.com/jeasonlzy/
                    .setCacheMode(CacheMode.NO_CACHE)

                    //可以全局统一设置超时重连次数,默认为三次,那么最差的情况会请求4次(一次原始请求,三次重连请求),不需要可以设置为0
                    .setRetryCount(3);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        // you must install multiDex whatever tinker is installed!
        MultiDex.install(base);


        // 安装tinker
        Beta.installTinker();
    }

} 