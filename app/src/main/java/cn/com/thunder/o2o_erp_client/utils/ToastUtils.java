package cn.com.thunder.o2o_erp_client.utils;

import android.widget.Toast;

import cn.com.thunder.o2o_erp_client.ThunderApplication;


public class ToastUtils {
    private static ThunderApplication app = ThunderApplication.getApp();
    private static String lastToastString = "";
    private static long lastTimeMillis = 0L;
    private static final int SHORT_DELAY = 2000; // 2 seconds

    private ToastUtils() {
    }

    public static void popUpToast(String text) {
        popUpToast(text, Toast.LENGTH_SHORT);
    }

    public static void popUpToast(int resId) {
        popUpToast(resId, Toast.LENGTH_SHORT);
    }

    public static void popUpToast(String text, int time) {
        if (!isSameMsg(text, time)) {

            Toast.makeText(app, text, time).show();
        }
    }

    public static void popUpToast(int resId, int time) {
        if (!isSameMsg(resId, time)) {
            Toast.makeText(app, resId, time).show();
        }
    }

    /**
     * 吐司的内容不相同时，保存吐司的内容和时间。 保存数据用来与下次吐司的内容和时间进行比较
     */
    private static boolean isSameMsg(String text, int time) {
        if (lastTimeMillis > System.currentTimeMillis() - SHORT_DELAY) { // 吐司未消失
            // Toast未消失时，判断是否是同一内容
            if (lastToastString.equals(text)) { // 吐司内容一致
                return true;
            }
        }
        lastTimeMillis = System.currentTimeMillis();
        lastToastString = text;
        return false;
    }

    private static boolean isSameMsg(int resId, int time) {
        String tempStr = app.getResources().getString(resId);
        if (lastTimeMillis > System.currentTimeMillis() - SHORT_DELAY) { // 吐司未消失
            // Toast未消失时，判断是否是同一内容
            if (lastToastString.equals(tempStr)) { // 吐司内容一致
                return true;
            }
        }
        lastTimeMillis = System.currentTimeMillis();
        lastToastString = tempStr;
        return false;
    }
}
