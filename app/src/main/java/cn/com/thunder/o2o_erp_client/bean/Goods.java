package cn.com.thunder.o2o_erp_client.bean;

/**
 * @author qiuyanlong(email:276644135@qq.com)
 * @date 2017-02-10 11:04
 * @package com.printer.sdk.android.usb.bean
 * 打印小票数据内容
 */
public class Goods {
    private String id;
    private String name;
    private int count;
    private double price;

    public Goods() {

    }

    public Goods(String name, int count, double price) {
        this.name = name;
        this.count = count;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Goods{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", count=" + count +
                ", price=" + price +
                '}';
    }
}