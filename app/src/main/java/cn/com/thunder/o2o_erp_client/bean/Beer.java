package cn.com.thunder.o2o_erp_client.bean;

/**
 * @author qiuyanlong(email:276644135@qq.com)
 * @date 2017-03-20 15:43
 * @package cn.com.thunder.o2o_erp_client.bean
 */
public class Beer {

    public String beerName; //酒水
    public String beerUnitPrice; //酒水单价
    public String beerCount; //酒水数量
    public String beerSubtotal; //酒水小计
} 