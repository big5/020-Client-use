package cn.com.thunder.o2o_erp_client.DownLoad;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.FileCallback;

import java.io.File;
import java.net.URL;

import cn.com.thunder.o2o_erp_client.Constant;
import okhttp3.Call;
import okhttp3.Response;

/**
 * @author qiuyanlong(email:276644135@qq.com)
 * @date 2017-04-27 09:33
 * @package cn.com.thunder.o2o_erp_client.DownLoad
 */
public class DownLoadFile  {

    private static final String TAG = "DownLoadFile";

    private Context mContent;
    private URL url;

    public DownLoadFile(Context mContent , URL url) {
        this.mContent = mContent;
        this.url = url;
    }

    /**
     * 下载新版本
     */
    private void downLoadNewApk(String downLoadUrl) {
        String path = Environment.getExternalStorageDirectory().getPath() + File.separator + Constant.APK_PATH;
        final File apkfile = new File(path + "/erp.apk");
        if (apkfile.exists()) {
            apkfile.delete();
        }
        Log.d(TAG, " apk path = " + path);
        OkGo.get(downLoadUrl)//
                .tag(this)//
                .execute(new FileCallback(path, "erp.apk") {  //文件下载时，可以指定下载的文件目录和文件名
                    @Override
                    public void onSuccess(File file, Call call, Response response) {

                    }

                    @Override
                    public void downloadProgress(long currentSize, long totalSize, float progress, long networkSpeed) {
                        //这里回调下载进度(该回调在主线程,可以直接更新ui)
                        Log.d(TAG, "progress = " + progress);
                    }
                });
    }

    public void cancelDownLoad(){
        //根据 Tag 取消请求
        OkGo.getInstance().cancelTag(this);
    }
}