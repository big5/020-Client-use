package cn.com.thunder.o2o_erp_client.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;

import cn.com.thunder.o2o_erp_client.R;
import cn.com.thunder.o2o_erp_client.utils.SysUtils;


/**
 * BaseDialog  (Dialog基类)
 */
public class BaseDialog extends Dialog {
    private int dialogId;

    protected BaseDialog(Context context) {
        super(context, R.style.BaseDialog);
    }

    protected BaseDialog(Context context, int style) {
        super(context, style);
    }

    protected BaseDialog(Context context, boolean cancelable) {
        super(context, R.style.BaseDialog);
        setCancelable(cancelable);
    }

    protected BaseDialog(Context context, boolean cancelable,
                         OnCancelListener cancelListener) {
        super(context, R.style.BaseDialog);
        setCancelable(cancelable);
        setOnCancelListener(cancelListener);
    }

    public void setDialogView(int layoutResID, float ratio) {
        View view = LayoutInflater.from(getContext())
                .inflate(layoutResID, null);
        WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        params.width = (int) (SysUtils.getScreenSize(getContext())[0] * ratio);
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        setContentView(view, params);
    }

    public void setDialogView(View view, float ratio) {
        WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        params.width = (int) (SysUtils.getScreenSize(getContext())[0] * ratio);
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        setContentView(view, params);
    }

    public void setLoginDialogView(View view) {
        WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        params.width = SysUtils.getScreenSize(getContext())[0];
        params.height = SysUtils.getScreenSize(getContext())[1];
        setContentView(view, params);
    }

    public void setDialogView(int layoutResID) {
        setDialogView(layoutResID, 0.8f);
    }

    public void setDialogView(View view) {
        setDialogView(view, 0.8f);
    }

    public void setDialogId(int dialogId) {
        this.dialogId = dialogId;
    }

    public int getDialogId() {
        return dialogId;
    }
} 