package cn.com.thunder.o2o_erp_client;

/**
 * @author qiuyanlong(email:276644135@qq.com)
 * @date 2017-04-21 11:19
 * @package cn.com.thunder.o2o_erp_client
 */
public class Constant {
    public static final String APK_PATH = "ERP_CLIENT/APK";
    public static final String LK_OPEN = "056";
    public static final String LK_CLOSE = "058";
    public static final String LK_KEEP = "057";
    public static final String LK_SHUT_DOWN = "059";
    public static final String LK_REBOOT = "060";

    public static final String LK_URL_HEADER = "http://";
    public static final String LK_URL_FOOT = "/encrypt_operation?";
} 