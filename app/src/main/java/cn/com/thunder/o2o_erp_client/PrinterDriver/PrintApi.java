package cn.com.thunder.o2o_erp_client.PrinterDriver;

import android.text.TextUtils;
import android.util.Log;

import com.iprt.xzc_pc.android_print_sdk.BluetoothPrinter;

import cn.com.thunder.o2o_erp_client.R;
import cn.com.thunder.o2o_erp_client.ThunderApplication;
import cn.com.thunder.o2o_erp_client.bean.Bills;


/**
 * @author qiuyanlong(email:276644135@qq.com)
 * @date 2017-02-10 10:13
 * @package com.printer.sdk.android.usb.util
 */
public class PrintApi {
    private static final String TAG = "PrintApi";
    private static ThunderApplication app = ThunderApplication.getApp();
    private static final String SPACE = " ";
    private static final String LINE = "-";
    private static final String NEW_LINE = "\n";

    private static final int lineSpaceTotal = 46;
    //    public static String encodeType = "GBK";  //编码方式
//    public static boolean is_58mm = true;     //纸宽    true 58mm   /   false  80mm
//    public static boolean is_thermal = true;  //打印机类型  true 热敏  / false  针打（暂不支持）
    private static BluetoothPrinter mPrinter;

    public static void setPrinter(BluetoothPrinter mPrinter) {
        PrintApi.mPrinter = mPrinter;
    }

    /**
     * 打印头部内容 ：打印次数，小票类型，流水号，订单号，原流水号，网络订单号，房台，操作类型
     * @param bill
     * @return
     */
    private static boolean printHead(Bills bill) {

        StringBuilder sb = new StringBuilder();

        /*************   显示打印次数   *********/
        if (bill.printFrequency != 0) {
            sb.append(getString(R.string.print_times)).append(bill.printFrequency);
            sb.append(NEW_LINE);
            if (mPrinter.printText(sb.toString()) == -1) {
                return false;
            }
        }


        sb.delete(0, sb.length());

        /*************  显示打单类型    *********/
        //文字居中
        mPrinter.setPrinter(BluetoothPrinter.COMM_ALIGN, BluetoothPrinter.COMM_ALIGN_CENTER);
        // 字号横向纵向扩大一倍
        mPrinter.setCharacterMultiple(1, 1);
        if (mPrinter.printText(bill.titleType + NEW_LINE) == -1) {
            return false;
        }



        /*************  显示  流水号，订单号，原流水号，网络订单号  *********/
        // 字号使用默认
        mPrinter.setPrinter(BluetoothPrinter.COMM_ALIGN, BluetoothPrinter.COMM_ALIGN_CENTER);
        mPrinter.setCharacterMultiple(0, 0);
        if (!TextUtils.isEmpty(bill.flowingBill)) {
            if (mPrinter.printText(getString(R.string.flowing_bill)+bill.flowingBill + NEW_LINE) == -1) {
                return false;
            }
        }
        if (!TextUtils.isEmpty(bill.orderNumber)) {
            if (mPrinter.printText(getString(R.string.flowing_bill)+bill.orderNumber + NEW_LINE) == -1) {
                return false;
            }
        }
        if (!TextUtils.isEmpty(bill.originalBill)) {
            if (mPrinter.printText(getString(R.string.original_bill)+bill.originalBill + NEW_LINE) == -1) {
                return false;
            }
        }

        if (!TextUtils.isEmpty(bill.netReceivables)) {
            if (mPrinter.printText(getString(R.string.netReceivables)+bill.netReceivables + NEW_LINE) == -1) {
                return false;
            }
        }

        /*************  显示房台   操作类型    *********/
        mPrinter.setPrinter(BluetoothPrinter.COMM_ALIGN, BluetoothPrinter.COMM_ALIGN_LEFT);
        if (!TextUtils.isEmpty(bill.roomName)) {

            if (!TextUtils.isEmpty(bill.OperationType)) {
                String out = getString(R.string.room_name) + bill.roomName +
                        getString(R.string.operation_Type) + bill.OperationType;
                int space = lineSpaceTotal - getStringLength(out);
                if (space > 0) {
                    sb.append(getString(R.string.room_name)).append(bill.roomName);
                    for (int i = 0; i < space; i++) {
                        sb.append(SPACE);
                    }
                    sb.append(getString(R.string.operation_Type)).append(bill.OperationType);
                    sb.append(NEW_LINE);
                } else {
                    sb.append(getString(R.string.room_name)).append(bill.roomName).append(NEW_LINE);
                    sb.append(getString(R.string.operation_Type)).append(bill.OperationType).append(NEW_LINE);
                }
            } else {
                sb.append(getString(R.string.room_name)).append(bill.roomName).append(NEW_LINE);
            }
        } else {
            if (!TextUtils.isEmpty(bill.OperationType)) {
                sb.append(getString(R.string.operation_Type)).append(bill.OperationType).append(NEW_LINE);
            }
        }
        /*************  显示分割线  ————————————————    *********/
        for (int i = 0; i < lineSpaceTotal; i++) {
            sb.append(LINE);
        }
        sb.append(NEW_LINE);


        if (mPrinter.printText(sb.toString()) == -1) {
            Log.e(TAG, "  printerBill  printConsumptionTime  print test false ");
            return false;
        }else {
            Log.i(TAG, "  printConsumptionTime  NO data need print ");
            return true;
        }
    }


    /**
     * 打印消费内容：开台时间，退单时间，结账时间，房间套餐，最低消费
     * @param bill
     * @return
     */
    private static boolean printConsumptionTime(Bills bill) {
        boolean show = false;
        StringBuilder sb = new StringBuilder();

        /*************   显示开台时间   *********/
        if (!TextUtils.isEmpty(bill.openTime)) {
            show = true;
            sb.append(getString(R.string.open_time)).append(bill.openTime);
            sb.append(NEW_LINE);
        }

        /*************   显示退单时间   *********/
        if (!TextUtils.isEmpty(bill.backTime)) {
            show = true;
            sb.append(getString(R.string.back_time)).append(bill.backTime);
            sb.append(NEW_LINE);
        }

        /*************   显示结账时间    *********/
        if (!TextUtils.isEmpty(bill.checkoutTime)) {
            show = true;
            sb.append(getString(R.string.checkout_time)).append(bill.checkoutTime);
            sb.append(NEW_LINE);
        }

        /*************   显示房间套餐    *********/
        if (!TextUtils.isEmpty(bill.roomPackage)) {
            show = true;
            sb.append(getString(R.string.room_package)).append(bill.roomPackage);
            sb.append(NEW_LINE);
        }

        /*************   显示最低消费    *********/
        if (!TextUtils.isEmpty(bill.minConsumption)) {
            show = true;
            sb.append(getString(R.string.min_Consumption)).append(bill.minConsumption);
            sb.append(NEW_LINE);
        }
        /*************  显示分割线  ————————————————    *********/
        for (int i = 0; i < lineSpaceTotal; i++) {
            sb.append(LINE);
        }
        sb.append(NEW_LINE);
        if(show){
            if (mPrinter.printText(sb.toString()) == -1) {
                Log.e(TAG, "  printerBill  printConsumptionTime  print test false ");
                return false;
            }
        }else{
            Log.i(TAG, "  printConsumptionTime  NO data need print ");
        }
        return true;
    }

    /**
     * 打印消费时间详情：总字符数  =  文字 34 + 间隔 12
     * 文字 =  时段(16),时长(4),单价(6),小计/元(8)
     * 间隔 =  3 * 4 =12
     * @param bill
     * @return
     */
    private static boolean printTimeDetail(Bills bill) {
        if (bill.rooms.size() == 0) {
            Log.i(TAG, "  printTimeDetail  NO data need print ");
            return true;
        }
        StringBuilder sb = new StringBuilder();

        /*************  显示消费时间列名  *********/
        if (bill.rooms != null && bill.rooms.size() > 0) {
            sb.append(getString(R.string.time_Interval));
            int space = 16 - getStringLength(getString(R.string.time_Interval));
            for (int i = 0; i < space + 4; i++) {
                sb.append(SPACE);
            }

            sb.append(getString(R.string.time_Length));
            space = 4 - getStringLength(getString(R.string.time_Length));
            for (int i = 0; i < space + 4; i++) {
                sb.append(SPACE);
            }

            sb.append(getString(R.string.time_UnitPrice));
            space = 6 - getStringLength(getString(R.string.time_UnitPrice));
            for (int i = 0; i < space + 4; i++) {
                sb.append(SPACE);
            }

            sb.append(getString(R.string.time_Subtotal));
            space = 8 - getStringLength(getString(R.string.time_Subtotal));
            for (int i = 0; i < space; i++) {
                sb.append(SPACE);
            }
            sb.append(NEW_LINE);

            for (int i = 0; i < bill.rooms.size(); i++) {

                /*************  显示包房消费信息数据部分 *********/
                space = 16 - getStringLength(bill.rooms.get(i).timeInterval);
                sb.append(bill.rooms.get(i).timeInterval);
                for (int p = 0; p < space + 4; p++) {
                    sb.append(SPACE);
                }
                space = 4 - getStringLength(bill.rooms.get(i).timeLength);
                sb.append(bill.rooms.get(i).timeLength);
                for (int p = 0; p < space + 4; p++) {
                    sb.append(SPACE);
                }
                space = 6 - getStringLength(bill.rooms.get(i).timeUnitPrice);
                sb.append(bill.rooms.get(i).timeUnitPrice);
                for (int p = 0; p < space + 4; p++) {
                    sb.append(SPACE);
                }
                space = 8 - getStringLength(bill.rooms.get(i).timeSubtotal);
                sb.append(bill.rooms.get(i).timeSubtotal);
                for (int p = 0; p < space; p++) {
                    sb.append(SPACE);
                }
                sb.append(NEW_LINE);
            }
        }

        /*************  显示 房间总计：    *********/
        if (!TextUtils.isEmpty(bill.roomTotalPay)) {
            int space = lineSpaceTotal - getStringLength(getString(R.string.totalPay)) - getStringLength(bill.roomTotalPay);
            for (int i = 0; i < space; i++) {
                sb.append(SPACE);
            }
            sb.append(getString(R.string.hasPayMoney));
            sb.append(bill.totalPay);
            sb.append(NEW_LINE);
        }

        /*************  显示分割线  ————————————————    *********/
        for (int i = 0; i < lineSpaceTotal; i++) {
            sb.append(LINE);
        }
        sb.append(NEW_LINE);

        if (mPrinter.printText(sb.toString()) == -1) {
            Log.e(TAG, "  printerBill  printTimeDetail  print test false ");
            return false;
        }
        return true;

    }

    /**
     * 打印酒水详情 ：总字符数  =  文字 34 + 间隔 12
     * 文字 =  酒水(16),数量(4),单价(6),小计/元(8)
     * 间隔 =  3 * 4 =12
     * @param bill
     * @return
     */
    private static boolean printDrinkDetail(Bills bill) {
        if (bill.beers.size() == 0) {
            Log.i(TAG, "  printDrinkDetail  NO data need print ");
            return true;
        }

        StringBuilder sb = new StringBuilder();
        /*************  酒水消费信息列名  *********/
        if (bill.beers != null && bill.beers.size() > 0) {
            sb.append(getString(R.string.beer_Name));
            int space = 16 - getStringLength(getString(R.string.beer_Name));
            for (int i = 0; i < space + 4; i++) {
                sb.append(SPACE);
            }

            sb.append(getString(R.string.beer_Count));
            space = 4 - getStringLength(getString(R.string.beer_Count));
            for (int i = 0; i < space + 4; i++) {
                sb.append(SPACE);
            }

            sb.append(getString(R.string.beer_UnitPrice));
            space = 6 - getStringLength(getString(R.string.beer_UnitPrice));
            for (int i = 0; i < space + 4; i++) {
                sb.append(SPACE);
            }



            sb.append(getString(R.string.beer_Subtotal));
            space = 8 - getStringLength(getString(R.string.beer_Subtotal));
            for (int i = 0; i < space; i++) {
                sb.append(SPACE);
            }
            sb.append(NEW_LINE);

            for (int i = 0; i < bill.beers.size(); i++) {
                /*************  显示酒水消费信息数据部分 *********/
                String beerName = bill.beers.get(i).beerName;
                int length = getStringLength(beerName);
                int line = length / 16;
                if (length % 16 != 0) {
                    line++;
                }
                StringBuilder temp = new StringBuilder();
                if (line > 1) {  //超过一行显示
                    int num = 0;
                    while (getStringLength(temp.toString()) <= 16) {
                        temp.append(beerName.charAt(num));
                        num++;
                    }
                    //酒水的具体名字前16个字节
                    sb.append(temp);
                    for (int p = 0; p < 4; p++) {
                        sb.append(SPACE);
                    }

                    //酒水的数量
                    space = 4 - getStringLength(bill.beers.get(i).beerCount);
                    sb.append(bill.beers.get(i).beerCount);
                    for (int p = 0; p < space + 4; p++) {
                        sb.append(SPACE);
                    }

                    //酒水的单价
                    space = 6 - getStringLength(bill.beers.get(i).beerUnitPrice);
                    sb.append(bill.beers.get(i).beerUnitPrice);
                    for (int p = 0; p < space + 4; p++) {
                        sb.append(SPACE);
                    }

                    //酒水的总价
                    space = 8 - getStringLength(bill.beers.get(i).beerSubtotal);
                    sb.append(bill.beers.get(i).beerUnitPrice);
                    for (int p = 0; p < space; p++) {
                        sb.append(SPACE);
                    }
                    sb.append(NEW_LINE);


                    if (--line > 1) {     //酒水的具体名字16个字节后的内容,需要额外两行显示
                        temp.delete(0, temp.length());
                        while (getStringLength(temp.toString()) <= 16) {
                            temp.append(beerName.charAt(num));
                            num++;
                        }
                        sb.append(temp);
                        sb.append(NEW_LINE);
                        sb.append(beerName.substring(num, length));
                        sb.append(NEW_LINE);
                    } else {             //酒水的具体名字16个字节后的内容,需要额外一行显示
                        temp.delete(0, temp.length());
                        while (getStringLength(temp.toString()) <= 16) {
                            temp.append(beerName.charAt(num));
                            num++;
                        }
                        sb.append(temp);
                        sb.append(NEW_LINE);
                    }
                } else {    //不超过一行显示

                    space = 16 - getStringLength(beerName);
                    sb.append(beerName);
                    for (int p = 0; p < space + 4; p++) {
                        sb.append(SPACE);
                    }

                    space = 4 - getStringLength(bill.beers.get(i).beerCount);
                    sb.append(bill.beers.get(i).beerCount);
                    for (int p = 0; p < space + 4; p++) {
                        sb.append(SPACE);
                    }

                    space = 6 - getStringLength(bill.beers.get(i).beerUnitPrice);
                    sb.append(bill.beers.get(i).beerUnitPrice);
                    for (int p = 0; p < space + 4; p++) {
                        sb.append(SPACE);
                    }

                    space = 8 - getStringLength(bill.beers.get(i).beerSubtotal);
                    sb.append(bill.beers.get(i).beerSubtotal);
                    for (int p = 0; p < space; p++) {
                        sb.append(SPACE);
                    }
                    sb.append(NEW_LINE);
                }
            }
        }
        /*************  显示 酒水总计：    *********/
        if (!TextUtils.isEmpty(bill.roomTotalPay)) {
            int space = lineSpaceTotal - getStringLength(getString(R.string.totalPay)) - getStringLength(bill.roomTotalPay);
            for (int i = 0; i < space; i++) {
                sb.append(SPACE);
            }
            sb.append(getString(R.string.hasPayMoney));
            sb.append(bill.totalPay);
            sb.append(NEW_LINE);
        }

        /*************  显示分割线  ————————————————    *********/
        for (int i = 0; i < lineSpaceTotal; i++) {
            sb.append(LINE);
        }
        sb.append(NEW_LINE);

        if (mPrinter.printText(sb.toString()) == -1) {
            Log.e(TAG, "  printerBill  printDrinkDetail  print test false ");
            return false;
        }
        return true;

    }

    /**
     * 打印消费金额内容：总计,已付，实收，应收，应退，找零，抹零
     * @param bill
     * @return true 成功， false 失败
     */
    private static boolean printStatistics(Bills bill) {
        StringBuilder sb = new StringBuilder();
        int space;
        /*************  显示 总计：    *********/
        if (!TextUtils.isEmpty(bill.totalPay)) {
            sb.append(getString(R.string.totalPay));
            space = lineSpaceTotal - getStringLength(getString(R.string.totalPay)) - getStringLength(bill.totalPay);
            for (int i = 0; i < space; i++) {
                sb.append(SPACE);
            }
            sb.append(bill.totalPay);
            sb.append(NEW_LINE);
        }

        /*************  显示 已付：    *********/
        if (!TextUtils.isEmpty(bill.hasPayMoney)) {
            sb.append(getString(R.string.hasPayMoney));
            space = lineSpaceTotal - getStringLength(getString(R.string.hasPayMoney)) - getStringLength(bill.hasPayMoney);
            for (int i = 0; i < space; i++) {
                sb.append(SPACE);
            }
            sb.append(bill.hasPayMoney);
            sb.append(NEW_LINE);
        }

        /*************  显示 预付：    *********/
        if (!TextUtils.isEmpty(bill.advancePayment)) {
            sb.append(getString(R.string.advance_payment));
            space = lineSpaceTotal - getStringLength(getString(R.string.advance_payment)) - getStringLength(bill.advancePayment);
            for (int i = 0; i < space; i++) {
                sb.append(SPACE);
            }
            sb.append(bill.advancePayment);
            sb.append(NEW_LINE);
        }


        /*************  显示 优惠：    *********/
        if (!TextUtils.isEmpty(bill.discount)) {
            sb.append(getString(R.string.discount));
            space = lineSpaceTotal - getStringLength(getString(R.string.discount)) - getStringLength(bill.discount);
            for (int i = 0; i < space; i++) {
                sb.append(SPACE);
            }
            sb.append(bill.discount);
            sb.append(NEW_LINE);
        }


        /*************  显示 折扣：    *********/
        if (!TextUtils.isEmpty(bill.percent)) {
            sb.append(getString(R.string.percent));
            space = lineSpaceTotal - getStringLength(getString(R.string.percent)) - getStringLength(bill.percent);
            for (int i = 0; i < space; i++) {
                sb.append(SPACE);
            }
            sb.append(bill.percent);
            sb.append(NEW_LINE);
        }

        /*************  显示 应收：    *********/
        if (!TextUtils.isEmpty(bill.shouldIncome)) {
            sb.append(getString(R.string.should_Income));
            space = lineSpaceTotal - getStringLength(getString(R.string.should_Income)) - getStringLength(bill.shouldIncome);
            for (int i = 0; i < space; i++) {
                sb.append(SPACE);
            }
            sb.append(bill.shouldIncome);
            sb.append(NEW_LINE);
        }

        /*************  显示 应退：    *********/
        if (!TextUtils.isEmpty(bill.shouldRefund)) {
            sb.append(getString(R.string.should_Refund));
            space = lineSpaceTotal - getStringLength(getString(R.string.should_Refund)) - getStringLength(bill.shouldRefund);
            for (int i = 0; i < space; i++) {
                sb.append(SPACE);
            }
            sb.append(bill.shouldRefund);
            sb.append(NEW_LINE);
        }

        /*************  显示 实收：    *********/
        if (!TextUtils.isEmpty(bill.realIncome)) {
            sb.append(getString(R.string.real_Income));
            space = lineSpaceTotal - getStringLength(getString(R.string.real_Income)) - getStringLength(bill.realIncome);
            for (int i = 0; i < space; i++) {
                sb.append(SPACE);
            }
            sb.append(bill.realIncome);
            sb.append(NEW_LINE);
        }


        /*************  显示 抹零：    *********/
        if (!TextUtils.isEmpty(bill.rounding)) {
            sb.append(getString(R.string.rounding));
            space = lineSpaceTotal - getStringLength(getString(R.string.rounding)) - getStringLength(bill.rounding);
            for (int i = 0; i < space; i++) {
                sb.append(SPACE);
            }
            sb.append(bill.rounding);
            sb.append(NEW_LINE);
        }

        /*************  显示 找零：    *********/
        if (!TextUtils.isEmpty(bill.refund)) {
            sb.append(getString(R.string.refund));
            space = lineSpaceTotal - getStringLength(getString(R.string.refund)) - getStringLength(bill.refund);
            for (int i = 0; i < space; i++) {
                sb.append(SPACE);
            }
            sb.append(bill.refund);
            sb.append(NEW_LINE);
        }

        /*************  显示分割线  ————————————————    *********/
        for (int i = 0; i < lineSpaceTotal; i++) {
            sb.append(LINE);
        }
        sb.append(NEW_LINE);

        if (mPrinter.printText(sb.toString()) == -1) {
            Log.e(TAG, "  printerBill  printStatistics  print test false ");
            return false;
        }
        return true;
    }

    /**
     * 打印支付方式或者退款方式
     * @param bill
     * @return
     */
    private static boolean printPayType(Bills bill) {
        StringBuilder sb = new StringBuilder();
        int space;
        String isPayOrRefund ;
        if(bill.isPayOrRefund == 1){
            isPayOrRefund = getString(R.string.pay_type);
        }else if(bill.isPayOrRefund == 2){
            isPayOrRefund = getString(R.string.refund_type);
        }else {
            return true;
        }
        String type ;
        if (bill.typePayOrRefund == 1) {
            type = getString(R.string.wei_xin);
        } else if (bill.typePayOrRefund == 2) {
            type = getString(R.string.zhi_fu_bao);
        } else if (bill.typePayOrRefund == 3){
            type = getString(R.string.Pos_cash);
        }else {
            type = "";
        }
        sb.append(type);
        space = lineSpaceTotal - getStringLength(isPayOrRefund) - getStringLength(type);
        for (int i = 0; i < space; i++) {
            sb.append(SPACE);
        }
        sb.append(type);
        sb.append(NEW_LINE);

        /*************  显示分割线  ————————————————    *********/
        for (int i = 0; i < lineSpaceTotal; i++) {
            sb.append(LINE);
        }
        sb.append(NEW_LINE);

        if (mPrinter.printText(sb.toString()) == -1) {
            Log.e(TAG, "  printerBill  printPayType  print test false ");
            return false;
        }
        return true;
    }

    /**
     * 打印备注、签字
     * @param bill
     * @return
     */
    private static boolean printFoot(Bills bill) {
        StringBuilder sb = new StringBuilder();
        /*************  显示 备注：   *********/
        if (!TextUtils.isEmpty(bill.reMarks)) {
            sb.append(getString(R.string.reMarks));
            int space = lineSpaceTotal - getStringLength(getString(R.string.reMarks)) - getStringLength(bill.reMarks);
            for (int i = 0; i < space; i++) {
                sb.append(SPACE);
            }
            sb.append(bill.reMarks);
            sb.append(NEW_LINE);
            sb.append(NEW_LINE);
        }

        /*************  显示 客人签字   *********/
        if(bill.autograph == 1){
            sb.append(getString(R.string.autograph)).append(NEW_LINE);
        }
        sb.append("\n\n\n\n\n\n");
        if (mPrinter.printText(sb.toString()) == -1) {
            Log.e(TAG, "  printerBill  printFoot  print test false ");
            return false;
        }
        return true;
    }

    public static boolean printerBill(Bills bill) {

        mPrinter.init();

        if(!printHead(bill)){
            return false;
        }
        if(!printConsumptionTime(bill)){
            return false;
        }
        if(!printTimeDetail(bill)){
            return false;
        }
        if(!printDrinkDetail( bill)){
            return false;
        }
        if(!printStatistics(bill)){
            return false;
        }
        if(!printPayType(bill)){
            return false;
        }
        return printFoot(bill);
    }


    /**
     * 打开钱箱
     */
    public static void openMoneyBox() {
        mPrinter.init();
        byte[] cmdOpen = new byte[]{0x1B, 0x70, 0x00, 0x32, 0x32};
        mPrinter.printByteData(cmdOpen);
//		Log.d("	Log-->	","	openMoneyBox result = "+result);
    }

    /**
     * 小票切刀
     */
    public static void autoCutPaper() {
        mPrinter.init();
        byte[] cmdOpen = new byte[]{0x1b, 0x6D};
        mPrinter.printByteData(cmdOpen);
//		Log.d("	Log-->	","	openMoneyBox result = "+result);
    }

    /**
     * 获取字符串中中文个数
     *
     * @param str
     * @return
     */
    private static int getGBCount(String str) {
        int count = 0;
        String regEx = "[^\u4e00-\u9fa5]+";
        String[] term = str.split(regEx);
        for (String aTerm : term) count = count + aTerm.length();
        return count;
    }

    // 根据UnicodeBlock方法判断中文标点符号
    private static boolean isChinesePunctuation(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        return ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_FORMS
                || ub == Character.UnicodeBlock.VERTICAL_FORMS;
    }

    /**
     * 获取中文标点的个数
     *
     * @param str
     * @return
     */
    private static int getGBSymbol(String str) {
        int symbolCount = 0;
        for (int i = 0; i < str.length(); i++) {
            char tmp = str.charAt(i);
            if (isChinesePunctuation(tmp)) {
                symbolCount++;
            }
        }
        return symbolCount;
    }

    /**
     * 获取字符串共计占用多少字符个数
     *
     * @param str
     * @return
     */
    private static int getStringLength(String str) {
        if (str == null) {
            return 0;
        }
        return str.length() + getGBCount(str) + getGBSymbol(str);
    }

    private static String getString(int StringID){
        return app.getString(StringID);
    }

} 