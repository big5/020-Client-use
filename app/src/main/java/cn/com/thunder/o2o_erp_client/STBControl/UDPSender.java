package cn.com.thunder.o2o_erp_client.STBControl;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

/**
 * @author qiuyanlong(email:276644135@qq.com)
 * @date 2017-05-19 14:48
 * @package cn.com.thunder.o2o_erp_client.STBControl
 */
public class UDPSender extends Thread{
    MulticastSocket sender = null;
    DatagramPacket dj = null;
    InetAddress group = null;
    String sendMsg = "FIND_BOX_MESSAGE";

    public UDPSender(String dataString) {
        sendMsg = dataString;
    }

    public UDPSender() {

    }

    @Override
    public void run() {
        try {
            sender = new MulticastSocket();
            group = InetAddress.getByName("255.255.255.255");
            dj = new DatagramPacket(sendMsg.getBytes(),sendMsg.getBytes().length,group,2007);
            sender.send(dj);
            sender.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
} 