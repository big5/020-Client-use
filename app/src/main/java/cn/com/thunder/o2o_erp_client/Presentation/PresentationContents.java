package cn.com.thunder.o2o_erp_client.Presentation;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author qiuyanlong(email:276644135@qq.com)
 * @date 2017-02-23 14:06
 * @package cn.com.thunder.erp_android.Presentation
 */
public class PresentationContents implements Parcelable {

    private String str;
    public PresentationContents(String str) {
        this.str = str;
    }

    protected PresentationContents(Parcel in) {
        str = in.readString();
    }

    public static final Creator<PresentationContents> CREATOR = new Creator<PresentationContents>() {
        @Override
        public PresentationContents createFromParcel(Parcel in) {
            return new PresentationContents(in);
        }

        @Override
        public PresentationContents[] newArray(int size) {
            return new PresentationContents[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(str);
    }
}