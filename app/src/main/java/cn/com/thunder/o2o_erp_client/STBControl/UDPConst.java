package cn.com.thunder.o2o_erp_client.STBControl;

/**
 * File Name    : SearcherConst
 * Description  : 搜索者常量
 * Author       : Ralap
 * Create Date  : 2016/9/22
 * Version      : v1
 */
public interface UDPConst {
    int RECEIVER_UDP_MSG = 2017;
    String MULTICAST_IP = "255.255.255.255";
    int DEVICE_FIND_PORT = 2007; // 设备监听端口
    int RESPONSE_DATA_MAX = 512; // 响应设备返回的最大数据长度

    int DEVICE_RECEIVE_DEFAULT_TIME_OUT = 5000; // 设备默认的接收超时时间

}
