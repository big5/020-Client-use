package cn.com.thunder.o2o_erp_client;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.iprt.xzc_pc.android_print_sdk.BluetoothPrinter;
import com.iprt.xzc_pc.android_print_sdk.PrinterType;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.cache.CacheMode;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cn.com.thunder.o2o_erp_client.DownLoad.DownLoadThread;
import cn.com.thunder.o2o_erp_client.Presentation.PresentationContents;
import cn.com.thunder.o2o_erp_client.Presentation.PresentationMode;
import cn.com.thunder.o2o_erp_client.PrinterDriver.PrintApi;
import cn.com.thunder.o2o_erp_client.STBControl.UDPConst;
import cn.com.thunder.o2o_erp_client.STBControl.UDPReceiver;
import cn.com.thunder.o2o_erp_client.STBControl.UDPSender;
import cn.com.thunder.o2o_erp_client.bean.Beer;
import cn.com.thunder.o2o_erp_client.bean.Bills;
import cn.com.thunder.o2o_erp_client.bean.LKStb;
import cn.com.thunder.o2o_erp_client.bean.Room;
import cn.com.thunder.o2o_erp_client.tool.AndroidBug5497Workaround;
import cn.com.thunder.o2o_erp_client.utils.ClsUtils;
import cn.com.thunder.o2o_erp_client.utils.SysUtils;
import cn.com.thunder.o2o_erp_client.utils.ToastUtils;
import okhttp3.Call;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    private final static String TAG = "MainActivity";
    private static final String THUNDER_PRINTER = "T802_BT_Printer"; //修改时同时修改 BluetoothDeviceListActivity
    private static final String THUNDER_PRINTER_PWD = "1234";
    private static final int RECEIVER_UDP_MSG = 2007;


    /**
     * 蓝牙打印机
     */
    private boolean usePrinter = true;
    private BluetoothAdapter mBluetoothAdapter = null;
    public static BluetoothPrinter bPrinter;
    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private BluetoothDevice currentDevice;

    public static boolean isConnected; //蓝牙打印机是否已经连接
    private boolean isConnecting;  //蓝牙打印机是否正在连接


    /**
     * webView
     */
    private WebView webView;
    private ValueCallback<Uri[]> mFilePathCallback;
    private final static int FILE_CHOOSER_RESULT_CODE = 1234;

    /**
     * presentation 双屏内容
     */
    private PresentationMode presentationMode;
    private static final String PRESENTATION_KEY = "presentation";
    private SparseArray<PresentationContents> mSavedPresentationContents;

    /**
     * UDP
     */
    private UDPReceiver mUDPReceiver;
    private HashSet<LKStb> lkStbs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mSavedPresentationContents = savedInstanceState.getSparseParcelableArray(PRESENTATION_KEY);
        } else {
            mSavedPresentationContents = new SparseArray<>();
        }
        initDoubleScreen();
        setContentView(R.layout.activity_main);

//        //默认禁用软键盘
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

        //解决软键盘上推问题，必须在setContentView之后设置
        AndroidBug5497Workaround.assistActivity(this);

        if (usePrinter) {
            if (initBluetooth()) {
                initReceiver();
            }
        }

        initWebView();

        lkStbs = new HashSet<>();
        createSTBReceiver();

    }


    @Override
    protected void onResume() {
        super.onResume();
        presentationMode.registerDisplays();
    }

    @Override
    protected void onStart() {
        super.onStart();

        mUDPReceiver.dealWithMessage(true);
        mUDPReceiver.open();

        new UDPSender().start();

        if (usePrinter) {
            if (mBluetoothAdapter == null) {
                if (initBluetooth()) {
                    initReceiver();
                    requestPermission();
                }
            } else if (!isConnected) {
                requestPermission();
            } else {
                String defaultAddress = getDefaultBTPrinter();
                if (!TextUtils.isEmpty(defaultAddress)) {
                    currentDevice = mBluetoothAdapter.getRemoteDevice(defaultAddress);
                    initPrinter(currentDevice);
                }
            }
        }
    }

    @Override
    protected void onPause() {
        // Be sure to call the super class.
        super.onPause();
        presentationMode.unRegisterDisplays();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mUDPReceiver.dealWithMessage(false);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Be sure to call the super class.
        super.onSaveInstanceState(outState);
        outState.putSparseParcelableArray(PRESENTATION_KEY, mSavedPresentationContents);
    }

    private void initReceiver() {
        //本机蓝牙状态广播
        IntentFilter localFilter = new IntentFilter();
        localFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);           //本机蓝牙状态广播
        registerReceiver(mStateReceiver, localFilter);

        //连接蓝牙设备广播
        IntentFilter externalFilter = new IntentFilter();
        externalFilter.addAction(BluetoothDevice.ACTION_PAIRING_REQUEST);          //蓝牙密码配对
        externalFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);         //连接状态断开广播
        externalFilter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);       //绑定状态改变
        registerReceiver(mPairReceiver, externalFilter);

    }

    /**
     * 双屏初始化
     */
    private void initDoubleScreen() {
        if (presentationMode == null) {
            presentationMode = new PresentationMode(this);
        }
        presentationMode.mSavedPresentationContents = mSavedPresentationContents;
        presentationMode.updateContents();
    }

    /**
     * 使用软键盘.如果onCreate时，禁用了软键盘。可以通过此方法解除禁用。但解除禁用后。无法重新禁用
     */
//    public void openKeyBoard() {
//        getWindow().clearFlags(
//                WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
//    }


    /**
     * 初始化webView
     */
    private void initWebView() {
        webView = (WebView) findViewById(R.id.webView);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setAllowFileAccess(true);
        settings.setSupportZoom(false);
        settings.setBuiltInZoomControls(false);
        settings.setAppCacheEnabled(false);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.addJavascriptInterface(new JSInterface(this, new JSInterface.JSMessage() {

            @Override
            public void showSecondScreen(boolean display, String content) {
                if (presentationMode.displays.length > 1) {
                    if (display) {
                        presentationMode.showPresentation(presentationMode.displays[0], new PresentationContents("www.baidu.com"));
                    } else {
                        presentationMode.hidePresentation(presentationMode.displays[0]);
                    }
                } else {
                    ToastUtils.popUpToast("分屏连接失败。无法切换分屏显示！");
                    presentationMode.updateContents();
                    if (presentationMode.haveSecondScreen()) {
                        presentationMode.registerDisplays();
                    }
                }
            }

            @Override
            public void printNotes(String data) {
                if (isConnecting) {
                    ToastUtils.popUpToast("正在连接打印机，请稍后！");
                    return;
                }
                if (!isConnected) {
                    ToastUtils.popUpToast("打印机未连接，或连接失败！");
                    return;
                }
                JSONObject json;
                try {
                    json = new JSONObject(data);
                    getBills(json);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void openBox() {
                if (bPrinter != null) {
                    PrintApi.setPrinter(bPrinter);
                    PrintApi.openMoneyBox();//打开钱箱
                } else {
                    SysUtils.LogD(TAG, "openBox   false   打印设备对象为空   ");
                }
            }

            @Override
            public String getSTBState() {
                if (lkStbs == null) return "";
                return new Gson().toJson(lkStbs);
            }

            @Override
            public void setSTBState(String content) {
                try {
                    JSONObject jsonObject = new JSONObject(content);
                    JSONObject object = jsonObject.optJSONObject("data");
                    if (object != null) {
                        String mac = object.optString("mac");
                        String ip = getSTBIpByMac(mac);
                        String command = object.optString("command");
                        int time = object.optInt("time");
                        int type = object.optInt("type");
                        if (TextUtils.isEmpty(mac)) {
                            SysUtils.LogE(TAG, "    setSTBState   Mac address is null  ");
                        } else if (TextUtils.isEmpty(ip)) {
                            SysUtils.LogE(TAG, "  setSTBState   IP  address is null , you need resend UDP to get IP !");
                        } else {
                            sendMessage2LKSTB(ip, command, time, type);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }), "JSInterface");

        webView.loadUrl("file:///android_asset/src/init_setup/index.html");

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if (progressBar != null) {
                    progressBar.setVisibility(android.view.View.VISIBLE);
                }

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (progressBar != null) {
                    progressBar.setVisibility(android.view.View.GONE);
                }
            }
        });

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onShowCustomView(View view, CustomViewCallback callback) {
                super.onShowCustomView(view, callback);
            }

            // For Android >= 5.0
            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams) {
                mFilePathCallback = filePathCallback;
                openImageChooserActivity();
                return true;
            }


        });

        /**
         * 文件下载
         */
        webView.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                SysUtils.LogE(TAG, "onDownloadStart被调用：下载链接：" + url);
                new Thread(new DownLoadThread(url)).start();
//                Uri uri = Uri.parse(url);
//
//                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//
//                startActivity(intent);
            }
        });

        //禁止webView长按复制静态页面内容
        webView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return true;
            }
        });
    }


    //webView选择本地图片
    private void openImageChooserActivity() {
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.setType("image/*");
        startActivityForResult(Intent.createChooser(i, "File Chooser"), FILE_CHOOSER_RESULT_CODE);
    }

    /**
     * 处理接收到的消息
     */
    private Handler bHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            SysUtils.LogD(TAG, "msg.what is : " + msg.what);
            switch (msg.what) {
                case RECEIVER_UDP_MSG:  //UDP广播，更新雷客STB状态
                    Bundle bundle = msg.getData();
                    String ip = bundle.getString("ip");
                    String address = bundle.getString("address");
                    int state = bundle.getInt("state");

                    // 刷新机顶盒数据
                    LKStb stb = new LKStb(address, ip, state);
                    reFreshSTBState(stb);
                    break;

                case BluetoothPrinter.Handler_Connect_Connecting:
                    isConnecting = true;
                    //mTitle.setText(R.string.title_connecting);
                    Toast.makeText(getApplicationContext(), R.string.bt_connecting, Toast.LENGTH_SHORT).show();
                    break;
                case BluetoothPrinter.Handler_Connect_Success:
                    isConnected = true;
                    isConnecting = false;
                    saveDefaultBTPrinter(bPrinter);
                    PrintApi.setPrinter(bPrinter);

                    Toast.makeText(getApplicationContext(), R.string.bt_connect_success, Toast.LENGTH_SHORT).show();
//                    PrintApi.setmPrinter(bPrinter);
//                    printerDemo();
                    break;
                case BluetoothPrinter.Handler_Connect_Failed:
                    isConnected = false;
                    isConnecting = false;
                    clearDefaultBTPrinter();
                    //mTitle.setText(R.string.title_not_connected);
                    Toast.makeText(getApplicationContext(), R.string.bt_connect_failed, Toast.LENGTH_SHORT).show();
                    break;
                case BluetoothPrinter.Handler_Connect_Closed:
                    SysUtils.LogE(TAG, "获取蓝牙打印机回调信息, 断开连接 ");
                    isConnected = false;
                    isConnecting = false;
                    //mTitle.setText(R.string.title_not_connected);
                    Toast.makeText(getApplicationContext(), R.string.bt_connect_closed, Toast.LENGTH_SHORT).show();
                    // if(normalClose!=true){
                    //    initPrinter(bDevice);
                    // }
                    break;
                case BluetoothPrinter.Handler_Message_Read:
                    // int length=  msg.arg1;
                    //  status = (int) msg.obj;
                    // SysUtils.LogD("收到数据长度:",""+length);
                    // SysUtils.LogD("返回的状态值:",""+status);
                    break;
            }
        }
    };


    /*********************************   蓝牙打印机   ************************************/

    /**
     * 初始化蓝牙
     */
    private boolean initBluetooth() {
        //蓝牙
        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            //无法获取本机蓝牙设备。
            Toast.makeText(this, R.string.can_not_find_local_bluetooth, Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_CHOOSER_RESULT_CODE:
                uploadImgFromSysPhotos(resultCode, data);
                break;
            case REQUEST_CONNECT_DEVICE:
                if (resultCode == Activity.RESULT_OK) {
                    String address = data.getExtras().getString(BluetoothDeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                    SysUtils.LogD(TAG, "device.getBondState() is : " + device.getBondState());

                    if (device.getBondState() == BluetoothDevice.BOND_NONE) {
                        boolean result = unPairOrPairDevice(false, device);
                        if (!result) {
                            SysUtils.LogE(TAG, "    BluetoothDevice.BOND_NONE  pairDevice  false ");
                        }
                    } else if (device.getBondState() == BluetoothDevice.BOND_BONDED) {
                        initPrinter(device);
                    }
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode != Activity.RESULT_OK) {
                    // User did not enable Bluetooth or an error occured
                    Toast.makeText(this, R.string.not_find_bluetooth, Toast.LENGTH_SHORT).show();
                }
//                else {
//                    // 获取蓝牙连接设备列表
//                    SysUtils.LogE(TAG, "  REQUEST_ENABLE_BT     getHasPairDevice  ");
//                    getHasPairDevice();
//                }
        }
    }

    /**
     * 上传图片,调用系统图库 与h5 file标签交互
     *
     * @param resultCode
     * @param intent
     */

    private void uploadImgFromSysPhotos(int resultCode, Intent intent) {
        if (mFilePathCallback != null) {//5.0+
            Uri[] uris = new Uri[1];
            uris[0] = intent == null || resultCode != RESULT_OK ? null : intent.getData();
            if (uris[0] != null) {
                mFilePathCallback.onReceiveValue(uris);
            }
            mFilePathCallback = null;
        }
    }

    /**
     * 如果本机蓝牙开启，直本机蓝牙开启
     * 如果本机蓝牙未开启，请求蓝牙权限
     */
    private void requestPermission() {
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        } else {
            SysUtils.LogE(TAG, " requestPermission    ");
            getHasPairDevice();
        }
    }


    /**
     * 获取权限后，读取已匹配蓝牙设备。
     * 注册蓝牙搜索广播，获取权限后。根据已绑定设备的数量。决定是否重新连接。
     * 1、如果一个设备,则检查设备是否是打印机上次对应的设备。
     * 如果是，直接连接。如果不是，则清除记录，重新搜索蓝牙设备，并绑定连接
     * 2、如果多于一个设备,W则检测是否有上次连接的打印设备，如果有，直接连接。
     * 如果没有，则直接清除绑定的所有蓝牙设备，并且重新搜索蓝牙打印机并绑定。
     */
    private void getHasPairDevice() {

        // Get a set of currently paired devices
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        String bindAddress = "";

        if (pairedDevices.size() == 1) {
            //1.读取本地已经匹配的蓝牙设备
            for (BluetoothDevice device : pairedDevices) {
                bindAddress = device.getAddress();
            }
            //2.通过本地蓝牙设备得到远程蓝牙设备
            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(bindAddress);

            //3.如果是绑定状态初始化
            if (device.getBondState() == BluetoothDevice.BOND_BONDED) {
                // 4.初始化打印设备
                initPrinter(device);
                return;
            } else {
                // 接触绑定
                unPairOrPairDevice(true, device);
            }

        } else {
            //解除绑定并且重新搜索
            for (BluetoothDevice device : pairedDevices) {
                unPairOrPairDevice(true, device);
            }
        }
        //搜索蓝牙动作
        Intent deviceIntent = new Intent(MainActivity.this, BluetoothDeviceListActivity.class);
        startActivityForResult(deviceIntent, REQUEST_CONNECT_DEVICE);
    }

    /**
     * 清空默认的蓝牙打印设备
     */
    private void clearDefaultBTPrinter() {
        SharedPreferences.Editor editor = ThunderApplication.getParamsManager().edit();
        editor.putString("btAddress", "");
        editor.apply();
    }

    /**
     * 保存为默认的蓝牙打印设备
     */
    private void saveDefaultBTPrinter(BluetoothPrinter bPrinter) {
        if (bPrinter != null) {
            SharedPreferences.Editor editor = ThunderApplication.getParamsManager().edit();
            editor.putString("btAddress", bPrinter.getMacAddress());
            editor.apply();
        }
    }

    /**
     * 读取默认的蓝牙打印设备信息
     */
    private String getDefaultBTPrinter() {
        return ThunderApplication.getParamsManager().getString("btAddress", "");
    }

    /**
     * 取消搜索
     */
    private void cancelSearch() {
        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();
        }
    }

    /**
     * 蓝牙状态改变广播
     * receive the state change of the bluetooth.
     */
    private BroadcastReceiver mStateReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            SysUtils.LogE(TAG, action);
            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        SysUtils.LogD(TAG, "STATE_OFF 手机蓝牙关闭");
                        isConnected = false;
                        cancelSearch();
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        SysUtils.LogD(TAG, "STATE_TURNING_OFF 手机蓝牙正在关闭");
                        isConnected = false;
                        cancelSearch();
                        break;
                    case BluetoothAdapter.STATE_ON:
                        SysUtils.LogD(TAG, "STATE_ON 手机蓝牙开启");
                        if (SysUtils.isApplicationBroughtToBackground(MainActivity.this)) {
                            //TODO 检测已匹配内容
                            SysUtils.LogD(TAG, " 手机蓝牙开启 , 当前应用是处于前台 ");
                            getHasPairDevice();
                        } else {
                            SysUtils.LogD(TAG, " 手机蓝牙开启 , 当前应用是处于后台 ");
                        }
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        SysUtils.LogD(TAG, "STATE_TURNING_ON 手机蓝牙正在开启");
                        isConnected = false;
                        break;
                }
            }
        }
    };

    /**
     * 蓝牙配对广播。
     * The BroadcastReceiver that listens for discovered devices
     */
    private final BroadcastReceiver mPairReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // When discovery finds a device
            switch (action) {
                case BluetoothDevice.ACTION_PAIRING_REQUEST://蓝牙密码配对
                    if (currentDevice.getName().contains(THUNDER_PRINTER)) {
                        try {
                            //1.确认配对
                            ClsUtils.setPairingConfirmation(currentDevice.getClass(), currentDevice, true);
                            //2.终止有序广播
                            abortBroadcast();//如果没有将广播终止，则会出现一个一闪而过的配对框。
                            //3.调用setPin方法进行配对...
                            ClsUtils.setPin(currentDevice.getClass(), currentDevice, THUNDER_PRINTER_PWD);

                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    } else {
                        SysUtils.LogE("提示信息", "这个设备不是目标蓝牙设备");
                    }
                    break;
                case BluetoothDevice.ACTION_BOND_STATE_CHANGED://绑定状态改变
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    if (!currentDevice.equals(device)) {
                        return;
                    }
                    switch (device.getBondState()) {
                        case BluetoothDevice.BOND_BONDING:
                            SysUtils.LogD(TAG, "bounding......");
                            break;
                        case BluetoothDevice.BOND_BONDED:
                            SysUtils.LogD(TAG, "bound success");
                            // if bound success, auto init BluetoothPrinter. open connect.
                            unregisterReceiver(mPairReceiver);
                            SysUtils.LogE(TAG, " 444 bound success");
                            initPrinter(device);
                            break;
                        case BluetoothDevice.BOND_NONE:
                            unregisterReceiver(mPairReceiver);
                            SysUtils.LogD(TAG, " ACTION_BOND_STATE_CHANGED    bound BOND_NONE");
                        default:
                            break;
                    }
                    break;

                case BluetoothDevice.ACTION_ACL_DISCONNECTED:       //连接状态断开广播
                    BluetoothDevice tempDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    if (tempDevice != null) {
                        SysUtils.LogE(TAG, "  mStateReceiver :" + tempDevice.getName());
                        SysUtils.LogE(TAG, "  mStateReceiver :" + tempDevice.getAddress());
                    } else {
                        SysUtils.LogE(TAG, "  device  =  null");
                    }
                    if (action.equals(BluetoothDevice.ACTION_ACL_DISCONNECTED)) {
                        if (bPrinter != null && tempDevice != null && isConnected) {
                            if (bPrinter.getMacAddress().equals(tempDevice.getAddress())) {
                                bPrinter.closeConnection();
                            }
                        }
                        SysUtils.LogE(TAG, "  广播接收到，可以连接");
                    }
                    break;
                default:
                    break;
            }
        }
    };

    /**
     * 取消匹配或者匹配蓝牙设备
     *
     * @param un_pair true 解除匹配   false 匹配
     * @param device  当前要连接的蓝牙设备
     * @return true 成功
     */
    private boolean unPairOrPairDevice(boolean un_pair, BluetoothDevice device) {
        boolean success;
        try {
            currentDevice = device;
            if (un_pair) {
                success = ClsUtils.unPair(device);
            } else {
//                Method createBondMethod = BluetoothDevice.class.getMethod("createBond");
//                success = (Boolean) createBondMethod.invoke(device);
//                SysUtils.LogE(TAG, "createBond is success? : " + success);
                success = ClsUtils.pair(device.getAddress(), THUNDER_PRINTER_PWD);
                SysUtils.LogE(TAG, "createBond is success? : " + success);
            }
        } catch (Exception e) {
            SysUtils.LogD(TAG, "removeBond or createBond failed.");
            e.printStackTrace();
            success = false;
        }
        return success;
    }


    /**
     * 连接蓝牙打印机
     *
     * @param device 当前要连接的蓝牙设备
     */
    private void initPrinter(BluetoothDevice device) {
        isConnecting = true;
        bPrinter = new BluetoothPrinter(device);
        bPrinter.setCurrentPrintType(PrinterType.M31);
        //set handler for receive message of connect state from sdk.
        bPrinter.setHandler(bHandler);
        bPrinter.openConnection();
        bPrinter.setEncoding("GBK");
    }


    /********************************************
     * 打印模块 方法 end
     ***********************************************/

    private Bills getBills(JSONObject json) {
        SysUtils.LogE(TAG, "  getBulls  json = " + json.toString());
        Bills bills = new Bills();
        try {
            JSONObject object = json.getJSONObject("data");
            bills.printFrequency = object.optInt("printFrequency");
            bills.titleType = object.optString("titleType");
            bills.flowingBill = object.optString("flowingBill");
            bills.orderNumber = object.optString("orderNumber");
            bills.originalBill = object.optString("originalBill");
            bills.netReceivables = object.optString("netReceivables");
            bills.roomName = object.optString("roomName");
            bills.OperationType = object.optString("OperationType");


            bills.openTime = object.optString("openTime");
            bills.backTime = object.optString("backTime");
            bills.checkoutTime = object.optString("checkoutTime");
            bills.roomPackage = object.optString("roomPackage");
            bills.minConsumption = object.optString("minConsumption");

            JSONArray roomArray = object.optJSONArray("room");
            List<Room> rooms = new ArrayList<>();
            Room room;
            for (int i = 0; i < roomArray.length(); i++) {
                room = new Room();
                JSONObject roomObject = roomArray.getJSONObject(i);
                room.timeInterval = roomObject.optString("timeInterval");
                room.timeLength = roomObject.optString("timeLength");
                room.timeUnitPrice = roomObject.optString("timeUnitPrice");
                room.timeSubtotal = roomObject.optString("timeSubtotal");
                rooms.add(room);
            }
            bills.rooms = rooms;
            bills.roomTotalPay = object.optString("roomTotalPay");

            List<Beer> beers = new ArrayList<>();
            Beer beer;
            JSONArray beerArray = object.optJSONArray("beer");
            for (int i = 0; i < beerArray.length(); i++) {
                JSONObject beerObject = beerArray.getJSONObject(i);
                beer = new Beer();
                beer.beerName = beerObject.optString("beerName");
                beer.beerUnitPrice = beerObject.optString("beerUnitPrice");
                beer.beerCount = beerObject.optString("beerCount");
                beer.beerSubtotal = beerObject.optString("beerSubtotal");
                beers.add(beer);
            }
            bills.beers = beers;
            bills.beerTotalPay = object.optString("beerTotalPay");

            bills.totalPay = object.optString("totalPay");
            bills.hasPayMoney = object.optString("hasPayMoney");
            bills.advancePayment = object.optString("advancePayment");
            bills.discount = object.optString("discount");
            bills.percent = object.optString("percent");
            bills.rounding = object.optString("rounding");
            bills.shouldIncome = object.optString("shouldIncome");
            bills.shouldRefund = object.optString("shouldRefund");
            bills.realIncome = object.optString("realIncome");
            bills.refund = object.optString("refund");

            bills.isPayOrRefund = object.optInt("isPayOrRefund");
            bills.typePayOrRefund = object.optInt("typePayOrRefund");
            bills.autograph = object.optInt("autograph");

            bills.reMarks = object.optString("reMarks");

            PrintApi.printerBill(bills);
            PrintApi.autoCutPaper();//切刀
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return bills;
    }

    /**
     * 创建UDP消息接受者
     */
    private void createSTBReceiver() {
        mUDPReceiver = new UDPReceiver() {


            /**
             * 	LEIKE_OPEN_API FROM CLOUD12PLUS MAC:%s ISFANGTAI:%d REMAININGTIME:%d TYPE:%d
             * 	 Mac：机顶盒mac地址
             * 	ISFANGTAI：是否购买了房台
             * 	TYPE:开台类型
             * 	TYPE == 3
             * 	关机或者重启
             * 	TYPE == 2
             * 	一直开台；REMAININGTIME:已经开台的时间
             * 	TYPE  == 0
             * 	倒计时开台；REMAININGTIME:房台剩余时间
             * @param datagramPacket
             */
            @Override
            public void parseUserData(DatagramPacket datagramPacket) {

                try {
                    InetSocketAddress inetSocketAddress = (InetSocketAddress) datagramPacket.getSocketAddress();
                    String ip = inetSocketAddress.getAddress().getHostAddress();
                    byte userData[] = datagramPacket.getData();
                    //TODO 解析收到的udp广播内容 设置 MAC 和 房间状态
                    String content = new String(userData, "UTF-8").trim();
                    String address = "";
                    int state = 0;  //0. 关台状态  1. 开台状态。 2. 关机或重启状态。
                    if (content.contains("LEIKE_OPEN_API") && content.contains("MAC")
                            && content.contains("TYPE") && content.contains("REMAININGTIME")) {

                        String[] params = content.split(" ");
                        for (String param : params) {
                            String keyValue[] = param.split(":");
                            switch (keyValue[0]) {
                                case "MAC":
                                    address = keyValue[1];
                                    break;
                                case "REMAININGTIME":
                                    if (Integer.valueOf(keyValue[1]) > 0) {
                                        state = 1;
                                    } else {
                                        state = 0;
                                    }
                                    break;
                                case "TYPE":
                                    if (Integer.valueOf(keyValue[1]) == 3) {
                                        state = 2;
                                    }
                                    break;
                            }
                        }

                        Message message = Message.obtain();
                        Bundle bundle = new Bundle();
                        bundle.putString("ip", ip);
                        bundle.putString("address", address);
                        bundle.putInt("state", state);
                        message.setData(bundle);
                        message.what = RECEIVER_UDP_MSG;
                        bHandler.sendMessage(message);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void printLog(String log) {

            }
        };
    }


    /**
     * 发送控制信息到指定的雷客机顶盒
     * http:// IP:port/encrypt_operation?
     * 接口描述详细内容查看文件 LeiKeSTB_Interface.md
     */
    private void sendMessage2LKSTB(String ip, String command, int time, int type) {

        String baseUrl = Constant.LK_URL_HEADER + ip + ":" + UDPConst.DEVICE_FIND_PORT + Constant.LK_URL_FOOT;
        String urlParam = "";
        String space = " ";
        switch (command) {
            case Constant.LK_OPEN: {
                if (type == 2) { //若开台方式是一直开台。则时间不能为0
                    time = 1000;
                }
                urlParam = "authority=2&passkey=&op=ID:056" + space + "LKCONTROLROOM_START TIME:" + time + space + "TYPE:" + type;
                break;
            }
            case Constant.LK_CLOSE: {
                urlParam = "authority=2&passkey=&op=ID:058" + space + "LKCONTROLROOM_CLOSEROOM";
                break;
            }
            case Constant.LK_KEEP: {
                urlParam = "authority=2&passkey=&op=ID:057" + space + "LKCONTROLROOM_MORETIME" + space + "TIME:" + time;
                break;
            }
            case Constant.LK_REBOOT: {
                urlParam = "authority=2&passkey=&op=ID:060" + space + "LKCONTROLROOM_RESTART";
                break;
            }
            case Constant.LK_SHUT_DOWN: {
                urlParam = "authority=2&passkey=&op=ID:059" + space + "LKCONTROLROOM_SHUTDOWN";
                break;
            }
            default:
                break;
        }

        OkGo.post(baseUrl + urlParam)     // 请求方式和请求url
                .tag(this)                       // 请求的 tag, 主要用于取消对应的请求
                .cacheMode(CacheMode.NO_CACHE)    // 缓存模式，详细请看缓存介绍
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {

                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        lkStbs.clear();
                        new UDPSender().start();
                    }

                });

    }

    /**
     * 刷新 机顶盒信息
     *
     * @param stb
     * @return
     */

    private boolean reFreshSTBState(LKStb stb) {
        if (stb == null) {
            return false;
        }
        for (LKStb tempStb : lkStbs) {
            if (tempStb.equals(stb)) {
                tempStb.ip = stb.ip;
                tempStb.state = stb.state;
                return true;
            }
        }
        return true;
    }

    /**
     * 刷新 机顶盒信息
     *
     * @param mac
     * @return
     */
    private String getSTBIpByMac(String mac) {
        if (mac == null) {
            return null;
        }
        for (LKStb tempStb : lkStbs) {
            if (tempStb.getAddress().equals(mac)) {
                return tempStb.getIp();
            }
        }
        return null;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (webView.canGoBack()) {
                webView.goBack();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (mUDPReceiver != null) {
                mUDPReceiver.close();
            }

            if (bPrinter != null) {
                bPrinter.closeConnection();
            }
            isConnected = false;
            unregisterReceiver(mStateReceiver);
            unregisterReceiver(mPairReceiver);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        android.os.Process.killProcess(android.os.Process.myPid());
    }
}
