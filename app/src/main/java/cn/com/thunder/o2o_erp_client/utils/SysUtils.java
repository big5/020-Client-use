package cn.com.thunder.o2o_erp_client.utils;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import java.util.List;

import cn.com.thunder.o2o_erp_client.ThunderApplication;

/**
 * @author qiuyanlong(email:276644135@qq.com)
 * @date 2016-11-10 09:45
 * @package com.thunder.starface.utils
 * @description SysUtils 工具类
 */
public class SysUtils {

    private static final String TAG = "SysUtils";

    public static long getUIThread() {
        return ThunderApplication.mMainThreadId;
    }

    /**
     * 判断当前线程是否是UI线程
     */
    public static boolean isUIThread() {
        return android.os.Process.myTid() == getUIThread();
    }

    /**
     * 获得屏幕尺寸
     */
    public static int[] getScreenSize(Context context) {
        int[] size = new int[2];
        WindowManager manager = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(outMetrics);
        size[0] = outMetrics.widthPixels;
        size[1] = outMetrics.heightPixels;
        return size;
    }


    /**
     * 判断软键盘是否打开 若返回true，则表示输入法打开
     */
    public static boolean isSoftKeyboardShow() {
        ThunderApplication app = ThunderApplication.getApp();
        InputMethodManager imm = (InputMethodManager) app
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm == null)
            return false;
        return imm.isActive();
    }


    /**
     * 显示、隐藏软键盘
     */
    public static void showSoftKeyboard(View v, boolean isShow) {
        ThunderApplication app = ThunderApplication.getApp();
        InputMethodManager imm = (InputMethodManager) app
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (isShow) {
            imm.showSoftInput(v, InputMethodManager.SHOW_FORCED);
        } else {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }


    /**
     * 判断网络连接是否可用
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null) {
        } else {
            // 如果仅仅是用来判断网络连,则可以使用 cm.getActiveNetworkInfo().isAvailable();
            NetworkInfo[] info = cm.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * 转换dp为Pixel
     */
    public static float convertDpToPixel(int dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                ThunderApplication.getApp().getResources().getDisplayMetrics());
    }

    /**
     * 转换sp为Pixel
     */
    public static float convertSpToPixel(int sp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp,
                ThunderApplication.getApp().getResources().getDisplayMetrics());
    }

    /**
     * 转换Pixel为dp
     */
    public static int convertPixelToDp(Context context, float px) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (px / scale + 0.5f);
    }

    /**
     * 获取10位长度的时间戳
     *
     * @return
     */
    public static Long getCurrentDayTimestampTenNumber() {
        return System.currentTimeMillis() / 1000;
    }


    /**
     * 获得当前应用包信息 （版本号，版本名）
     */
    public static PackageInfo getPackageInfo() {
        Application app = ThunderApplication.getApp();
        PackageManager manager = app.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(app.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return info;
    }

    public static boolean isApplicationBroughtToBackground(final Context context) {
        String packageName = context.getPackageName();
        ActivityManager activityManager = (ActivityManager) context.getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager
                .getRunningAppProcesses();
        if (appProcesses == null)
            return false;

        Log.i(TAG, "list size:" + appProcesses.size());
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            // The name of the process that this object is associated with.
            Log.i(TAG, "appName:" + appProcess.processName + " appImportance:" + appProcess.importance);
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND &&
                    appProcess.processName.equals(packageName)) {
                return true;
            }

        }
        return false;
    }


    public static void LogE(Object tag, String msg) {
        if (ThunderApplication.debugLog) {
            String strTag;
            if (tag instanceof String) {
                strTag = (String) tag;
            } else if (tag instanceof Class) {
                Class cls = (Class) tag;
                strTag = cls.getSimpleName();
            } else {
                strTag = tag.getClass().getSimpleName();
            }
            Log.e(strTag, msg);
        }
    }
    public static void LogD(Object tag, String msg) {
        if (ThunderApplication.debugLog) {
            String strTag;
            if (tag instanceof String) {
                strTag = (String) tag;
            } else if (tag instanceof Class) {
                Class cls = (Class) tag;
                strTag = cls.getSimpleName();
            } else {
                strTag = tag.getClass().getSimpleName();
            }
            Log.e(strTag, msg);
        }
    }
} 