package cn.com.thunder.o2o_erp_client.Presentation;

import android.app.Presentation;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import cn.com.thunder.o2o_erp_client.R;

/**
 * @author qiuyanlong(email:276644135@qq.com)
 * @date 2017-02-23 14:08
 * @package cn.com.thunder.erp_android.Presentation
 */
public class DemoPresentation extends Presentation {

//    private DisplayManager mDisplayManager;
    private static final String TAG = "DemoPresentation";
    PresentationContents mContents;
    Context context;
    WebView secondWebView;
    ProgressBar sencondProgressBar;
    public DemoPresentation(Context context, Display display, PresentationContents contents) {

        super(context, display);
        this.context = context;
        mContents = contents;
        // Get the display manager service.
//        mDisplayManager = (DisplayManager) context.getSystemService(Context.DISPLAY_SERVICE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Be sure to call the super class.
        super.onCreate(savedInstanceState);

        // Get the resources for the context of the presentation.
        // Notice that we are getting the resources from the context of the presentation.
        Resources r = getContext().getResources();

        // Inflate the layout.
        setContentView(R.layout.activity_web);
        initWebContainerView();
        initWebView();
    }
    private void initWebContainerView(){
        secondWebView = (WebView) findViewById(R.id.activity_web_webview);
        sencondProgressBar = (ProgressBar) findViewById(R.id.activity_web_progress);
    }

    private void initWebView() {
        WebView webView = (WebView) findViewById(R.id.activity_web_webview);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.activity_web_progress);

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        webView.loadUrl("http://www.baidu.com");
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressBar.setVisibility(android.view.View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.setVisibility(android.view.View.GONE);
            }
        });

        webView.setWebChromeClient(new WebChromeClient());

        //禁止webView长按复制静态页面内容
        webView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return true;
            }
        });
    }

}