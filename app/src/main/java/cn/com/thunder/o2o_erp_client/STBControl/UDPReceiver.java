package cn.com.thunder.o2o_erp_client.STBControl;

import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Arrays;
import java.util.Enumeration;

import static cn.com.thunder.o2o_erp_client.STBControl.UDPConst.RESPONSE_DATA_MAX;

/**
 * @author qiuyanlong(email:276644135@qq.com)
 * @date 2017-05-19 13:35
 * @package cn.com.thunder.o2o_erp_client.STBControl
 */
public abstract class UDPReceiver extends Thread {
    private static final String TAG = "UDPReceiver";

    private int mUserDataMaxLen;

    private volatile boolean mOpenFlag;
    private volatile boolean dealWith;

    private DatagramSocket mSocket;

    /**
     * 构造函数
     * 不需要用户数据
     */
    public UDPReceiver() {
        this(RESPONSE_DATA_MAX);
    }

    /**
     * 构造函数
     *
     * @param userDataMaxLen 搜索主机发送数据的最大长度
     */
    public UDPReceiver(int userDataMaxLen) {
        this.mUserDataMaxLen = userDataMaxLen;
    }

    /**
     * 打开
     * 即可以上线
     */
    public boolean open() {
        // 线程只能start()一次，重启必须重新new。因此这里也只能open()一次
        if (this.getState() != State.NEW) {
            return false;
        }

        dealWith = true;
        mOpenFlag = true;
        this.start();
        return true;
    }

    /**
     * 关闭，彻底退出
     */
    public void close() {
        mOpenFlag = false;
    }

    /**
     * 不关闭情况下，是否处理消息UDP
     */
    public void dealWithMessage(boolean dealWith) {
        this.dealWith = dealWith;
    }

    @Override
    public void run() {

        byte[] buf = new byte[32 + mUserDataMaxLen];
        printLog("设备开启");
        DatagramPacket recePack = null;
        try {
            mSocket = new DatagramSocket(2007);
            // 初始
            mSocket.setSoTimeout(5000);

            recePack = new DatagramPacket(buf, buf.length);
        } catch (SocketException e) {
            e.printStackTrace();
        }

        if (mSocket == null || mSocket.isClosed() || recePack == null) {
            return;
        }

        while (mOpenFlag) {
            if (dealWith) {
                try {
                    // waiting for search from host
                    mSocket.receive(recePack);
                    // verify the data
                    if (verifySearchData(recePack)) {
                        parseUserData(recePack);
                    }
                    Arrays.fill(buf,(byte)0);
                } catch (IOException e) {
                }
            }
        }

        mSocket.close();
        printLog("设备关闭或已被找到");
    }

    /**
     * 校验UDP是否是本机UDP广播
     */
    private boolean verifySearchData(DatagramPacket pack) {
        //TODO 此处可以对数据内容进行校验，成功才做解析数据
        InetSocketAddress inetSocketAddress = (InetSocketAddress) pack.getSocketAddress();
        String address = inetSocketAddress.getAddress().getHostAddress();
        return isOwnIp(address);
    }

    /**
     * 获取本机在的IP
     *
     * @param ip 需要判断的ip地址
     * @return true-是本机地址
     */
    public boolean isOwnIp(String ip) {
        if (ip == null) {
            return false;
        }
        String ownIp = getLocalHostIp();
        return !ip.equals(ownIp);
    }

    private String getLocalHostIp() {
        String ipAddress = "";
        try {
            Enumeration<NetworkInterface> en = NetworkInterface
                    .getNetworkInterfaces();
            // 遍历所用的网络接口
            while (en.hasMoreElements()) {
                NetworkInterface nif = en.nextElement();// 得到每一个网络接口绑定的所有ip
                Enumeration<InetAddress> inet = nif.getInetAddresses();
                // 遍历每一个接口绑定的所有ip
                while (inet.hasMoreElements()) {
                    InetAddress ip = inet.nextElement();
                    if (!ip.isLoopbackAddress()
                            && ip instanceof Inet4Address) {
                        return ip.getHostAddress();
                    }
                }
            }
        }
        catch(SocketException e)
        {
            Log.e("feige", "获取本地ip地址失败");
            e.printStackTrace();
        }
        return ipAddress;
    }

    /**
     * 解析用户数据
     * 默认返回true，如果调用者有自己的数据，需重写
     *
     * @return 解析结果
     */

    public abstract void parseUserData(DatagramPacket datagramPacket);


    /**
     * 打印日志
     * 由调用者打印，SE和Android不同
     */
    public abstract void printLog(String log);
} 