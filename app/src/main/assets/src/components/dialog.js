import React from 'react'
import { Modal, Button } from 'react-bootstrap'

/*
 * 对话框
 */
export default class Dialog extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            show: this.props.show,
            data: this.props.data
        }
    }

    onHide() {
        this.props.onHide()
    }

    render() {
        return (
            <Modal {...this.props} style={{position: 'absolute', top: '0', width: '1280px', height: '720px', background: 'rgba(0,0,0,0.4)',zIndex: '1000'}}>
                <Modal.Header closeButton>
                    <Modal.Title>{this.props.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                {this.props.children}
                </Modal.Body>
                <Modal.Footer>
                </Modal.Footer>
            </Modal>
        )
    }
}

Dialog.propTypes = {
    show: React.PropTypes.bool.isRequired,
    title: React.PropTypes.string.isRequired
}
