import React from 'react'
import COMMON from '../lib/common'

export default class ImgUpload extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            src: props.src,
            policy: '',
            signature: ''
        }
    }

    submitFrom(event) {
        event.preventDefault()
        let form = event.target
        fetch('http://coupon.ktvsky.com/by/upload').then((res) => {
            return res.json()
        }).then((res) => {
            if (res.errcode != 200) {
                this.warnning(res.errmsg)
                return
            }
            this.setState({
                policy: res.upyun[1],
                signature: res.upyun[0]
            }, () => {
                this.uploadImg(Zepto(form))
            })
        })
    }

    componentWillReceiveProps(props) {
        if(this.state.src == props.src){
            return
        }
        this.setState({
            src: props.src
        })
    }

    changeImg(event) {
        Zepto(event.target).parent().find('button').click()
    }

    changeValue(e) {

    }

    uploadImg(obj) {
        let form = obj
        const self = this
        Zepto.ajax({
            type: 'post',
            url: 'http://v0.api.upyun.com/autodynemv',
            processData: false,
            contentType: false,
            data: new FormData(form[0]),
            success: function(data) {
                const obj = JSON.parse(data)
                let url = obj.url
                if (obj.code != 200) {
                    alert('图片上传失败，请重新上传')
                    return
                }
                self.setState({
                    src: url
                })
            }
        });
    }

    render() {
        let data = this.state
        return (
            <div>
                <span><img src={data.src ? (COMMON.IMGHOST + data.src) : ''} alt='请选择图片'/></span>
                <form onSubmit={(e) => this.submitFrom(e) }>
                    <fieldset>
                        <input name='file' type='file' onChange={(e) => this.changeImg(e)}/>
                        <input name='policy' value={ data.policy } type="hidden" onChange={(e) => this.changeValue(e)}/>
                        <input name='signature' value={ data.signature} type="hidden" onChange={(e) => this.changeValue(e)}/>
                        <input name='pic' type='hidden' value={ data.src } onChange={(e) => this.changeValue(e)}/>
                        <button type='submit'></button>
                    </fieldset>
                </form>
                <p>{this.props.remark}</p>
            </div>
        )
    }
}
