let JSBridge = JSBridge || {}

JSBridge.test = function(funcName, parma, callback) {
    let datas = {}
    datas.data = parma ? parma : ''
    let promise = new Promise((resolve, reject) => {
        switch (funcName) {
            case 'printNotes':
                resolve(JSInterface.printNotes(JSON.stringify(datas)))
                break;
            case 'openBox':
                resolve(JSInterface.openBox())
                break;

            case 'startPayCode':
                resolve(JSInterface.startPayCode());
                break;

            case 'getPayCode':
                resolve(JSInterface.getPayCode());
                break;

            case 'endPayCode':
                resolve(JSInterface.endPayCode());
                break;

            case 'showSplitScreen':
                resolve(JSInterface.showSplitScreen(JSON.stringify(datas)));
                break;

            case 'open':
                console.log('open', datas)
                resolve(JSInterface.setSTBState(JSON.stringify(datas)))
                break
            default:
                break;
        }
    })
    promise.then((value) => {
        console.log('success print')
        callback(value)
    })
}

export default JSBridge
