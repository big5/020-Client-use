import LIBrary from './util'
import fetch from 'isomorphic-fetch'
import COMMON from './common'
let PAY = PAY || {}
//倒计时时间
PAY.count = 10

PAY.PAYPARMA = {
    op: "fastpay",
    ktvid: '',
    date: '',
    paytype: '', // wx / ali,
    mark: "0",
    other: "0",
    action: 'MICROPAY',
    data: {
        ktv_id: '',
        erp_id: '',
        paraAuthCode: '', // WXcode/ ALIcode
        paraTotalFee: 1, //支付金额（分）
        paraBody: 'qqqqqq', //商品描述
        paraBillCreateIp: ''
    },
    erpid: '',
    time: '', //当前时间戳
}

PAY.QUERY = {
    "op": "query",
    "ktvid": '',
    "paytype": "", //wx, ali
    "data": ''
}

PAY.payMothed = function(parma, callback) {
    parma.data.paraTotalFee = 1
    let strPay = ''
    let queryUrl = PAY.QUERY
    queryUrl.ktvid = parma.ktvid
    queryUrl.paytype = parma.paytype
    queryUrl.op = 'query'
    for (var key in parma) {
        strPay += ('&' + key + '=' +
        (typeof parma[key] == 'object' ?
        JSON.stringify(parma[key]) :
        parma[key].toString()))
    }
    strPay = '?' + strPay.substr(1)
    fetch(COMMON.PAYURL + parma['paytype'] + strPay,
    {method: 'post'})
    .then((res) => { return res.json()})
    .then((res) => {
        if(res.errcode != 200){
            LIBrary.warnning('支付失败')
            return
        }
        if (res.result_code === 'SUCCESS'
        && res.return_code === 'SUCCESS') {
            callback()
            return
        }
        queryUrl.data = JSON.stringify({paraOutTradeNo: res.order})
        PAY.querry(queryUrl, callback)
    })
}

PAY.querry = function(parma, callback) {
    PAY.count -- ;
    let queryStr = ''
    for (var key in parma) {
        queryStr += ('&' + key + '=' +
        (typeof parma[key] == 'object' ?
        JSON.stringify(parma[key]) :
        parma[key].toString()))
    }
    queryStr = '?' + queryStr.substr(1)
    fetch(COMMON.PAYURL + parma['paytype'] + queryStr , {method: 'post'})
    .then((res) => {return res.json()})
    .then((res) => {
        if (res.errcode == 200 &&
            res.trade_state === 'SUCCESS' &&
            res.return_code === 'SUCCESS') {
            callback()
            return
        }
        if(PAY.count <= 0){
            LIBrary.warnning('支付时间过长')
            return
        }
        console('querry')
        setTimeout(() => {
            PAY.querry(parma, callback)
        }, 1000)
    })
}

export default PAY

/*
    {
        op : fastpay,
        ktvid: 82479,
        date: 2017-05-16 10:00:00,
        paytype: WX  wx / ali,
        mark: something,
        other: asdfasd,
        action: MICROPAY,
        data: {
            ktv_id: 84579,
            erp_id: "eee",
            paraAuthCode: 13/20, WXcode/ ALIcode
            paraTotalFee: 100, 支付金额（分）
            paraBody: somgthing, 商品描述
            paraBillCreateIp: 'ddd',
        }
        erpid: '',
        time: '', 当前时间戳
    }


 */
