let COMMON = {
    TYPEIP: 'MAC',
    // TYPEIP: 'IP',
    DATE : new Date(),
    // PAYURL: '//pay.ktvsky.com/',
    PAYURL: 'http://pay.ktvsky.com/',
    // PAYURL: 'http://101.254.157.124:8911/',
    // URL : 'http://101.254.157.124:9090',//立刚
    // URL : 'http://192.168.0.172:8888',
    // URL : 'http://192.168.0.30:8088',
    // URL :'http://101.254.157.124:8888',//
    URL:'http://android.stage.ktvsky.com',
    // URL: 'http://localhost:8089',
    IMGHOST: 'http://cdn.ktvsky.com/',
    ORDERTYPE: ['微信', '支付宝', 'POS机'],
    WEEKS: ['周一', '周二', '周三', '周四', '周五', '周六', '周日'],
    TIMES:['00:00','00:30','01:00','01:30','02:00','02:30','03:00','03:30',
        '04:00','04:30','05:00','05:30','06:00','06:30','07:00','07:30',
        '08:00','08:30','09:00','09:30','10:00','10:30','11:00','11:30',
        '12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30',
        '16:00','16:30','17:00','17:30','18:00','18:30','19:00','19:30',
        '20:00','20:30','21:00','21:30','22:00','22:30','23:00','23:30',
        '23:59'],
    PRICEREG : (/^\d+(?:.\d{1,2})?$/),
    PHONEREG : (/^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/),
    MACREG: (/\b\d{12}\b/),
    IPREG: (/^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/),
    HMTIME:(/^(?:[01]\d|2[0-3])(?::[0-5]\d)$/),
    NUMBER:(/^\d{1,5}$/)
}

COMMON.YEAR = COMMON.DATE.getFullYear()
COMMON.MONTH = COMMON.DATE.getMonth() + 1 >= 10 ? COMMON.DATE.getMonth() + 1 : '0' + (COMMON.DATE.getMonth() + 1)
COMMON.DAY = COMMON.DATE.getDate() >= 10 ? COMMON.DATE.getDate() : '0' + COMMON.DATE.getDate()
COMMON.HOUR = COMMON.DATE.getHours() >= 10 ? COMMON.DATE.getHours() : '0' + COMMON.DATE.getHours()
COMMON.MINUTE = COMMON.DATE.getMinutes() >= 10 ? COMMON.DATE.getMinutes() : '0' + COMMON.DATE.getMinutes()
COMMON.SECONDS = COMMON.DATE.getSeconds() >= 10 ? COMMON.DATE.getSeconds() : '0' + COMMON.DATE.getSeconds()
COMMON.TIME = COMMON.YEAR  + '-' + COMMON.MONTH + '-' + COMMON.DAY +
' ' + COMMON.HOUR + ':' + COMMON.MINUTE + ':' + COMMON.SECONDS

export default COMMON
