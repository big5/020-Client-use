import PRINTMODUL from './printModul'
import JSBridge from './bridge'
import LIBrary from './util'
import COMMON from './common'
let PRINRNOTES = PRINRNOTES || {}
let HANDLE = HANDLE || {}
//房间处理
HANDLE.ROOM = function(list) {
    let room = []
    list.map((timeDetial, index) => {
        let item = {}
        item.timeInterval = timeDetial['st'] + '--' + timeDetial['ed']
        item.timeLength = timeDetial['minute']
        if (timeDetial['pack_id'] == 0) {
            item.timeUnitPrice = LIBrary.Fix2(timeDetial['money'] / timeDetial['minute'])
            item.timeSubtotal = LIBrary.Fix2(timeDetial['money'])
        } else {
            item.timeInterval += '(套)'
            item.timeUnitPrice = ''
            item.timeSubtotal = LIBrary.Fix2(timeDetial['money'])
        }
        room.push(item)
    })
    return room
}
//酒水处理
HANDLE.BEER = function(list) {
    let food = []
    let products = HANDLE.combineProduct(list)
    products.map((foodDetial, index) => {
        let item = {}
        item.beerName = foodDetial['name']
        item.beerSubtotal = LIBrary.Fix2(foodDetial['money'])
        item.beerCount = foodDetial['count']
        if (foodDetial['pack_id'] == 0) {
            item.beerUnitPrice = LIBrary.Fix2(foodDetial['money'] / foodDetial['count'])
        } else {
            item.beerName += '(套)'
            item.beerUnitPrice = LIBrary.Fix2(foodDetial['money'] / foodDetial['count'])
        }
        food.push(item)
    })
    return food
}
//合并
HANDLE.combineProduct = function(list) {
    let foods = []
    list.map((value, index) => {
        let flag = false
        foods.map((food, index) => {
            if (value['pack_id'] == 0 &&
            value['product_id'] == food['product_id']) {
                food['count'] += value['count']
                flag = true
                return
            }
            if (value['pack_id'] != 0 &&
            value['pack_id'] == food['pack_id']) {
                food['count'] += value['count']
                flag = true
                return
            }
        })
        if (!flag) {
            foods.push(value)
        }
    })
    return foods
}
// 打单
PRINRNOTES.print = function(data) {
    JSBridge.test('printNotes', data)
}
// 单笔帐单，结账前打单
PRINRNOTES.BFSingleBill = function(parma, billNo) {
    PRINRNOTES.CheckDetail(parma, 'BF', billNo, false)
}
// 单笔帐单，结账后打单
PRINRNOTES.AFSingleBill = function(orderNo, billNo) {
    LIBrary.fetch('/order/detail?order_no=' + orderNo +
    '&token=xElg2KGuuc0ESefLjx3wY7-m2WOjGnP1', {
        method: 'GET'
    }, (res) => {
        PRINRNOTES.CheckDetail(res.detail, 'AF', billNo, true)
    })
}
//总结账单
PRINRNOTES.AllBill = function(orderNo, billNo) {
    LIBrary.fetch('/order/detail?order_no=' + orderNo +
    '&token=xElg2KGuuc0ESefLjx3wY7-m2WOjGnP1', {
        method: 'GET'
    }, (res) => {
        PRINRNOTES.checkAll(res.detail, 'AF', billNo, true)
    })
}
//处理所有帐单逻辑
PRINRNOTES.checkAll = function(data, type, billNo, autograph) {
    let room = []
    let beer = []
    let totalPay = 0
    let hasPayMoney = 0
    let date = new Date()
    let detail = data
    let time = COMMON.TIME
    detail['bills'].map((value, index) => {
        let roomArry = []
        let beerArray = []
        if (billNo && billNo != value['bill_no']) {
            return
        }
        totalPay += value['money']
        hasPayMoney = value['real_money']

        if (value['service_type'] == 1) {
            room = room.concat(HANDLE.ROOM(value['list']))
        }

        if (value['service_type'] == 2) {
            beer = beer.concat(HANDLE.BEER(value['list']))
        }

        if (value['service_type'] == 3) {
            beer = beer.concat(HANDLE.BEER(value['list']))
        }
    })

    PRINTMODUL.room = room
    PRINTMODUL.beer = beer
    PRINTMODUL.titleType = '结账单'
    PRINTMODUL.OperationType = '结账'
    PRINTMODUL.roomName = detail.room_name
    PRINTMODUL.totalPay = LIBrary.Fix2(totalPay)
    PRINTMODUL.hasPayMoney = LIBrary.Fix2(hasPayMoney)
    PRINTMODUL.openTime = detail.update_time
    PRINTMODUL.originalBill = detail.order_no
    PRINTMODUL.shouldIncome = LIBrary.Fix2(totalPay - hasPayMoney)
    PRINTMODUL.checkoutTime = detail.update_time
    PRINTMODUL.autograph = autograph ? 1 : 0
    JSBridge.test('printNotes', PRINTMODUL)
}
//处理单笔帐单逻辑
PRINRNOTES.CheckDetail = function(data, type, billNo, autograph) {
    let totalPay = 0
    let hasPayMoney = 0
    let room = []
    let beer = []
    let date = new Date()
    let detail = data
    let time = COMMON.TIME
    detail['bills'].map((value, index) => {
        let roomArry = []
        let beerArray = []
        let orderType = value['bill_no'] ?
        value['bill_no'].substring(0, 2) : ''
        if (type == 'AF' &&
        value['bill_no'] != billNo) {
            return
        }
        switch (orderType) {
            case 'KT':
                totalPay += value['money']
                room = HANDLE.ROOM(value['list'])
                PRINTMODUL.titleType = '收款单'
                PRINTMODUL.OperationType = '开台'
                break;

            case 'DD':
                totalPay += value['money']
                beer = HANDLE.BEER(value['list'])
                PRINTMODUL.titleType = '收款单'
                PRINTMODUL.OperationType = '点单'
                break;

            case 'TD':
                totalPay += value['money']
                beer = HANDLE.BEER(value['list'])
                PRINTMODUL.titleType = '收款单'
                PRINTMODUL.OperationType = '退单'
                break;

            case 'XS':
                PRINTMODUL.titleType = '收款单'
                PRINTMODUL.OperationType = '续时'
                totalPay += value['money']
                beer = HANDLE.BEER(value['list'])
                break;

            case 'YF':
                PRINTMODUL.titleType = '收款单'
                PRINTMODUL.OperationType = '预付'
                PRINTMODUL.room = ''
                PRINTMODUL.beer = ''
                break

            case 'ZT':
                PRINTMODUL.titleType = '收款单'
                PRINTMODUL.OperationType = '转台'
                break

            case 'YF':
                PRINTMODUL.titleType = '收款单'
                PRINTMODUL.OperationType = '预付'
                break

            case 'JZ':
                PRINTMODUL.titleType = '收款单'
                PRINTMODUL.OperationType = '结账'
                break

            case 'BD':
                PRINTMODUL.titleType = '补打单'
                PRINTMODUL.OperationType = '补打小票'
                PRINTMODUL.shouldIncome = ''
                break

            case 'WL':
                PRINTMODUL.titleType = '预定(网络)'
                PRINTMODUL.OperationType = '网络订单'
                break

            default:
                break;
        }
    })

    PRINTMODUL.room = room
    PRINTMODUL.beer = beer
    PRINTMODUL.roomName = detail.room_name
    PRINTMODUL.totalPay = LIBrary.Fix2(totalPay)
    PRINTMODUL.hasPayMoney = LIBrary.Fix2(hasPayMoney)
    PRINTMODUL.openTime = detail.update_time
    PRINTMODUL.originalBill = detail.order_no
    PRINTMODUL.shouldIncome = LIBrary.Fix2(totalPay - hasPayMoney)
    PRINTMODUL.checkoutTime = detail.update_time
    PRINTMODUL.autograph = autograph ? 1 : 0
    PRINTMODUL.titleType += (detail['bills']['payType'] == 2 ? '(后结)' : '')
    console.log(PRINTMODUL)
    return
    JSBridge.test('printNotes', PRINTMODUL)
}
export {PRINRNOTES, HANDLE}
