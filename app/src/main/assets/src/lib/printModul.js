const backOrder = {
        "titleKTLeft": "", // 左头标题
        "titleKT": "",  // 右头标题
        "titleType": "",  // 退单
        "titleAD": "",  // 票据AD
        "titleSK": "", //票据SK
        "roomName": "",               //房台号
        "OperationType": "",        //操作类型
        "printFrequency": '',                //打印次数
        "openTime": "",    //开台时间
        "backTime": "",    // 退单时间
        "checkoutTime": "",//结账时间
        "roomPackage": "",       //房间套餐
        "originalBill": "",         //原始单号
        "room": [
            // {
            //     "timeInterval": "10:00-12:00",//时段
            //     "timeLength": "120",        //时长
            //     "timeUnitPrice": "10",      //时间单价
            //     "timeSubtotal": "600",      //时段小计
            // },
        ],
        "beer": [
            // {
            //     "beerName": "哈尔滨冰纯",    //酒水
            //     "beerUnitPrice": "120",     //酒水单价
            //     "beerCount": "10",          //酒水数量
            //     "beerSubtotal": "1200",     //酒水小计
            // },
        ],
        "totalPay": "",     //总计 1260
        "hasPayMoney": "",   //已付 500
        "discount": "",      //优惠 200
        "percent": "",        //打折 10
        "rounding": "",       //抹零 60
        "shouldIncome": "",  //应收/应退 500
        "realIncome": "",   //收款/退款 1000
        "refund": "",          // 找零 0
        "payType": "",           //支付方式 3
        "payTypeMoney": "",  //支付方式金额。 500
        "netReceivables": "",   //网络收款单号。 112541204156
        "reMarks": "",        //备注。 小费
        "minConsumption": "",        //最低消费。
        "autograph": ''
}

export default backOrder
