import fetch from 'isomorphic-fetch'
import JSBridge from './bridge'
import COMMON from './common'
import eventproxy from 'eventproxy'
let LIBrary = LIBrary || {}
LIBrary.eventproxy = new eventproxy()

LIBrary.fetch = (reqUrl, args, callback, isCached = false) => {
    if (isCached) {
        const cached = sessionStorage.getItem(url)
        if (cached) {
            return callback(JSON.parse(cached))
        }
    }
    if(reqUrl)
    reqUrl += localStorage.getItem('token') ?
            (reqUrl.indexOf('?') > 0 ?
            '&token=' + localStorage.getItem('token') :
            '?token=' + localStorage.getItem('token')) : ''
    args['mode'] = 'cors'
    setTimeout(() => {
        fetch(COMMON.URL + reqUrl, args).then((res) => {
            return res.json()
        }).then((data) => {

            if (data.errcode == 10002) {
                window.location = '../init_setup/index.html#/login'
                return
            }

            if (data.errcode != 200 ) {
                LIBrary.warnning(data.errmsg)
                return
            }

            if (isCached) {
                sessionStorage.setItem(url, JSON.stringify(data))
            }

            if(args.method.toLowerCase() == 'post'){
                LIBrary.warnning('保存成功')
            }

            if(args.method.toLocaleLowerCase() == 'put'){
                LIBrary.warnning('修改成功')
            }

            if(args.method.toLocaleLowerCase() == 'delete'){
                LIBrary.warnning('删除成功')
            }
            callback(data)
        }).catch(err => {
            // window.location.href = '../room_setup/index.html#/wrongPage'
            alert(err)
        })
    }, 200)
}

LIBrary.times = (i, callback, l = i) => {
    if (i === 0)
        return

    callback(l - i)
    LIBrary.times(i - 1, callback, l)
}

LIBrary.removeByIndex = (arr, index) => {
    let item = []
    arr.map((value, i) => {
        if (i != index) {
            item.push(arr[i])
        }
    })
    console.log("\n item is :"+item)
    return item
}

LIBrary.changeByIndex = (arr, index, val) => {
    let item = []
    arr.map((value, i) => {
        if (i == index) {
            arr[i] = val
        }
        item.push(arr[i])
    })
    return item
}

LIBrary.checkByValue = (arr, val) => {
    let flag = false
    arr.map((value, i) => {
        if (value = val) {
            flag = true
        }
    })
    return flag
}

LIBrary.creatArray = (len, num) => {
    let arr = [];
    for (let i = 0; i < len; i++) {
        arr.push(num)
    }
    return arr
}

//判断时间大小
LIBrary.exDateRange = (sDate1, sDate2) => {
    var iDateRange;
    if (sDate1 != "" && sDate2 != "") {
        var startDate = sDate1.replace(/-/g, "/");
        var endDate = sDate2.replace(/-/g, "/");
        var S_Date = new Date(Date.parse(startDate));
        var E_Date = new Date(Date.parse(endDate));
        iDateRange = (S_Date - E_Date) / 86400000;
    }
    return iDateRange;
}

LIBrary.warnning = (str) => {
    LIBrary.eventproxy.emit('warnMsg',str)
}

LIBrary.countDown = (str) => {
    LIBrary.eventproxy.emit('countDown',str)
}

LIBrary.countDown = (str) => {
    LIBrary.eventproxy.emit('closeCountDown',str)
}

LIBrary.Fix2 = (data) => {
    return (data/100).toFixed(2)
}

export default LIBrary
