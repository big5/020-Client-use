import JSBridge from './bridge'
var ROOMOPERATION =  ROOMOPERATION || {}
ROOMOPERATION.handleRoom = function(OperationType, mac, time, state) {
    let parma = {
        "mac": mac, //mac地址
        "command": '', //操作类型 (开台：056、续台：057、关台：058、关机：059、重启：060)
        "time": time, //时长（倒计时开台，一直开台，续台时间）
        "type": state, //开台类型
    }

    switch (OperationType) {
        case 'open':
            parma.command = '056'
            break
        case 'continue':
            parma.command = '057'
            break
        case 'close':
            parma.command = '058'
            break
        case 'shutDown':
            parma.command = '059'
            break
        case 'reOpen':
            parma.command = '060'
    }

    JSBridge.test('open', parma)
}
