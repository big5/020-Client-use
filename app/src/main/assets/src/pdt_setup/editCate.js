import React from 'react'
import {REQUEST_URLS} from './setting'
import LIBrary from '../lib/util'
import Dialog from '../components/dialog'
export default class EditCate extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            cate: '',
            showDialog: '',
        }
    }

    componentWillReceiveProps(props){
        if(this.state.cate == props.cate &&
            this.state.showDialog == props.type){
            return
        }
        this.setState({
            cate: props.cate,
            showDialog: props.type
        })
    }

    cancelAddCate(){
        this.props.cancelAddCate()
    }

    deleteCates() {
        this.props.deleteCates()
    }

    putCates( name){
        this.props.putCates( name)
    }

    submitAddCates(event) {
        event.preventDefault()
        const obj = event.target
        const form = Zepto(event.target).parent()
        switch (obj.name) {
            case 'put':
                let form_data = {}

                form.find('input').map((index, input) => {
                    if(input.value == ''){
                        return
                    }
                    form_data[input.name] = input.value
                })

                if(form_data.name == ''){
                    LIBrary.warnning('请填写要修改酒水分类名称')
                    return
                }

                LIBrary.fetch(REQUEST_URLS.cate + '?cate_id='+ form_data['cate_id'], {
                    method: 'put',
                    body: JSON.stringify({name: form_data['name']})
                }, (res) => {
                    this.putCates(form_data.name)
                    this.cancelAddCate()
                })
                break;
            case 'delete':
                const cateId = form.find('input[name=cate_id]').val()
                LIBrary.fetch(REQUEST_URLS.cate, {
                    method: 'delete',
                    body: JSON.stringify({cate_ids: [cateId]})
                }, (res) => {
                    this.deleteCates()
                    this.cancelAddCate()
                })
                break;
        }
    }

    templateEditCates(params) {
        let cateId = params && params['cate_id'] ? params['cate_id'] : ''
        let name = params && params['name'] ? params['name'] : ''
        return(
            <form onSubmit={(e) => this.submitAddCates(e)} className='addRoombox'>
                <h1>修改酒水</h1>
                <input name='cate_id' defaultValue={ cateId } hidden />
                <input name='name' defaultValue={ name } placeholder='请输入商品分类，如酒水'/>
                <button type='submit' name='put' onClick={ (e) => this.submitAddCates(e) } className='dialogLittltButton dialogLittltButtonChange'>修改</button>
                <button type='submit' name='delete' onClick={ (e) => this.submitAddCates(e) } className='dialogLittltButton dialogLittltButtonDelete'>删除</button>
            </form>
        )
    }

    render() {
        return (
            <Dialog show={ this.state.showDialog == 'edit'}
                children={ this.templateEditCates(this.state.cate) }
                title={''}
                onHide = {(e) => this.cancelAddCate() }/>
        )
    }
}
