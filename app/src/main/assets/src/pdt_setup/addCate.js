import React from 'react'
import {REQUEST_URLS} from './setting'
import LIBrary from '../lib/util'
import Dialog from '../components/dialog'

export default class addCates extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            showDialog: '',
            count: 4
        }
    }

    componentWillReceiveProps(props){
        if(this.state.showDialog == props.type){
            return
        }
        this.setState({
            showDialog: props.type,
            count: 4
        })
    }

    getMore() {
        this.setState({
            count: this.state.count + 2
        })
    }

    deleteInput(e) {
        const LI = Zepto(e.target).parent()
        LI.find('input')[0].value = ''
    }

    cancelAddCate(){
        this.props.cancelAddCate()
    }

    addCates(list) {
        this.props.addCates(list)
    }

    submitAddCates(event) {
        event.preventDefault()
        const obj = event.target
        const form = Zepto(event.target).parent()
        let values = []
        form.find('input').map((index, input) => {
            if(input.value == ''){
                return
            }
            values.push(input.value)
        })
        if(values.length < 1){
            LIBrary.warnning('请填写要添加酒水分类名称')
            return
        }
        LIBrary.fetch(REQUEST_URLS.cate, {
            method: 'POST',
            body: JSON.stringify({names: values})
        }, (res) => {
            this.addCates(res.cates)
            this.cancelAddCate()
        })
    }

    templateCatesInput() {
        var items = []
        LIBrary.times(this.state.count, (index) => {
            items.push(
                <li key={index} className='singleLi'>
                    <input className='singleInput' placeholder='请输入商品分类，如酒水'/>
                    <span onClick={(index) => this.deleteInput(index)} className='delete3'></span>
                </li>
            )
        })
        return items
    }

    templateAddCates() {
        return (
            <form className='addRoombox'>
                <h1>添加酒水</h1>
                <ul>{this.templateCatesInput()}</ul>
                <p className='add' onClick={() => this.getMore()}>添加酒水</p>
                <button type='submit' name='post' onClick={ (e) => this.submitAddCates(e) } className='dialogLittltButton'>完成</button>
            </form>
        )
    }

    render() {
        return (
            <Dialog show={ this.state.showDialog  == 'add'}
                children={ this.templateAddCates() }
                title={''}
                onHide = {(e) => this.cancelAddCate() }/>
        )
    }
}
