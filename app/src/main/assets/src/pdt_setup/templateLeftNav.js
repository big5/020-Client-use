import React from 'react'
import LIBrary from '../lib/util'

export default class TemplateLeftNav extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            cates: [],
            currentCates: 0
        }
    }

    componentWillReceiveProps(props) {
        if(this.state.cates == this.props.cates){
            return
        }
        let arr = props.cates
        this.setState({
            cates: arr.concat([{name: '套餐'}])
        })
    }

    chooseCates(index) {
        this.setState({
            currentCates: index
        })
        this.props.chooseCates(index)
    }

    templateCates(params) {
        var items = []
        LIBrary.times(params.length, (idx) => {
            let name = params[idx] && params[idx]['name'] ? params[idx]['name'] : ''
            items.push(
                <li key={ idx }
                    className={'cursor' +' '+ ( idx == this.state.currentCates ? 'active' : '') }
                    onClick={(type) => this.chooseCates(idx)}> { name } </li>
            )
        })
        return items
    }

    render() {
        return(
            <ul className='roomTypeSetup'>
                {this.templateCates(this.state.cates)}
            </ul>
        )
    }
}
