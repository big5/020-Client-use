import React from 'react'
import LIBrary from '../lib/util'
import Zepto from 'npm-zepto'
import Checkbox from './checkbox'
import {REQUEST_URLS} from './setting'
import ProductCell from './productCell'

export default class Single extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            product: [],
            cateId: props.cateId,
        }
    }

    componentWillReceiveProps(props) {
        if(props.cateId == this.state.cateId || !props.cateId){
            return
        }
        this.getCateInfo(props.cateId)
    }

    chooseAll(event){
        if(event.target.checked) {
            Zepto('input.choose').attr('checked', 'checked')
        }else{
            Zepto('input.choose').removeAttr('checked')
        }
    }

    resetProduct(index, value) {
        let arr = LIBrary.removeByIndex(this.state.product, this.state.product.length -1)
        this.setState({
            product: arr.concat(value)
        })
    }

    getCateInfo(data) {
        LIBrary.fetch(REQUEST_URLS.product + '?cate_id=' + data, {
            method: 'GET'
        }, (res) => {
            this.setState({
                cateId: data,
                product: res.list
            })
        })
    }

    putProductsDetails(value, index) {
        let arr = this.state.product
        arr[index] = value
        this.setState({
            product: arr
        },() => {
            console.log(arr)
        })
    }

    showProductAdd() {
        this.setState({
            product: this.state.product.concat([''])
        })
    }

    cancelAddProduct() {
        let arr = LIBrary.removeByIndex(this.state.product, this.state.product.length-1)
        this.setState({
            product: arr
        })
    }

    onlineProduct(value, form_data, state){
        LIBrary.fetch(REQUEST_URLS.product, {
            method: 'PUT',
            body: JSON.stringify(form_data)
        }, (res) => {
            let arr = this.state.product
            arr.map((product, index) => {
                if(value.indexOf(product['product_id']) < 0 ){
                    return
                }
                arr[index]['state'] = state
            })
            this.setState({
                product: arr
            })
        })
    }

    putProducts(event){
        const method = event.target.className
        const checkbox = Zepto('input.choose')
        let value = []
        let form_data = []
        let state = method == 'offline' ? 0 : 1
        checkbox.map((index, input) => {
            if(input.checked){
                value.push(this.state.product[index]['product_id'])
                form_data.push({product_id: this.state.product[index]['product_id'], state: state})
            }
        })
        switch (method) {
            case 'delete':
                LIBrary.fetch(REQUEST_URLS.product, {
                    method: 'DELETE',
                    body: JSON.stringify({product_ids: value})
                }, (res) => {
                    let arr = []
                    this.state.product.map((product, index) => {
                        if(value.indexOf(product['product_id']) >=0 ){
                            return
                        }
                        arr.push(product)
                    })
                    this.setState({
                        product: arr
                    })
                })
                break;
            case 'offline' :
                this.onlineProduct(value, form_data, state)
                break
            case 'online' :
                this.onlineProduct(value, form_data, state)
                break;
        }
    }

    templateProducts(params) {
        let items = []
        LIBrary.times(params.length, (idx) => {
            items.push(
                <ProductCell
                    index={idx} key={idx}
                    product={params[idx]}
                    putProductsDetails = {(value, idx) => this.putProductsDetails(value, idx) }
                    cancelAddProduct={(e) => this.cancelAddProduct()}
                    resetProduct={(idx, value) => this.resetProduct(idx, value)}
                    cateId={this.state.cateId} />
            )
        })
        return items
    }

    render() {
        let data = this.state
        return (
            <div className="Single">
                <p className="SingleTitle">
                    <span>单品管理</span>
                    <span className='content'>
                        <input type='checkbox' onClick={(e) => this.chooseAll(e)} /> 全部
                    </span>
                    <span className='online' onClick={ (e) => this.putProducts(e)}>上架</span>
                    <span className='offline' onClick={ (e) => this.putProducts(e)}>下架</span>
                    <span className='deleteSingle'  onClick={ (e) => this.putProducts(e)}>删除</span>
                </p>
                <div className="commodityDetail">
                    <div className="commodityDetailTitle">
                        <ul>
                            <li></li>
                            <li>商品图</li>
                            <li>商品名称</li>
                            <li>单价</li>
                            <li>库存</li>
                            <li>状态</li>
                            <li>操作</li>
                        </ul>
                    </div>
                    <section>
                        {this.templateProducts(data.product)}
                    </section>
                </div>
                <span onClick={(e) => this.showProductAdd(e)} className="addSingle">
                    +添加单品
                </span>
            </div>
        )
    }
}
