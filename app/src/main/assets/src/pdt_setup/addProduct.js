import React from 'react'
import {Link} from 'react-router'
import LIBrary from '../lib/util'
import COMMON from '../lib/common'

import Dialog from '../components/dialog'
import ImgUpload from '../components/imgUpload'
import Zepto from 'npm-zepto'

import Checkbox from './checkbox'
import {REQUEST_URLS} from './setting'

export default class productCell extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            cateId: this.props.cateId,
            product: this.props.product,
            currentProduct: this.props.currentProduct,
        }
    }

    componentWillReceiveProps(props){
        if(this.state.cateId == props.cateId &&
         this.state.product == props.product){
            return
        }
        this.setState({
            cateId: props.cateId,
            product: props.product,
            index: props.index,
            currentProduct: props.currentProduct
        })
    }

    putProducts(value){
        this.props.putProducts(value)
    }

    cancelAddProduct() {
        this.props.cancelAddProduct()
    }

    saveEditProduct(event) {
        const form = Zepto(event.target).parents('.addProductContainer')
        let values = {}
        let flag = false
        let str = ''
        let form_data = ''
        let method = form.find('input[name=product_id]').attr('value') ? 'put' : 'post'
        form.find('input').map((index, input) => {
            if (input.value == '' ) {
                return
            }
            if (input.name == 'price' && !COMMON.PRICEREG.test(input.value)) {
                flag = true
                str = '输入金额最多两位小数'
                return
            }
            values[input.name] = input.value
        })
        if (flag) {
            LIBrary.warnning(str)
            return
        }
        values.price *= 100
        values.discount = form.find('input[type=Checkbox]').attr('checked') ? 1 : 0
        form_data = method=='put' ? [values] : values
        LIBrary.fetch(REQUEST_URLS.product + '?cate_id=' + this.state.cateId, {
            method: method,
            body: JSON.stringify(form_data)
        }, (res) => {
            let value = values
            if(method == 'post'){
                value = res.detail
            }
            this.putProducts(value)
        })
    }

    render() {
        let data = this.state.product ? this.state.product : ''
        let price = data && data['price'] ? (data['price']/100).toFixed(2) : ''
        let productId = data['product_id'] ? data['product_id'] : ''
        let name = data['name'] ? data['name'] : ''
        let spec = data['spec'] ? data['spec'] : ''
        let unit = data['unit'] ? data['unit'] : ''
        let stock = data['stock'] || data['stock'] == 0 ? data['stock'] : ''
        let state = data && data['state'] == 0 ? 0 : 1
        let pic = data['pic'] ? data['pic'] : ''
        let discount = data['discount'] ? data['discount'] : 0
        return (
            <ul className='addProductContainer'>
                <li className="chooseCon">
                </li>
                <li className="imgCon">
                    <ImgUpload src={ pic }/>
                </li>
                <li className="nameCon">
                    <input type='text' name='name' defaultValue={ name }
                    placeholder='请输入商品名称，限16个字'/>
                    <input type='hidden' name='product_id' defaultValue={ productId } />
                </li>
                <li className="standardAndUnit">
                    <input type='text' name='spec' defaultValue={ spec } placeholder='规格'/>
                    <input type='text' name='unit' defaultValue={ unit } placeholder='单位'/>
                </li>
                <li className="stockAndprice">
                    <input type='text' name='stock' defaultValue={ stock }  placeholder='库存量'/>
                    <input type='text' name='price' defaultValue={ price } placeholder='单价'/>
                </li>
                <li className="operaCon">
                    <span>
                        <input type='text' name='state' defaultValue={ state } hidden />
                        <input type='checkbox' defaultChecked={discount ? true  : false} />
                        参与打折
                    </span>
                    <span>
                        <button onClick={(e) => this.saveEditProduct(e)}>保存</button>
                        <button onClick={(e) => this.cancelAddProduct(e)}>取消</button>
                    </span>
                </li>
            </ul>
        )
    }
}
