'use strict';
import React from 'react'
import LIBrary from '../lib/util'
import COMMON from '../lib/common'
import {REQUEST_URLS} from './setting'
import ImgUpload from '../components/imgUpload'
import PackDetail from '../room_setup/packDetail'

export default class AddMeal extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            roomType: props.roomType,
            index: props.index,
            meal: props.meal
        }
    }

    componentWillReceiveProps(props) {
        if(this.state.meal == props.meal &&
            this.state.roomType == props.roomType){
            return
        }
        this.setState({
            meal: props.meal,
            roomType: props.roomType
        })
    }

    cancelAddMeal(){
        this.props.cancelAddMeal()
    }

    putMeal(value) {
        this.props.putMeal(value)
    }

    editCateMeal(event) {
        const form = Zepto(event.target).parents('.submitFrom')
        const method = event.target.name
        let form_data = {}
        let weeks = []
        let roomTypes = []
        let list = []
        let flag = false
        let str = ''
        form.find('input').map((index, input) => {
            let text = !Zepto(input).val() ?
             Zepto(input).attr('value') :
             Zepto(input).val()

            if (text == '') {
                return
            }

            if (input.name == 'price' &&
            !COMMON.PRICEREG.test(text)) {
                flag = true
                str = '请输入正确金额，最多两位小数'
                return
            }

            if(input.name == 'end_date' &&
            LIBrary.exDateRange(form_data['start_date'],
            text)>0){
                flag = true
                str = '开始日期不能大于结束日期'
                return
            }

            form_data[input.name] = text
        })
        form.find('ul.foods').find('li').map((index, li) => {
            list.push({
                product_id: Zepto(li).attr('data-productId'),
                count: Zepto(li).attr('data-count') == '不限' ? 0 : Zepto(li).attr('data-count')
            })
        })
        form.find('button.weeks').map((index, btn) => {
            if(!Zepto(btn).hasClass('active')){
                return
            }
            weeks.push(index + 1)
        })
        form.find('button.roomType').map((index, btn) => {
            if(!Zepto(btn).hasClass('active')){
                return
            }
            roomTypes.push(this.state.roomType[index]['rt_id'])
        })
        if(flag){
            LIBrary.warnning(str)
            return
        }
        form_data.day = weeks.toString()
        form_data.rt_ids = roomTypes.toString()
        form_data.list = list
        form_data.price *= 100
        form_data['policy'] ?
        delete(form_data['policy']) :''
        form_data['signature'] ?
        delete(form_data['signature']) : ''
        form_data['file'] ?
        delete(form_data['file']) : ''
        form_data = method == 'post' ? form_data : [form_data]
        LIBrary.fetch(REQUEST_URLS.pack, {
            method: method,
            body: JSON.stringify(form_data)
        }, (res) => {
            let value = ''
            if(method == 'post'){
                value = res.detail
            }else{
                value = form_data[0]
            }
            this.putMeal(value)
        })
    }

    chooseBtn(event) {
        const obj = event.target
        Zepto(obj).hasClass('active') ?
        Zepto(obj).removeClass('active') :
        Zepto(obj).addClass('active')
    }

    templateweekday(meal) {
        let items = []
        LIBrary.times(COMMON.WEEKS.length, (idx) => {
            let days = meal ? meal['day'] : ''
            let className = days.indexOf((idx+1)) >= 0 ? 'active' : ''
            items.push(
                <button key={idx} type='button'
                    className={'cursor weeks '+ className}
                    onClick={(e) => { this.chooseBtn(e)}}>
                    { COMMON.WEEKS[idx] }
                    <span></span>
                </button>
            )
        })
        return items
    }

    templateRoomType(meal) {
        let items = []
        LIBrary.times(this.state.roomType.length, (idx) => {
            let roomType = this.state.roomType[idx]
            let rtIds = meal ? meal['rt_ids'] : ''
            items.push(
                <button key={idx} type='button' className={'cursor' + ' ' + 'roomType'+ ' ' +
                    (rtIds.indexOf(roomType['rt_id']) >= 0 ? 'active' : '')}
                    onClick={(e) => { this.chooseBtn(e)}}>
                    {roomType['name']}
                    <span></span>
                </button>
            )
        })
        return items
    }

    render() {
        let data = this.state
        let meal = data.meal
        let name = meal && meal['name'] ? meal['name'] : ''
        let packId = meal && meal['pack_id'] ? meal['pack_id'] : ''
        let pic = meal && meal['pic'] ? meal['pic'] : ''
        let startDate = meal && meal['start_date'] ? meal['start_date'] : ''
        let endDate = meal && meal['end_date'] ? meal['end_date'] : ''
        let st = meal && meal['st'] ? meal['st'] : ''
        let ed = meal && meal['ed'] ? meal['ed'] : ''
        let price = meal && (meal['price'] || meal['price'] == 0) ?
        (meal['price']/100).toFixed(2) : ''
        return (
            <div className='submitFrom'>
                <p>
                    <span>正在编辑...</span>
                    <button onClick={ (e) => this.cancelAddMeal()} type='button'> 取消 </button>
                    <button name={meal && (packId || packId==0) ? 'put' : 'post'} onClick={(e) => {this.editCateMeal(e)}}>保存</button>
                </p>
                <p className='mealName'><label>套餐名称：</label>
                    <input type='text' name='name' defaultValue={ name }/>
                    <input type='text' name='pack_id' defaultValue={ packId } style={{display: 'none'}}/>
                </p>
                <div className='upLoadImg'>
                    <ImgUpload src={ pic } remark={'备注：最近图片180*180px'}/>
                </div>
                <p>
                    <label>有效期：</label>
                    <input type='date' name='start_date' defaultValue={ startDate }/>
                    --
                    <input type='date' name='end_date' defaultValue={ endDate }/>
                </p>
                <p className='mealPrice'><label>价格：</label>
                    <input type='text' name='price' defaultValue={ price }/></p>
                <p><label>日期类别：</label>
                    {this.templateweekday(meal)}
                </p>
                <p><label>适用包房：</label>
                    {this.templateRoomType(meal)}
                </p>
                <p><label>可使用时段：</label>
                    <input type='time' name='st' defaultValue={ st }/>
                    --
                    <input type='time' name='ed' defaultValue={ ed }/>
                </p>
                <PackDetail packId={ packId }/>
            </div>
        )
    }
}
