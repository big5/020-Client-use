'use strict';
import React from 'react'
import LIBrary from '../lib/util'
import {REQUEST_URLS} from './setting'
import MealCell from './mealCell'

export default class Meal extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            cateId: this.props.cateId,
            meals: [],
            roomType: []
        }
    }

    componentWillReceiveProps(props) {
        if(props.cateId == this.state.cateId){
            return
        }
        this.getMeals()
    }

    componentDidMount() {
        this.getMeals()
        LIBrary.fetch(REQUEST_URLS.roomType, {
            method: 'GET'
        },(res) => {
            let arr = []
            res.list.map((value, index) => {
                arr.push({name:value['name'], rt_id: value['rt_id']})
            })
            this.setState({
                roomType: arr
            })
        })
    }

    putMeal(index, value) {
        let meal = this.state.meals
        meal[index] = value
        this.setState({
            meals: meal
        },(e) => {
            console.log(meal, value)
        })
    }

    cancelAddMeal() {
        let meals = this.state.meals
        this.setState({
            meals: LIBrary.removeByIndex(meals, meals.length-1)
        })
    }

    getMeals() {
        LIBrary.fetch(REQUEST_URLS.pack, {
            method: 'GET'
        }, (res) => {
            this.setState({meals: res.list})
        })
    }

    addMeal() {
        this.setState({
            meals: this.state.meals.concat([''])
        })
    }

    deleteMeal(index) {
        this.setState({
            meals: LIBrary.removeByIndex(this.state.meals, index)
        })
    }

    templateMeal(params) {
        let items = []
        LIBrary.times(params.length, (idx) => {
            items.push(
                <MealCell
                    key={idx}
                    index = {idx}
                    meal ={params[idx]}
                    roomType={ this.state.roomType }
                    putMeal ={(idx, value) => this.putMeal(idx, value) }
                    deleteMeal= { (idx) => this.deleteMeal(idx) }
                    cancelAddMeal= { () => this.cancelAddMeal() }
                    />
            )
        })
        return items
    }

    render() {
        let data = this.state
        return (
            <div className="room_meal">
                <h1 className="setuptitle">套餐设置</h1>
                { this.templateMeal(data.meals) }
                <button onClick={ () => { this.addMeal() } } className='addtime addDrinksMeal'> 添加套餐 </button>
            </div>
        )
    }
}
