const REQUEST_URLS = {
    cate: '/cate',
    product: '/cate/product',
    pack: '/pack',
    roomType: '/room/type'
}

export {REQUEST_URLS}
