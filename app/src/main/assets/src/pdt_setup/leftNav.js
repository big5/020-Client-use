import React from 'react'
import LIBrary from '../lib/util'
import {REQUEST_URLS} from './setting'
import EditCate from './editCatesContainer'
import TemplateLeftNav from './templateLeftNav'

export default class leftNav extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            currentCates: 0,
            cates: []
        }
    }

    componentWillMount(){
        LIBrary.fetch(REQUEST_URLS.cate, {
            method: 'GET'
        }, (res) => {
            this.setState({cates: res.list}, () => {
                this.chooseCates(0)
            })
        })
    }

    addCates(list) {
        let cates = this.state.cates
        this.setState({
            cates: this.state.cates.concat(list)
        })
        if(cates.length == 0){
            this.chooseCates(0)
        }
    }

    deleteCates() {
        let arr = LIBrary.removeByIndex(this.state.cates, this.state.currentCates)
        this.setState({
            cates: arr
        })
        if(arr.length == 0){
            location.reload(true)
        }
    }

    putCates(name) {
        let arr = this.state.cates
        arr[this.state.currentCates]['name'] = name
        this.setState({
            cates: arr
        })
    }

    chooseCates(index) {
        this.setState({
            currentCates: index
        })
        let cateId = typeof this.state.cates[index] == 'object' &&
                    (this.state.cates[index]['cate_id'] ||
                    this.state.cates[index]['cate_id'] == 0 )?
                    this.state.cates[index]['cate_id'] : ''
        this.props.chooseCates(cateId)
    }

    render() {
        let data = this.state
        let cate = typeof data.cates[data.currentCates] == 'object' ?
                    data.cates[data.currentCates] : ''
        return(
            <div className='roomTypeSetupContain'>
                <TemplateLeftNav cates={this.state.cates}
                chooseCates={(index) => this.chooseCates(index)}/>
                <EditCate cate={cate}
                    deleteCates={() => this.deleteCates()}
                    putCates={(name) => this.putCates(name)}
                    addCates={(list) => this.addCates(list)}/>
            </div>
        )
    }
}
