import React from 'react'
import LIBrary from '../lib/util'
import AddCate from './addCate'
import EditCate from './editCate'

export default class leftNav extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            cate: {},
            showDialog: ''
        }
    }

    componentWillReceiveProps(props) {
        if (props.cate == '' ||
        props.cate == this.state.cate) {
            return
        }
        this.setState({cate: props.cate})
    }

    EditCates(type) {
        this.setState({
            showDialog : type
        })
    }

    cancelAddCate() {
        this.setState({
            showDialog : ''
        })
    }

    addCates(list) {
        this.props.addCates(list)
    }

    deleteCates() {
        this.props.deleteCates()
    }

    putCates(value) {
        this.props.putCates(value)
    }

    render() {
        return (
            <section>
                <ol className='EditeRoomType'>
                    <li>
                        <div onClick={() => this.EditCates('add')} className='cursor newRoom'>
                            <span>+</span>
                            <span>新建品类</span>
                        </div>
                        <div onClick={() => this.EditCates('edit')} className='newRoom'>
                            <span className='editRoom'></span>
                            <span>修改品类</span>
                        </div>
                    </li>
                </ol>
                <AddCate addCates={(list) => this.addCates(list)}
                cancelAddCate={() => this.cancelAddCate()}
                type={this.state.showDialog}/>
                <EditCate cate={this.state.cate}
                    cancelAddCate={() => this.cancelAddCate()}
                    deleteCates={() => this.deleteCates()}
                    putCates={(value) => this.putCates(value)}
                    type={this.state.showDialog}/>
            </section>
        )
    }
}
