require('./app.scss')
require('../room_setup/app.scss')

import React from 'react'
import ReactDOM from 'react-dom'
import {Router, Route, hashHistory, Link, IndexRedirect} from 'react-router'
import Container from './container.js'
ReactDOM.render((
    <Router history={hashHistory}>
        <Route path='/' >
            <IndexRedirect to='/product' />
        </Route>
        <Route path='/product' component={ Container }/>
    </Router>
), document.getElementById('body'))
