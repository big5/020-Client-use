import React from 'react'
import Title from '../room_setup/title'
import LeftNav from './leftNav'
import Single from './Single'
import Meal from './meal'
export default class container extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            cateId: '',
        }
    }

    chooseCates(cateId) {
        this.setState({
            cateId: cateId
        })
    }

    render() {
        let display = !this.state.cateId ? 'block' : 'none'
        let displayMeal = this.state.cateId? 'block' : 'none'
        console.log('container', display,'display', 'displayMeal',displayMeal)
        return (
            <div className='room'>
                <Title/>
                <div className='DrinksDown'>
                    <LeftNav chooseCates={(cateId) => this.chooseCates(cateId)}/>
                    <div className='setUpRight'>
                        <div style={{display: display}}>
                            <Meal cateId={this.state.cateId}/>
                        </div>
                        <div style={{display: displayMeal}}>
                            <Single cateId={this.state.cateId} />
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}
