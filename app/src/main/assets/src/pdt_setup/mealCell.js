'use strict';
import React from 'react'
import {REQUEST_URLS} from './setting'
import LIBrary from '../lib/util'
import AddMeal from './addMeal'

export default class MealCell extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            meal: this.props.meal,
            index: this.props.index,
            roomType: this.props.roomType,
            isEditMeal: this.props.meal['pack_id'] ? false : true
        }
    }

    componentWillReceiveProps(props){
        this.setState({
            meal: props.meal,
            index: props.index,
            roomType: props.roomType,
            isEditMeal: props.meal['pack_id'] ? false : true
        })
    }

    putMeal(meal) {
        this.setState({
            meal: meal,
            isEditMeal: false
        })
        this.props.putMeal(this.state.index,meal)
    }

    EditMeal() {
        let flag = !this.state.isEditMeal
        this.setState({
            isEditMeal: flag
        })
    }

    cancelAddMeal(){
        this.EditMeal()
        if(!this.state.meal['pack_id']){
            this.props.cancelAddMeal()
        }
    }

    deleteMeal(index) {
        LIBrary.fetch(REQUEST_URLS.pack,{
            method: 'DELETE',
            body: JSON.stringify({pack_ids: [this.state.meal['pack_id']]})
        },(res) => {
            this.props.deleteMeal(index)
        })
    }

    render() {
        let data = this.state.meal
        let name = data && data['name'] ? data['name'] : ''
        let startDate = data && data['start_date'] ? data['start_date'] : ''
        let endDate =  data && data['end_date'] ? data['end_date'] : ''
        let price =  data && ( data['price'] || data['price'] == 0) ? (data['price'] / 100).toFixed(2) : ''
        let style1 = { display: ( data['pack_id'] ? 'block' : 'none') }
        let hide = this.state.isEditMeal ? '' : 'hide'
        this.state.isEditMeal ? console.log('no hide') : console.log('has hide')
        return (
            <div>
                <ul className='edited' style={ style1 }>
                    <li>
                        <p>{name}</p>
                        <p>有效期：{startDate} 至 {endDate}</p>
                    </li>
                    <li>
                        { price }元
                    </li>
                    <li>
                        <span>上架</span>
                        <span>下架</span>
                        <span name='delete' onClick={() => { this.deleteMeal(this.state.index) }}>删除</span>
                        <span name='put' onClick={() => { this.EditMeal()}}>修改</span>
                    </li>
                </ul>
                <div className={'editing'+' ' + hide }>
                    <AddMeal
                        index={this.state.index}
                        roomType={this.state.roomType}
                        meal={ data }
                        putMeal={ (meal) => this.putMeal(meal) }
                        cancelAddMeal={ () => this.cancelAddMeal()}/>
                </div>
            </div>
        )
    }
}
