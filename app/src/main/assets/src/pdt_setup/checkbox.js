import React from 'react'

export default class checkbox extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            className: false
        }
    }

    choosed(e) {
        this.setState({
            className: !this.state.className
        })
    }

    render() {
        return (
            <i className={ 'checkBefo'+ ' '+this.props.className + ' ' + ( this.state.className ? 'check-icon' : '' )} onClick={ (e) => this.choosed(e) }></i>
        )
    }
}
