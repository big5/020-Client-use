import React from 'react'
import Checkbox from './checkbox'
import AddProduct from './addProduct'
import COMMON from '../lib/common'
import LIBrary from '../lib/util'

export default class productCell extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            cateId: this.props.cateId,
            product: this.props.product,
            currentProduct: this.props.index,
            isEditProduct: this.props.product['product_id'] ? false : true,
            checked: false
        }
    }

    checked(e) {
        let checked = e.target.checked
        this.setState({
            checked: checked
        })
    }

    componentWillReceiveProps(props){
        if(this.state.cateId == props.cateId &&
         this.state.product == props.product &&
         this.state.currentProduct == props.index){
            return
        }
        this.setState({
            cateId: props.cateId,
            product: props.product,
            currentProduct: props.index,
            isEditProduct: this.props.product['product_id'] ? false : true,
        })
    }

    cancelAddProduct() {
        let product = this.state.product
        this.setState({
            isEditProduct: false
        },() => {
            if(!product['product_id']){
                this.props.cancelAddProduct()
            }
        })
    }

    EditProduct() {
        this.setState({
            isEditProduct: true
        })
    }

    putProducts(value){
        this.setState({
            isEditProduct: false,
            product: value
        })
        this.props.putProductsDetails(value, this.state.currentProduct)
    }

    render() {
        let data = this.state.product ? this.state.product : ''
        let price = data && data['price'] ? (data['price']/100).toFixed(2) : ''
        let productId = data['product_id'] ? data['product_id'] : ''
        let name = data['name'] ? data['name'] : ''
        let spec = data['spec'] ? data['spec'] : ''
        let unit = data['unit'] ? data['unit'] : ''
        let stock = data['stock'] || data['stock'] == 0 ? data['stock'] : ''
        let state = data && data['state'] == 0 ? 0 : 1
        let pic = data['pic'] ? data['pic'] : ''
        let num = ((this.state.currentProduct+1)/100).toFixed(2).toString().replace('.','')
        let discount = data['discount'] ? true : false
        let style = { display: ( this.state.isEditProduct ? 'block' : 'none')}
        let style2 = { display: ( !this.state.isEditProduct ? 'block' : 'none')}
        return (
            <div className='productCell' >
                <div>
                    <ul style={ style2 }>
                        <li className="chooseCon">
                            <input type='checkbox' className='choose' onChange={(e) => this.checked(e)}/>
                        </li>
                        <li className="imgCon">
                            <img src={pic ? COMMON.IMGHOST + pic : ''} alt='图片错误'/>
                        </li>
                        <li className="nameCon">
                            <p><span>{num}</span> <span className={(discount ? 'discount' : '')}></span></p>
                            <p><span>{ name }</span><span>{data['spec']}</span></p>
                        </li>
                        <li className="priceCon">
                            <span>{ price }／{unit}</span>
                        </li>
                        <li className="stockCon">
                            <span>{data['stock']} /{unit}</span>
                        </li>
                        <li className="stateCon">
                            <span >{(state == 0 ? '未上架' : '已上架')}</span>
                        </li>
                        <li className="operaCon">
                            <span onClick={(e) => this.EditProduct(e)}>编辑</span>
                        </li>
                    </ul>
                </div>
                <div style={style}>
                    <AddProduct cancelAddProduct={() => this.cancelAddProduct()}
                        putProducts={(value) => this.putProducts(value)}
                        product={data}
                        cateId={ this.state.cateId } />
                </div>
            </div>
        )
    }
}
