
const REQUEST_URLS = {
    getIdentify: '/store/code',
    verifyIdengify: '/store/verify',
    sigup: '/store/signup',
    login: '/store/login',
    resetPwd: '/store/reset',
    register: '/store/signup',
    roomType: '/room/type',
    roomIP: '/room/ip'
}

export { REQUEST_URLS }
