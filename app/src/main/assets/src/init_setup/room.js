import React from 'react'
import { Link, hashHistory } from 'react-router'
import Zepto from 'npm-zepto'
import LIBrary from '../lib/util'
import { REQUEST_URLS } from './settings'

export default class RoomSetup extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            count: 8
        }
    }
    //提示
    warnning(str) {
        this.setState({warning: str})
        setTimeout(() => {
            this.setState({warning: ''})
        }, 2000)
    }

    addRoomType() {
        this.setState({
            count: this.state.count + 2
        })
    }

    inputDelete(event) {
        $(event.target).prev().val('')
        $(event.target).next().addClass('hide')
    }

    inputFocus(event) {
        $(event.target).next().addClass('hide')
    }

    inputBlur(event) {
        $(event.target).next().removeClass('hide')
    }

    nextTodo(event) {
        event.preventDefault()
        const form = $(event.target)
        let values = []
        let flag = false
        form.find('input').forEach((input, idx, arrays) => {
            if (input.value == '') {
                return
            }

            if(values.indexOf(input.value) >0){
                flag = true
            }

            values.push(input.value)
        })

        if(flag){
            this.warnning('请勿输入提交相同')
            return
        }

        if(values.length == 0){
            this.warnning('请输入包房类型')
            return
        }

        LIBrary.fetch(REQUEST_URLS.roomType, {
            method: 'POST',
            body: JSON.stringify({
                names: values
            })
        }, (res) => {
            // 成功
            const path = `/ip/setup`
            hashHistory.push({
                pathname: path
            })
        })
    }

    initRoomType() {
        let items = []
        LIBrary.times(this.state.count, (idx) =>
            items.push(<li key={idx}>
                <input maxLength='16'
                    type="text"
                    placeholder="请输入房型 例如：小包"
                    onFocus={(e) => this.inputFocus(e)}
                    onBlur={(e) => this.inputBlur(e)} />
                <span className="delete hide" onClick={(e) => this.inputDelete(e)}></span>
            </li>)
        )
        return items
    }

    render() {
        const data = this.warnning
        return (
            <div className="roomSetup">
                <form onSubmit={ (e) => this.nextTodo(e) }>
                    <h1>房型设置</h1>
                    <ul>{this.initRoomType()}
                    <div className="addRoomType" onClick={this.addRoomType.bind(this)}>新增房型</div>
                    <p>{data.warnning}</p>
                    </ul>
                    <button className="next" type="submit">下一步</button>
                </form>
            </div>
        )
    }
}
