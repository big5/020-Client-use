import React from 'react'
import { Link, hashHistory } from 'react-router'
import Zepto from 'npm-zepto'
import MD5 from 'MD5'

import { REQUEST_URLS } from './settings'
import LIBrary from '../lib/util'
import COMMON from '../lib/common'


export class PasswordForget extends React.Component {
    constructor(props, context) {
        super(props)
        this.state = {
            phone: '',
            identify: '',
            warning: '',
            num: '获取验证码',
            clickable: true
        }
        this.remPwd = this.props.location.state.remPwd
    }

    componentDidMount(){
        LIBrary.eventproxy.on('warnMsg', (str) => {
            this.warnning(str)
        });
    }

    warnning(str){
        this.setState({
            warnning: str
        })
        setTimeout(() => {
            this.setState({
                warnning: ''
            })
        },2000)
    }
    //验证码倒计时
    countDown() {
        const self = this
        let number = 120
        let timer = ''
        this.setState({
            num: number,
            clickable: false
        })
        timer = setInterval(() => {
            if (self.state.num <= 0) {
                self.setState({
                    num: '获取验证码',
                    clickable: true
                })
                clearInterval(timer)
                return
            }
            self.setState({
                num: number--
            })
        }, 1000)
    }
    //修改密码
    input(e, str) {
        let value = e.target.value
        const self = this
        if (e.target.name == 'phone' &&
            !COMMON.PHONEREG.test(value)  ) {
            LIBrary.warnning('请输入正确电话号码！')
            return
        }
        if (e.target.name == 'code' &&
            (isNaN(value) ||
            value.length != 6)) {
            LIBrary.warnning('请输入正确验证码')
            return
        }
    }
    //获取验证码
    getIden() {
        const self = this
        const phone = Zepto('input[name=phone]').val()
        if (!self.state.clickable) {
            return
        }
        if (!COMMON.PHONEREG.test(phone)) {
            LIBrary.warnning('请输入正确手机号')
            return
        }
        LIBrary.fetch(REQUEST_URLS.getIdentify +
            '?phone=' + phone, {
            method: 'GET'
        }, (res) => {
            this.countDown()
        })
    }
    //提交手机号和验证码
    submit(event) {
        event.preventDefault()
        let flag = false
        let form_data = {}
        let str = ''
        const self = this
        const form = Zepto('form')

        form.find('input').map((index,input) => {
            if(input.value ==''){
                flag = true
                str = '请提交完整信息'
                return
            }
            if(input.className == 'phone' &&
            !self.phoneReg.test(input.value)){
                flag = true
                str = '请输入正确手机号'
                return
            }
            if(input.className == 'code' &&
            isNaN(input.value)){
                flag = true
                str = '请输入正确验证码'
                return
            }
            if(input.className == 'password2' &&
                input.value != form_data['password1']){
                    flag = true
                    str = '请输入相同密码'
                    return
                }

            form_data[input.name] = input.value
        })
        if(flag){
            LIBrary.warnning(str)
            return
        }
        LIBrary.fetch(REQUEST_URLS.resetPwd , {
            method: 'POST' ,
            body: JSON.stringify({
                phone: form_data.phone,
                passwd: MD5(form_data.password1),
                code: form_data.code
            })
        }, (data) => {
            if(self.remPwd){
                localStorage.setItem('userAndPwd', form_data.phone + '&' +
                                    MD5(form_data.password1) + '&' +
                                    form_data.password1.length)
            }
            //跳转进入

            const path = `/login`
            hashHistory.push({pathname: path})
        })
    }

    render() {
        const data = this.state
        return (
            <div className='passwordReset'>
                <h1>找回密码</h1>
                <form onSubmit={ (e) => this.submit(e) }>
                    <ul>
                        <li>
                            <lable></lable>
                            <input type='tel' name='phone' maxLength='11' placeholder='请输入手机号' onBlur={(e) => {this.input(e)}}/>
                        </li>
                        <li>
                            <lable></lable>
                            <input type='tel' maxLength='6' name='code' placeholder='请输入验证码' onBlur={(e) => {this.input(e) }}/>
                            <span className='getNum' onClick={this.getIden.bind(this)}>{data.num}</span>
                        </li>
                        <li>
                            <lable></lable>
                            <input type='password' name='password1' maxLength='12' placeholder='请输入新密码' onBlur={(e) => {
                                this.input(e, 'pwd1')
                            }}/>
                        </li>
                        <li>
                            <lable></lable>
                            <input type='password' name='password2' maxLength='12' placeholder='请再次输入密码' onBlur={(e) => {
                                this.input(e, 'pwd2')
                            }}/>
                        </li>
                        <li className={data.warnning ? 'warning' : ''}>{data.warnning}</li>
                    </ul>
                    <button className='next' type='submit' onClick={this.submit.bind(this)}>确认</button>
                </form>
            </div>
        )
    }
}
