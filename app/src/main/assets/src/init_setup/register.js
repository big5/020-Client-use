import React from 'react'
import {hashHistory, Link} from 'react-router'
import {REQUEST_URLS} from './settings'
import LIBrary from '../lib/util'
import COMMON from '../lib/common'
import MD5 from 'MD5'
export default class Register extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            warnning: '',
            clickable: true,
            identify: '获取验证码'
        }
    }

    componentDidMount(){
        LIBrary.eventproxy.on('warnMsg', (str) => {
            this.warnning(str)
        });
    }

    warnning(str){
        this.setState({
            warnning: str
        })
        setTimeout(() => {
            this.setState({
                warnning: ''
            })
        },1500)
    }

    //验证码倒计时
    countDown() {
        const self = this
        let number = 120
        let timer = ''
        this.setState({identify: number, clickable: false})
        timer = setInterval(() => {
            if (number < 0 || isNaN(self.state.identify)) {
                self.setState({identify: '获取验证码', clickable: true})
                clearInterval(timer)
                return
            }
            self.setState({
                identify: number--
            })
        }, 1000)
    }

    //获取验证码
    getCode() {
        const self = this
        const phone = Zepto('input[name=phone]').attr('value')
        if (!self.state.clickable) {
            return
        }
        if (!COMMON.PHONEREG.test(phone)) {
            debugger
            LIBrary.warnning('请输入手机号')
            return
        }

        LIBrary.fetch(REQUEST_URLS.getIdentify + '?phone=' + phone, {
            method: 'GET'
        }, (res) => {
            self.countDown()
        })
    }
    //用户提交判断
    submit(event) {
        event.preventDefault()
        const self = this
        const datas = this.state
        const form = Zepto('form')
        let pwdLength = 0
        let form_data = {}
        let flag = false
        let str = ''
        form.find('input').map((index, input) => {
            if (input.value == '') {
                flag = true
                str = '请填写完整信息'
                return
            }
            if (input.name == 'phone' && !COMMON.PHONEREG.test(input.value)) {
                flag = true
                str = '请输入正确手机号'
                return
            }
            if (input.name == 'code' && input.value.length != 6) {
                flag = true
                str = '请输入正确验证码'
            }
            if(input.name == 'password1' && input.value.length <8 || input.value.length > 12){
                flag = true
                str = '请输入正确密码，8至12位'
            }
            if (input.name == 'password2' && input.value != form_data['password1']) {
                flag = true
                str = '请输入相同密码'
            }
            form_data[input.name] = input.value
        })

        if (flag) {
            LIBrary.warnning(str)
            return
        }

        form_data.passwd = MD5(form_data.password1)
        pwdLength = form_data.password1.length
        form_data.password1 = ''
        form_data.password2 = ''

        LIBrary.fetch(REQUEST_URLS.register, {
            method: 'POST',
            body: JSON.stringify(form_data)
        }, (res) => {
            setTimeout(() => {
                const path = `/login`
                hashHistory.push({pathname: path})
            }, 200)
        })
    }

    render() {
        const data = this.state
        return (
            <div className='register'>
                <h1>注册</h1>
                <form onSubmit={(e) => this.submit(e)}>
                    <ul>
                        <li>
                            <label>联系人：</label>
                            <input type='text' name='manager' placeholder='请输入姓名'/>
                        </li>
                        <li>
                            <label>店铺名称：</label>
                            <input type='text' name='name' placeholder='请输入店铺名'/>
                        </li>
                        <li>
                            <label>店铺地址：</label>
                            <input type='text' name='address' placeholder='请输入店铺地址'/>
                        </li>
                        <li>
                            <label>营业时间</label>
                            <input type='time' name='st' />
                            <span className='timeTO'>至</span>
                            <input type='time' name='ed' />
                        </li>
                        <li>
                            <label>绑定手机：</label>
                            <input type='text' name='phone' maxLength='11' placeholder='请输入手机号'/>
                        </li>
                        <li>
                            <label>验证码：</label>
                            <input type='text' maxLength='6' name='code' placeholder='请输入验证码'/>
                            <span onClick={this.getCode.bind(this)}>{data.identify}</span>
                        </li>
                    </ul>
                    <ul>
                        <li>
                            <label>设置登录密码：</label>
                            <input type='password' maxLength='12' name='password1' placeholder='请输入设置密码'/>
                        </li>
                        <li className='aginPassword'>
                            <label>再次输入密码：</label>
                            <input type='password' maxLength='12' name='password2' placeholder='请再次输入密码'/>
                        </li>
                        <li className={data.warnning ? 'warning' : ''} >
                            {data.warnning}
                        </li>
                    </ul>
                    <button className='next goLogin' type='submit'>下一步</button>
                    <Link to="/login" className='next goPre' style={{color: '#fff'}}>返回</Link>
                </form>
            </div>
        )
    }
}
