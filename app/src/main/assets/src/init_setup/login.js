import React from 'react'
import {Link, hashHistory} from 'react-router'
import {REQUEST_URLS} from './settings'
import Zepto from 'npm-zepto'
import LIBrary from '../lib/util'
import COMMON from '../lib/common'
import MD5 from 'MD5'

export default class Login extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            passwd: '',
            phone: '',
            remPwd: true,
            warnning: '',
            roomType: ''
        }
    }

    componentDidMount() {
        const token = localStorage.getItem('token')
        if(token) {
            window.location.href = '../room_setup/index.html#/room'
        }
        LIBrary.eventproxy.on('warnMsg',(str) => {
            this.warnning(str)
        })
    }

    warnning(str) {
        this.setState({warnning: str})
        setTimeout(() => {
            this.setState({warnning: ''})
        }, 2000)
    }
    //记住密码
    remumberPwd(e) {
        const obj = e.target
        this.setState({remPwd: obj.checked})
    }
    //输入手机号和密码
    input(e) {
        const input = e.target
        if (input.name == 'phone' &&
        !COMMON.PHONEREG.test(input.value)) {
            LIBrary.warnning('请输入正确手机号')
            return
        }
        if (input.name == 'passwd' &&
        input.value == '' ||
        input.value.length < 8 ||
        input.value.length > 12) {
            LIBrary.warnning('请输入正确密码！')
            return
        }
    }
    //忘记密码
    forgetPwd() {
        setTimeout(() => {
            const path = `/pass/forget`
            hashHistory.push({
                pathname: path,
                state: {
                    remPwd: this.state.remPwd
                }
            })
        }, 200)
    }
    //提交
    submit(event) {
        event.preventDefault()
        const form = Zepto('form')
        let form_data = {}
        let flag = false
        let str = ''
        form.find('input').map((index, input) => {
            if (input.type == 'checkbox') {
                return
            }
            if (input.name == 'phone' &&
            !COMMON.PHONEREG.test(input.value)) {
                flag = true
                str = '请输入正确手机号'
            }
            form_data[input.name] = input.value
        })
        if (flag) {
            LIBrary.warnning(str)
        }
        form_data['passwd'] = Zepto('input[type=password]').val().length > 12 ?
        this.state.passwd :
        MD5(form_data['passwd'])
        this.login(form_data)
    }

    inited(callback) {
        LIBrary.fetch(REQUEST_URLS.roomType, {
            method: 'GET'
        }, (res) => {
            if (res.list.length > 0) {
                window.location = '../room_setup/index.html#/room'
                return
            }
            callback()
        })
    }

    notInit() {
        setTimeout(() => {
            const path = `/room/setup`
            hashHistory.push({pathname: path})
        }, 500)
    }

    login(form_data) {
        LIBrary.fetch(REQUEST_URLS.login + '?phone=' + form_data['phone'] + '&passwd=' + form_data['passwd'], {
            method: 'GET'
        }, (res) => {
            res.token ? localStorage.setItem('token', res.token) : ''
            this.inited(this.notInit)
        })
    }

    render() {
        const data = this.state
        return (
            <div className="login">
                <img src='../img/loginTitle.png'/>
                <form onSubmit={(e) => this.submit(e)}>
                {/* <form onSubmit={(e) => this.submit(e)} autoComplete='off'> */}
                    <ul>
                        <li>
                            <label></label>
                            <input type="text" name='phone' maxLength='11' defaultValue={data.phone} placeholder='请输入手机号' onBlur={(e) => {
                                this.input(e)
                            }}/>
                        </li>
                        <li>
                            <label></label>
                            <input type="password" name='passwd' maxLength='12'
                                defaultValue={data.passwd && data.passwd.length > 13 ? data.passwd.substr(0, 13) : ''}
                                placeholder='请输入密码' onBlur={(e) => {this.input(e)}}/>
                        </li>
                        <li className={data.warnning ? 'warning' : ''}>
                            { data.warnning }
                        </li>
                    </ul>
                    <p>
                        <input type="checkbox" defaultChecked={data.remPwd ? 'checked' : ''} onChange={this.remumberPwd.bind(this)}/>
                        <label className='rememberPsw'>记住密码</label>
                        <span onClick={this.forgetPwd.bind(this)}>忘记密码?</span>
                    </p>
                    <button className="next" type="submit">登录</button>
                </form>
                <p>
                    <Link to="/register" style={{color: '#fff'}}>注册账户</Link>
                </p>
                <a href='../pay_setup/index.html#/wechatPay'>JS调用Android测试入口</a>
            </div>
        )
    }
}
