require('./app.scss')
// require('../lib/common.css')

import React from 'react'
import ReactDOM from 'react-dom'
import { Router, Route, hashHistory, Link ,IndexRedirect} from 'react-router'

import Login from './login'
import Register from './register'
import { PasswordForget, PasswordReset } from './password'
import RoomSetup from './room'
import IPSetup from './ip'

ReactDOM.render((
    <Router history={hashHistory}>
        <Route path='/' component={ Login } >
            <IndexRedirect to="/login" />
        </Route>
        <Route path='/login' component={ Login } />
        <Route path='/register' component={ Register } />
        <Route path='/pass/forget' component={ PasswordForget } />
        <Route path='/pass/reset' component={ PasswordReset } />
        <Route path='/room/setup' component={ RoomSetup } />
        <Route path='/ip/setup' component={ IPSetup } />
    </Router>
), document.getElementById('body'))
