import React from 'react'
import {Link} from 'react-router'
import Zepto from 'npm-zepto'
import LIBrary from '../lib/util'
import COMMON from '../lib/common'
import { REQUEST_URLS } from './settings'

export default class IPSetup extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            roomTabs: [],
            count: [],
            warnning: '',
            currentIndex: 0,
        }
    }

    warnning(str){
        this.setState({
            warnning: str
        })
        setTimeout(() => {
            this.setState({
                warnning: ''
            })
        }, 2000)
    }

    componentDidMount() {
        //初始设置有有n个房型，每个有4个
        LIBrary.fetch(REQUEST_URLS.roomType, {
            method: 'GET'
        }, (res) => {
            let count = res.list.map((index) => {
                return 4
            })
            this.setState({roomTabs: res.list, count: count})
        })
        LIBrary.eventproxy.on('warnMsg', (str) => {
            this.warnning(str)
        })
    }

    inputDelete(event, index) {
        Zepto(event.target).parents('li').find('input').val('')
    }

    inputFocus(event) {
        Zepto(event.target).parents('li').find('span').removeClass('hide')
    }

    inputBlur(event) {
        Zepto(event.target).parents('li').find('span').addClass('hide')
    }

    //获取点击的房间类型
    tabChoiced(index) {
        this.setState({currentIndex: index})
    }
    //增加房间ip设置
    addIPSetting() {
        let counts = this.state.count
        counts[this.state.currentIndex] += 1
        let item = this.state.roomTabs[this.state.currentIndex]
        this.setState({count: counts})
    }

    finish(e) {
        e.preventDefault()
        const form = Zepto(e.target)
        let checkarr = []
        let roomInfor = []
        let flag = false
        let str = ''
        this.state.roomTabs.map((value, index) => {
            let obj = {}
            obj.rt_id = value.id
            obj.list = []
            roomInfor.push(obj)
        })

        Zepto('input').map((index,input) => {
            let text = input.value.replace(/ /g,'')
           if(text == ''){
               return
           }

           if(input.name == 'ip' &&
            !COMMON.IPREG.test(text)){
                flag = true
                str = '请输入正确ip地址'
            }

            if(input.name == 'mac' &&
            !COMMON.MACREG.test(text)){
                flag = true
                str = '请输入正确mac地址'
            }


           if(checkarr.indexOf(text) >= 0){
               flag = true
               str = '信息提交重复，请修改后提交'
           }
           checkarr.push(text)
       })

       if(flag){
           LIBrary.warnning(str)
           return
       }

       if(checkarr.length == 0){
           LIBrary.warnning('请填写详细信息')
           return
       }

        Zepto('.ip').map((idx, value) => {
            roomInfor[idx].rt_id = this.state.roomTabs[idx]['rt_id']
            Zepto(value).find('input').map((index, val) => {
                let num = index % 2 ==1  ? (parseInt(index / 2)) : index / 2
                let text = val.value.replace(/ /g,'')
                if(text == '' ){
                    return
                }
                if(val.name == 'ip'){
                    roomInfor[idx].list[num]['ip'] = text
                    roomInfor[idx].list[num]['mac'] = ''
                    return
                }
                if(val.name == 'mac') {
                    roomInfor[idx].list[num]['mac'] = text
                    roomInfor[idx].list[num]['ip'] = ''
                    return
                }
                let json = {}
                json[val.name] = text
                roomInfor[idx].list.push(json)
            })
        })
        LIBrary.fetch(REQUEST_URLS.roomIP, {
            method: 'POST',
            body: JSON.stringify(roomInfor)
        }, (res) => {
            window.location = '../room_setup/index.html#/room'
        })
    }

    //ip，房型设置
    renderiPSetting(index) {
        let items = []
        LIBrary.times(index, (idx) =>{
            let input = COMMON.TYPEIP == 'MAC' ?
            <input type='text' name='mac'
                className='ipNo '
                placeholder='00 00 00 00 00 00'
                maxLength='18'
                style={{display: COMMON.TYPEIP == 'MAC' ? 'block' : 'none'}}
            /> :
            <input type='text' name='ip'
                className='ipNo '
                placeholder='0.0.0.0'
                maxLength='18'
                style={{display: COMMON.TYPEIP == 'IP' ? 'block' : 'none'}}
            />
            items.push(
                <li key={idx}>
                    <input type='text' name='name'  className='roomNo' placeholder='请输入房间号' />
                    <label>mac地址</label>
                    <div className='ipAddr'>  {input} </div>
                    <span className='delete hide' onClick={(e) => this.inputDelete(e, idx)}></span>
                </li>
        )})
        return items
    }

    //渲染房间类型nav
    renderRooms() {
        let items = []
        items = this.state.roomTabs.map((value, index) => {
            return <li key={index}
                className= { index == this.state.currentIndex ? 'active' : 'cursor' }
                onClick={() => { this.tabChoiced(index) }}>
                {value['name']}
                <span></span>
            </li>
        })
        return items
    }

    //房间类型box
    renderIPRoomSetting() {
        let items = []
        const self = this
        items = this.state.roomTabs.map((value, index) => {
            return  <ul key={index} className='ip'
                    style={{display: index == this.state.currentIndex ? 'block' : 'none' }}>
                        {this.renderiPSetting(this.state.count[index])}
                    </ul>
        })
        return items
    }

    render() {
        const datas = this.state
        const text = COMMON.TYPEIP =='MAC' ? 'mac地址设置' : 'ip地址设置'
        return (
            <div className='ipSetup'>
                <form onSubmit={ (e) => this.finish(e) }>
                    <h1>{ text }</h1>
                    <ol className='menu'>
                        {this.renderRooms()}
                    </ol>
                    <div className='ipRoom'>
                        {this.renderIPRoomSetting()}
                        <div className='addRoomIpType' onClick={this.addIPSetting.bind(this)}>
                            新增房间号
                        </div>
                        <div className={datas.warnning ? 'warning' : ''}>{datas.warnning}</div>
                    </div>
                    <div className='buttons'>
                        <button className='prev' type='submit' >
                            下一步
                        </button>
                        <button  type='submit' className='finish' >
                            完成
                        </button>
                    </div>
                </form>
            </div>
        )
    }
}
