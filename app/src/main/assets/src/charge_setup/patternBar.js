/**
 * Created by lixinfeng on 2017/4/25.
 */

import React from 'react'
import LIBrary from '../lib/util'
import ROOMCOM from './roomCommon'
export default class PatternBar extends React.Component {
    constructor(props) {
        super(props)
        this.state={
            data:props.data,
            curIndex:props.cur!==''?props.cur:'',
            isAllow:props.isAllow||true
        }
    }

    // shouldComponentUpdate(nextProps,nextState) {
    //     // console.log("PatternBar is :"+JSON.stringify(nextProps),JSON.stringify(nextState))
    // }

    changeIndex(idx) {
        console.log("&:"+idx,"isAllow is :"+this.props.isAllow)
        if(!this.props.isAllow) {
            this.props.selected(idx)
            this.setState({
                curIndex: idx
            })
            console.log("have been selected", idx)
        }
    }

    templateList(products) {
        const list = []
        let cur = this.state.curIndex
        LIBrary.times(products.length,(index)=>{
            list.push(<li key={index} className={"roomButton "+( cur === index?"selected":"")}
                        onClick={()=>this.changeIndex(index)}>
                <p>{products[index]}</p><span><em></em></span></li>)
        })
        return list
    }

    render(){
        return(
            <ul className='boxState'>{this.templateList(this.props.data)}</ul>
        )
    }
}
