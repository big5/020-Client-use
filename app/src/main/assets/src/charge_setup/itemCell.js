import React from 'react'
import { REQUEST_URLS } from './setting'
import LIBrary from '../lib/util'

export default class BuyItemsCell extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            product: props.product,
            count: 1
        }
    }

    componentWillReceiveProps(props) {
        if (props.product == this.state.product) {
            return
        }
        this.setState({product: props.product})
    }

    changeNum(e) {
        const obj = e.target
        if (e.target.value == 0) {
            e.target.value = 1
        }
        const num = obj.value &&
        obj.value >= 1 ?
        obj.value : 1
        this.setState({count: num})
        this.props.putNum(num)
    }

    deleteProudct() {
        this.props.deleteProudct()
    }

    templateID(params) {
        if(typeof params != 'object'){
            return
        }
        if(params['product_id'] ||
        params['product_id'] == 0){
            return <input type='hidden' name='product_id' defaultValue={ params['product_id']}/>
        }
        if(params['pack_id'] ||
        params['pack_id'] == 0){
            return <input type='hidden' name='pack_id' defaultValue={ params['pack_id']}/>
        }
    }

    render() {
        let product = this.state.product
        let name = product &&
        product['name'] ?
        product['name'] : ''
        let price = product &&
        product['price'] ?
        LIBrary.Fix2(product['price']) : ''
        let totalMoney = product &&
        product['price'] ?
        LIBrary.Fix2(product['price'] * this.state.count) : ''
        return (
            <div className='buyItem'>
                {this.templateID(product)}
                <span className='name'>{name}</span>
                <span className='price'>{price}</span>
                <span className='count'>
                    <input defaultValue={this.state.count} name='count' onBlur={(e) => this.changeNum(e)}/>
                </span>
                <span className='totalMoney'>{totalMoney}</span>
                <span onClick={() => this.deleteProudct()}>删除</span>
            </div>
        )
    }
}
