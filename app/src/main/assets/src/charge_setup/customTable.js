/**
 * Created by lixinfeng on 2017/4/27.
 */

import React from 'react'
import ROOMCOM from './roomCommon'
import LIBrary from '../lib/util'
import Zepto from 'npm-zepto'
import {BootstrapTable,TableHeaderColumn}  from 'react-bootstrap-table'


export default class CustomTable extends React.Component {
    constructor(props) {
        super(props)
        this.state= {
            product: props.data,
            type:props.type||'',
            expend:props.expend||'',
            arr: ROOMCOM[props.type]
        }
    }
    
    componentWillReceiveProps(nextProps) {
        if (nextProps.data!=this.state.product||nextProps.type!=this.state.type||nextProps.expend!=this.state.expend){
            this.setState({
                product:nextProps.data,
                type:nextProps.type,
                expend:nextProps.expend
            })
        }
    }
    
    componentDidUpdate() {
       this.toggleTabel()
    }
    
    toggleTabel() {
        Zepto('.customTable>.table-body>table>tbody').off().on('click','tr', (e)=> {
            let evt = e.target || e.srcElement
            let $tr =  Zepto(e.target).parent()
            if($tr[0].nodeName == 'TR' && $tr.attr('class') == 'expend') {
                $tr.next().toggleClass('hiden')
                e.stopPropagation()
                return false
            }
        })
    }

    getNewObj(obj) {
        let name
        if (this.state.type == ROOMCOM.RWINE) {
            return{
                "name":obj.name,
                "unit":obj.unit,
                "unit_price":obj.unit_price,
                "count":obj.count,
                "shop_time":obj.shop_time,
                "money": obj.money,
                "pay_state":obj.pay_state==1?"已付":"未付"
            }
        }
        if (!Array.isArray(this.state.product)) {
           name =  obj.pack_id ? '套餐':'计时'
        } else {
            name = obj.pack_id ?  obj.name || '套餐' : obj.name || '计时'
        }
        return{
            "name":name,
            "st_ed":obj.st?(obj.st+"-"+obj.ed):"",
            "minute":obj.minute|"",
            "money_minute":obj.money_minute?(obj.money_minute/100).toFixed(2):'-',
            "money":((obj.money||obj.price)/100).toFixed(2)||""
        }
    }
    
    templateTabRow(obj,len) {
        const rows = []
        let keyList = this.state.arr
        LIBrary.times(keyList.length,(index)=>{
            rows.push(<td key={index}>{obj[keyList[index]]}</td>)
        })
        if (Array.isArray(this.state.product) && len){rows.push(<td rowSpan={len} key={keyList.length}>补打小票</td>)}
        return rows
    }

    templateChildren() {
        let list = []
        let objArr = []
        let isArray = false
        const data = this.state.product
        if (Array.isArray(data)) {
            objArr = data
            isArray = true
        } else if(data.pack_id) {
            objArr.push(data)
        } else {
            objArr = data.list||[]
        }
        if(!objArr || !objArr.length) {return}
        if (!isArray) {
            LIBrary.times(objArr.length,(index)=>{
                index == 0 ?
                    list.push (
                        <tr key={index}>
                            <td rowSpan={data.pack_id?2:objArr.length} className={data.expend}>
                            </td>
                            {data.pack_id ? this.templateTabRow(this.getNewObj(data),2) :
                                this.templateTabRow(this.getNewObj(objArr[index]),objArr.length)}
                        </tr>
                    ):list.push(
                        <tr key={index}>
                            {this.templateTabRow(this.getNewObj(objArr[index]))}
                        </tr>
                    )
                    if (data.pack_id) {
                        list.push(<tr key={index+1000} className="hiden"><td colSpan={ROOMCOM.RWINEBILL.length}>
                            <BillTable data={data.list} st={data.st}/></td></tr>)
                    }
            })
        } else {
            LIBrary.times(objArr.length,(index)=>{
               list.push( <tr key={index+200}>
                        <td rowSpan={objArr[index].len} className= {objArr[index].list[0].expend}>
                            {objArr[index].bill_no || ''}
                        </td>
                        {this.templateTabRow(this.getNewObj(objArr[index].list[0]),objArr[index].len)}
                    </tr>
                )
                let arr = objArr[index].list
                LIBrary.times(arr.length,(idx)=>{
                    if(idx!=0){ list.push( <tr key={idx+1000*index} className={arr[index].expend}>
                            {this.templateTabRow(this.getNewObj(arr[idx]))}
                        </tr>
                    )}
                    if (arr[idx].pack_id && arr[idx].list && arr[idx].list.length ) {
                        list.push(<tr key={idx+2000+(index+1)*100}  className={`hide`}><td colSpan={ROOMCOM.RWINEBILL.length}>
                            <BillTable data={arr[idx].list} st={arr[index].st} payState={objArr[index].pay_type}/></td></tr>)
                    }
                })
            })
        }
        return list
    }

    render() {
        return (
            <div className="customTable">
                <div className="table-body">
                    <table>
                        <colgroup>
                            <col className/>
                            <col className/>
                            <col className/>
                        </colgroup>
                        <thead>
                        <tr>
                        {this.props.children}
                        </tr>
                        </thead>
                        <tbody>
                        {this.templateChildren()}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}



class BillTable extends React.Component {
    constructor(props) {
        super(props)
        this.state={
            data:props.data,
            st:props.st,
            payState:props.payState
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.data!=this.state.data||nextProps.st!=this.state.st){
            this.setState({
                data:nextProps.data,
                st:nextProps.st,
                payState:props.payState
            })
        }
    }

    templateTitle() {
        const list = []
        const data= ROOMCOM.RWINEBILL
        LIBrary.times(data.length,(index)=>{
            index==0?
                list.push(<TableHeaderColumn key={index} dataField={ROOMCOM.RWINEBILL_ENG[index]} isKey={true}>{ROOMCOM.RWINEBILL[index]}</TableHeaderColumn>):
                list.push(<TableHeaderColumn key={index} dataField={ROOMCOM.RWINEBILL_ENG[index]} >{ROOMCOM.RWINEBILL[index]}</TableHeaderColumn>)
        })
        return list
    }

    getNewData(data) {
        const newData = []
        for (let i in data){
            newData.push(this.getChangeObj(data[i]))
        }
        return newData
    }

    getChangeObj(obj) {
        return {
            "name":obj.name,
            "unit":obj.unit,
            "unit_price":((obj.price/obj.count)/100).toFixed(2),
            "count":obj.count,
            "shop_time":this.state.st,
            "pay_state":this.state.payState==1?"已付":"未付",
            "money": (obj.price/100).toFixed(2)
        }
    }

    render() {
        return (
            <BootstrapTable data={ this.getNewData(this.state.data) }>
                {this.templateTitle()}
            </BootstrapTable>
        )
    }
}
