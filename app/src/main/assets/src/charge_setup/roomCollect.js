/**
 * Created by lixinfeng on 2017/5/2.
 */
import React from 'react'
import moment from 'moment'
import LIBrary from '../lib/util'
import ROOMCOM from './roomCommon'
import RoomCard from './roomCard'
import RoomCell from './roomCell'

export default class RoomCollect extends React.Component {
    constructor(props) {
        super(props)
        this.state={
            list:props.list || [],
            num:props.num || 0,
            currentCell:0
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.num!=this.state.num || nextProps.list!=this.state.list) {
            console.log("change roomCollect")
            this.setState({
                list:nextProps.list
            })
        }
    }

    changeIndex(idx) {
        this.props.changeIndex(idx)
        this.setState({
            currentCell:idx
        })
    }

    //计算不同状态下房间数量
    getStateArray(list) {
        let arr = []
        let o = {}
        for (let obj of list) {
            if (!o[obj.room_type]) {
                o[obj.room_type] = 1
                continue
            }
            o[obj.room_type] += 1
        }
        for (let j = 1; j <= ROOMCOM.RSTATES.length; j++) {
            o[j] ? arr.push(o[j]) : arr.push(0)
        }
        return arr
    }

    templateRoomStates() {
        const rmStates = []
        if(!this.state.list.length){return rmStates}
        const list = this.state.list
        const stateArray = this.getStateArray(list)
        LIBrary.times(ROOMCOM.RSTATES.length, (index) => {
            rmStates.push(<li key={index}>
                <span style={{backgroundColor: ROOMCOM.RSTATES_COLOR[index]}} className='checkRoomState'></span>
                <span style={{color: ROOMCOM.RSTATES_COLOR[index]}} className='nameRoomState'>{ROOMCOM.RSTATES[index]}</span><span style={{color: ROOMCOM.RSTATES_COLOR[index]}}>({stateArray[index]})</span>
            </li>)
        })
        return rmStates
    }


    getMinutesDiff(obj) {
        if(obj['room_type'] == ROOMCOM.RSTATE_USING || obj['room_type'] == ROOMCOM.RSTATE_TIMEOUT){
            //时差
            // let start = moment(obj.st,"YYYY.MM.DD HH:mm")
            // let end = moment(obj.ed,"YYYY.MM.DD HH:mm")
            let start = moment(obj.st,"HH:mm")
            let end = moment(obj.ed,"HH:mm")
            let cur = moment()
            let timeDiff = end > cur ? end.diff(cur,'minutes') : cur.diff(end,'minutes')
            let startDes = start.format('HH:mm')
            let endDes = end.format('HH:mm')
            return end > cur ? (timeDiff > 30 ? startDes+"-"+endDes : '还剩'+timeDiff+'分钟') : "超出"+timeDiff+"分钟"
        }
        return ''
    }

    //包房基本信息
    templateRoomCell() {
        const cells = []
        if(!this.state.list.length){return cells}
        const roomcells = this.state.list
        LIBrary.times(roomcells.length, (index) => {
            let obj = roomcells[index]
            cells.push(<RoomCell key={index}
                                 msg={{
                                     'roomType': obj['room_type'],
                                     'name': obj['name'],
                                     'describe': obj['describe'],
                                     'rtName': obj['rt_name'],
                                     'eventIndex': index,
                                     'des':this.getMinutesDiff(obj),
                                     'payType':obj['pay_type'] ? ROOMCOM.RPAYSTATES[obj['pay_type']-1]:'',
                                     'active': index == this.state.currentCell ? 'active' : ''
                                 }}
                                 changeIndex={idx=>this.changeIndex(idx)}/>)
        })
        return cells
    }

    render(){
        return(
            <div className="roomCollect leftCon">
                <RoomCard
                  title={<ul className='leftRoomState'>{this.templateRoomStates()}</ul>}
                  children={<div className="roomCellCollection">{this.templateRoomCell()}
                  </div>}/>
            </div>
        )
    }
}
