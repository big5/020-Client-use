/**
 * Created by lixinfeng on 2017/4/14.
 */

import React from 'react'
import ROOMMON from './roomCommon'

export default class RoomCell extends React.Component {
    constructor(props) {
        super(props)
        this.state={
            roomType:props.msg.roomType,
            name : props.msg.name,
            describe:props.msg.describe,
            rtName:props.msg.rtName,
            eventIndex:props.msg.eventIndex,
            active:props.msg.active,
            des:props.msg.des,
            payType:props.msg.payType
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.msg.roomType != this.state.roomType) {
            console.log("roomCell is change")
            this.setState({
                roomType:nextProps.msg.roomType
            })
        }
    }

    shouldComponentUpdate(nextProps,nextState) {
        return this.props.msg.active!=nextProps.msg.active || this.state.roomType!=nextState.roomType
    }

    selectIndex() {
        this.props.changeIndex(this.props.msg.eventIndex)
    }
    render() {
        const data = this.state
        console.log("it's roomType is :"+data.roomType)
        return(
            <div className={"roomCell "+ this.props.msg.active} style={{backgroundColor:ROOMMON[ROOMMON.RCOLOR+[data.roomType]]}}
                onClick={()=>this.selectIndex()} >
                <p className="roomNumber">{data.name}</p>
                <div className="cellCenter">
                    <p className="roomName">{data.rtName}</p>
                </div>
                <div className='cellBottom'>
                    <p className="roomDetail">{data.des}</p>
                    <p className="roomPayType">{data.payType}</p>
                </div>
            </div>
        )
    }
}
