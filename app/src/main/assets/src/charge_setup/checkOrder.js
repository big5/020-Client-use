import React from 'react'
import LIBrary from '../lib/util'
import COMMON from '../lib/common'
var ReactBsTable = require('react-bootstrap-table');
var BootstrapTable = ReactBsTable.BootstrapTable;
var TableHeaderColumn = ReactBsTable.TableHeaderColumn;

export default class CheckOrder extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            product: a,
            index: 0
        }
        this.selectRowProp = {
            mode: 'radio'
        }

    }

    getOrders(e, idx) {
        let type = idx == 0 ? 'wx' :
        ( idx == 1 ? 'ali' : 'pos')

        // LIBrary.fetch('', {
        //     method: 'get'
        // }, (res) => {
        //     this.setState({res: 11})
        // })
    }

    templateNav() {
        let items = []
        LIBrary.times(COMMON.ORDERTYPE.length, (idx) => {
            items.push(
                <span className={idx == this.state.index ? 'active' : ''}
                key={idx} onClick={(e) => this.getOrders(e, idx)}>
                    {COMMON.ORDERTYPE[idx]}
                </span>
            )
        })
        return items
    }

    render() {
        return (
            <div className='shoppingCart'>
                <div className='nav'>
                    { this.templateNav() }
                </div>
                <BootstrapTable data={this.state.product} selectRow={this.selectRowProp} >
                    <TableHeaderColumn isKey dataField='num'>序号</TableHeaderColumn>
                    <TableHeaderColumn dataField='order_no'>商户单号</TableHeaderColumn>
                    <TableHeaderColumn dataField='order_no'>订单号</TableHeaderColumn>
                    <TableHeaderColumn dataField='bill_no'>流水号</TableHeaderColumn>
                    <TableHeaderColumn dataField='wx_no'>微信订单号</TableHeaderColumn>
                    <TableHeaderColumn dataField='money'>支付金额</TableHeaderColumn>
                    <TableHeaderColumn dataField='state'>支付状态</TableHeaderColumn>
                </BootstrapTable>
            </div>
        )
    }
}

var a = [
    {
        num: 1,
        order_no: 2012000000,
        bill_no: 2023033223,
        wx_no: 32232323,
        money: 13333,
        state: 0,
    }, {
        num: 2,
        order_no: 2012000000,
        bill_no: 2023033223,
        wx_no: 32232323,
        money: 13333,
        state: 0,
    },{
        num: 3,
        order_no: 2012000000,
        bill_no: 2023033223,
        wx_no: 32232323,
        money: 13333,
        state: 0,
    },{
        num: 4,
        order_no: 2012000000,
        bill_no: 2023033223,
        wx_no: 32232323,
        money: 13333,
        state: 0,
    },{
        num: 5,
        order_no: 2012000000,
        bill_no: 2023033223,
        wx_no: 32232323,
        money: 13333,
        state: 0,
    },{
        num: 6,
        order_no: 2012000000,
        bill_no: 2023033223,
        wx_no: 32232323,
        money: 13333,
        state: 0,
    }
]
