/**
 * Created by lixinfeng on 2017/4/24.
 */
import React from 'react'
import LIBrary from '../lib/util'
import ROOMCOM from './roomCommon'
import { REQUEST_URLS } from './setting'
import Dialog from '../components/dialog'
import PatternBar from './patternBar'
import BackOrder from './backOrder'
import DropDownButton  from '../room_setup/select'
import { hashHistory } from 'react-router'

export default class RoomItems extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isShow:false,
            isBreak:false,
            data:props.data,
            curIndex:'',
            curType:''
        }
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.data!=this.state.data){
            this.setState({data:nextProps.data})
        }
    }

    onSelectItem(n) {
        console.log("index is :"+n)
    }

    getRoomList() {
        const roomList = []
        if(this.state.data && !this.state.data.dataList.length) {
            const list = this.state.data.dataList || []
            for (let o of list){
                roomList.push(o['name']+o['rmName'])
            }
        }
        return roomList
    }

    jumpNextLink(msg,path) {
        hashHistory.push({
            pathname: path,
            state: {
                roomMsg: msg
            }
        })
    }

    putRoomType(type) {
        const obj = this.props.data.dataList[this.props.data.cur]
        if (!obj){return}
        console.log("obj is :"+JSON.stringify(obj))
        let rmType,dec
        switch (type) {
            case ROOMCOM.ROPERATE_TYPE_SETFREE:
                rmType = 1;
                this.netWork(obj,rmType)
                break
            case ROOMCOM.ROPERATE_TYPE_SETCLEAN:
                rmType = 4
                this.netWork(obj,rmType)
                break
            case ROOMCOM.ROPERATE_TYPE_BREAK:
                rmType = 5;
                dec = Zepto('.roomItems  .editReason').find('textarea').text()
                this.netWork(obj,rmType)
                break
            case ROOMCOM.ROPERATE_TYPE_CLOSE:
                rmType = 4
                LIBrary.fetch(REQUEST_URLS.orderClose+"?order_no="+obj.orderNo, {
                    method: 'GET',
                }, (res) => {
                    this.props.updateItem(this.props.data.cur,rmType)
                })
                break
            default: break
        }
    }

    netWork(obj,rmType) {
        LIBrary.fetch(REQUEST_URLS.roomIp, {
            method: 'PUT',
            body:JSON.stringify({
                "rt_id":obj.rtId,
                "room_id":obj.rmId,
                "name":obj.name,
                "room_type":rmType
            })
        }, (res) => {
            rmType == 5 ? this.setState({isBreak:false,isShow:false}) : this.hideDom()
            this.props.updateItem(this.props.data.cur,rmType)
        })
    }

    //关台
    closeRoom(type) {
        this.putRoomType(type)
    }

    //故障 、 置空闲 、置清扫
    transfer() {
        this.putRoomType(this.state.curType)
    }

    //隐藏Dialog
    hideDom() {
        this.setState({
            isShow:false
        })
    }

    templateList() {
        const items = []
        const data = this.state
        const list = this.props.data.list || []
        LIBrary.times(list.length, (index) => {
            let curClass
            if(!data.curIndex&&data.curIndex!==0) {
                curClass = ""
            }else if(list[index]!=ROOMCOM.ROPERATE_TYPE_BREAK) {
                curClass =  data.curIndex==index?"red":""
            }else {
                curClass = data.curIndex==index?"gray":""
            }
            items.push(<li key={index}
                           className={"operateItem  "+curClass}>
                    <a onClick={(type)=>this.templateDialogChildren(list[index],index)}>
                   {list[index]}
                   </a></li>)
        })
        return items
    }

    templateChildren(type) {
        let children
        const obj = this.props.data.dataList?this.props.data.dataList[this.props.data.cur]:''
        console.log("roomItems obj is :"+JSON.stringify(obj))
        switch (type) {
            case ROOMCOM.ROPERATE_TYPE_CONTINUE:
                //获取续时计费  套餐
                children = (<div className="dialog-body">
                    <div>
                        <lable>续时时长</lable>
                        <input type="number" />
                        <lable>分钟</lable>
                    </div>
                    <div className="case">
                        <PatternBar data=""/>
                    </div>
                    <div className="makeSure">
                        <button onClick={()=>this.transfer(type)}>确认</button>
                    </div>
                </div>)
                break
            //转台
            case ROOMCOM.ROPERATE_TYPE_SWITCH:
                children = (
                    <div>
                        <label>转到</label>
                        <DropDownButton data={this.getRoomList()} onSelectItem={(n)=>this.onSelectItem(n)}/>
                    </div>
                )
                break
            case ROOMCOM.ROPERATE_TYPE_PAYBF:
                children = (
                    <div>
                        <label></label>
                        <input type="number" onChange={()=>this.changePayBefore}/>
                        <label>支付方式</label>
                        <DropDownButton data={this.getRoomList()} onSelectItem={(n)=>this.onSelectItem(n)}/>
                    </div>
                )
                break
            case ROOMCOM.ROPERATE_TYPE_BACK://退单
                children = (
                    <BackOrder msg={obj}/>
                )
                break
            case ROOMCOM.ROPERATE_TYPE_SETFREE://置空闲
                children = this.templateYORN()
                break
            case ROOMCOM.ROPERATE_TYPE_SETCLEAN://置清扫
                children = this.templateYORN()
                break
            default:break
        }
        return children
    }

    templateYORN() {
        return (
            <div className='addRoombox'>
                <button onClick={()=>this.hideDom()} className='dialogLittltButton dialogLittltButtonChange'>取消</button>
                <button onClick={()=>this.putRoomType(this.state.curType)} className='dialogLittltButton dialogLittltButtonDelete'>确定</button>
            </div>
        )
    }

    templateDialogChildren(type,idx) {
        //当前操作列表数组 根据不同type做判断
        this.getRoomList()
        const obj = this.props.data.dataList[this.props.data.cur]
        if(type===ROOMCOM.ROPERATE_TYPE_FREE) {
            this.setState({curIndex:idx,curType:type})
            this.jumpNextLink(obj,'/openRoom')
        } else if (type === ROOMCOM.ROPERATE_TYPE_SETTLE) {
            this.setState({curIndex:idx,curType:type})
            this.jumpNextLink(obj,'/roomSettle')
        } else if (type === ROOMCOM.ROPERATE_TYPE_CONTINUE||
            type === ROOMCOM.ROPERATE_TYPE_SWITCH||
            type === ROOMCOM.ROPERATE_TYPE_PAYBF||
            type === ROOMCOM.ROPERATE_TYPE_BACK) {
            this.setState({
                isShow:true,
                isBreak:false,
                curIndex:idx,
                curType:type
            })
        } else if (type === ROOMCOM.ROPERATE_TYPE_SETFREE ||
            type === ROOMCOM.ROPERATE_TYPE_SETCLEAN  ||
            type === ROOMCOM.ROPERATE_TYPE_BREAK||
            type === ROOMCOM.ROPERATE_TYPE_SPOT) {
            let isBreak = false
            let show = true
            if (type === ROOMCOM.ROPERATE_TYPE_BREAK) {
                isBreak = true
                show = false
            }
            this.setState({
                isBreak:isBreak,
                isShow:show,
                curType:type,
                curIndex:idx
            })
            if (type === ROOMCOM.ROPERATE_TYPE_SPOT) {
                this.jumpNextLink(obj,'/foodOrder')
            }
        } else if (type===ROOMCOM.ROPERATE_TYPE_DETAIL) {
            this.setState({
                isBreak:false,
                curIndex:idx
            })
            this.jumpNextLink(obj,'/consumerDetail')
        } else if (type === ROOMCOM.ROPERATE_TYPE_CLOSE) {
            this.setState({
                isBreak:false,
                curIndex:idx,
                curType:type
            })
            this.closeRoom(type)
        }
    }

    render() {
        const data = this.state
        const type = this.props.data.dataList && this.props.data.dataList.length ? this.props.data.dataList[this.props.data.cur].name : ''
        return (
            <div className="roomItems">
                <Dialog title={type}
                        show={data.isShow}
                        onHide={(e) => this.hideDom() }
                        children={this.templateChildren(data.curType)}/>
                <ul>{this.templateList()}</ul>
                <div className={"editReason "+(data.isBreak?"":"hide")}>
                    <textarea placeholder="请输入故障详情"></textarea>
                    <button onClick={()=>this.transfer()}>确定</button>
                </div>
            </div>
        )
    }
}
