import React from 'react'
import LIBrary from '../lib/util'
import JSBridge from '../lib/bridge'
import ROOMCOMMON from './roomCommon'
import PAY from '../lib/payMothed'
import COMMON from '../lib/common'
import {PRINRNOTES} from '../lib/printNote'
export default class Settle extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            rate: 100,
            money: 0,
            showDialog: false,
            time: 20
        }
        this.orderNo = props.roomMsg.orderNo
        this.roomName = props.roomMsg.name
        this.rtId = props.roomMsg.rtId
        this.roomId = props.roomMsg.rmId
        this.payType = props.roomMsg.payType
    }

    componentWillReceiveProps(props) {
        this.setState({
            money: props.money
        })
    }

    delayPrintNote() {
        PRINRNOTES.AFSingleBill(this.state.orderNo, this.state.billNo)
        window.location.href = './index.html#/roomMessage'
    }

    //打印
    print(values) {
        this.props.print(values)
    }

    //返回上一级
    back() {
        this.props.back()
    }

    count(e) {
        const rate = Zepto('input[name=rate]').val()
        const money = (rate/100)*this.state.money
        this.setState({
            money: money
        })
    }

    countCheck(e) {
        const realMoney = e.target.value
        const check = realMoney - Zepto('input[name=money]').val()
        if(check < 0){
            LIBrary.warnning('实收金额请大于应收金额')
            return
        }
        Zepto('input[name=check]').val(check.toFixed(2))
    }

    //结账
    settle(value, callback) {
        const buyItems = Zepto('div.buyItem')
        let products = []
        let packs = []
        let foods = []
        buyItems.map((index, obj) => {
            let itemProducts = {}
            let itemPacks = {}
            let item = {}
            if(Zepto(obj).find('input[name=product_id]').val()>=0){
                itemProducts.product_id = parseInt(Zepto(obj).find('input[name=product_id]').val())
                itemProducts.count = parseInt(Zepto(obj).find('input[name=count]').attr('value'))
                products.push(itemProducts)
            }

            if(Zepto(obj).find('input[name=pack_id]').val()>=0){
                itemPacks.pack_id = parseInt(Zepto(obj).find('input[name=pack_id]').val())
                itemPacks.count = parseInt(Zepto(obj).find('input[name=count]').attr('value'))
                packs.push(itemPacks)
            }
            item.beerCount = parseInt(Zepto(obj).find('input[name=count]').attr('value'))
            item.beerUnitPrice = Zepto(obj).find('span.price').text()
            item.beerName = Zepto(obj).find('span.name').text()
            item.beerSubtotal = Zepto(obj).find('span.totalMoney').text()
            foods.push(item)
        })
        value.packs = packs
        value.products = products
        delete(value.room)
        delete(value.check)
        LIBrary.fetch('/order/product',{
            method: 'post',
            body: JSON.stringify(value)
        }, (res) => {
            if(value['pay_type'] != 1 || (value['pay_md'] != 2 && value['pay_md'] != 3 && value['pay_md'] != 4)){
                PRINRNOTES.AllBill(this.orderNo,res.detail.bill_no)
                setTimeout(() => {
                    window.location.href = './index.html#/roomMessage'
                }, 1000);
            }
            this.setState({
                orderNo: this.orderNo,
                billNo: res.detail.bill_no
            })
            typeof callback == 'function' ? callback(value) : ''
        })
    }

    templatePayType(params) {
        let items = []
        LIBrary.times(params.length, (idx) => {
            items.push(
                <option key={idx} value={(idx+1)}>{ params[idx] }</option>
            )
        })
        return items
    }

    submit(e) {
        e.preventDefault()
        const method = e.target.name
        const form = Zepto(e.target).parent()
        const date = new Date()
        let values = {}
        let flag = false
        let str = ''
        values['pay_md'] = form.find('select').val()
        form.find('input').map((index, input) => {
            if(parseInt(values['pay_md']) == 1 &&
            input.value == ''){
                flag = true
                return
            }
            if(method == 'confirm' &&
            parseInt(values['pay_md']) == 1 &&
            input.name == 'real_money' &&
            parseInt(input.value*100) <=
            parseInt(values['money'])){
                flag = true
                str = '实收金额请大于等于应收金额'
                return
            }
            values[input.name] = input.value
        })
        if(flag){
            LIBrary.warnning(str)
            return
        }
        values['money'] *= 100
        values['real_money'] = parseInt(values['real_money']*100) - parseInt(values['check']*100)
        values['describe'] = form.find('textarea').val()
        values['state'] = parseInt(values['pay_md']) == 1 ? 1 : 0
        switch (method) {
            case 'confirm':
            //确认结账
                this.settle(values, this.commfirPay.bind(this))
                break;
            case  'print':
            // 结账打印
                this.print(values)
                break
            case 'back':
                // 返回上一级
                this.props.back()
                break
            default:
        }

    }

    commfirPay(values) {
        switch (parseInt(values['pay_md'])){
            case 1:
                JSBridge.test('openBox')
            break;

            case 2:
                Zepto('input#paycode').attr('type', 'text')
                this.startGetCode(values, 'wx')
            break;

            case 3:
                this.startGetCode(values, 'ali')
            break

            case 4:
            break;

            case 5:
            break
        }
    }

    startGetCode(value, type) {
        let timer = ''
        let code = ''
        Zepto('input#paycode').show()
        Zepto('input#paycode').trigger('focus')
        setTimeout(() => {
            code = Zepto('input#paycode').val()
            if(code){
                this.startPay(value, code, type)
                Zepto('input#paycode').val('')
                Zepto('input#paycode').attr('type','hidden')
                LIBrary.warnning('扫码成功')
                return
            }
            this.startGetCode(value, type)
        }, 1000)
    }

    startPay(value, code, type) {
        let parma = PAY.PAYPARMA
        parma.paytype = type
        parma.ktvid = Zepto('input#storeId').attr('value')
        parma.date = COMMON.TIME.toString()
        parma.paraTotalFee = value['money']
        parma.erpid = this.state.orderNo
        parma.time = COMMON.TIME.toString()
        parma.data.ktv_id = Zepto('input#storeId').attr('value')
        parma.data.paraBody = 'ktv,点单'
        parma.data.paraAuthCode = code
        parma.data.erp_id = this.state.orderNo
        PAY.payMothed(parma, this.delayPrintNote.bind(this))
        LIBrary.countDown()
    }

    templatePayWay() {
        let money = LIBrary.Fix2(this.state.money)
        var template = <div>
            <p className='payType'><label className='checkPay1 payKind1'>支付方式:</label>
                <select name='pay_md' className='payment'>
                    {this.templatePayType(ROOMCOMMON.RPAYMDS)}
                </select>
            </p>
            <p className='settleDiscount'><label className='checkPay1 payKind2'>打折: </label><input type='text' name='rate' defaultValue={ this.state.rate } onBlur={(e) => this.count(e)}/>%<br/><span className='feeDiscount'>*房费已优惠</span></p>
            <p className='settleDiscount'><label className='checkPay2 payKind2'>应收: </label><input type='hidden' name='money' defaultValue={ money } /><span> {money} </span></p>
            <p className='settleDiscount'><label className='checkPay1 payKind2'>实收: </label><input type='text' name='real_money' defaultValue={ '0.00'  } onBlur={(e) => this.countCheck(e)}/></p>
            <p className='settleDiscount'><label className='checkPay2 payKind2'>找零: </label><input type='text' name='check' defaultValue={ '0.00'  }/></p>
        </div>
        return template
    }

    render() {
        let money = LIBrary.Fix2(this.state.money)
        let style = {
            marginBottom: '100px'
        }

         return (
            <div style={ style }>
                <p><input type='hidden' id='paycode' name='paycode' /></p>
                <form>
                    <h1>送达包房: <span>{ this.roomName }</span>
                        <input type='hidden' name='room' defaultValue={ this.roomName }/>
                    </h1>
                    <p style={{ display: (this.needRoomNum ? 'display' : 'none') }}>
                        <input type='hidden' name='rt_id' defaultValue={ this.rtId }/>
                        <input type='hidden' name='room_id' defaultValue={ this.roomId } />
                        <input type='hidden' name='order_no' defaultValue={this.orderNo} />
                    </p>
                    {this.payType ? this.templatePayWay() : ''}
                    <div className='editReason'>
                        <textarea placeholder='备注： 100字内' name='describe' className='remark'></textarea>
                    </div>
                    <button name='back' onClick={(e) => this.submit(e)} className='checkOutBtn checkOutBtnBack'>返回上一级</button>
                    <button name='print' onClick={(e) => this.submit(e)} className='checkOutBtn checkOutBtnThreaten'>打单</button>
                    <button name='confirm' onClick={(e) => this.submit(e)} className='checkOutBtn checkOutBtnComfirm'>确认</button>
                </form>
                <p><label>付款码</label><input id='paycode' type='hidden'/></p>
            </div>
        )
    }
}
