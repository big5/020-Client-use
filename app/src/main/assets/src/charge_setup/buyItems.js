import React from 'react'
import LIBrary from '../lib/util'
import MealItems from './mealItems'
import ProductsItem from './productsItem'

export default class BuyItems extends React.Component {
    constructor(props) {
        super(props)
        this.state={
            product: props.product,
            cateId: props.cateId,
            totalProductMoney: 0,
            totalPackMoney: 0,
        }
    }

    componentWillReceiveProps(props){
        this.setState({
            product: props.product,
            cateId: props.cateId
        })
    }

    resetdetails() {
        this.setState({
            totalProductMoney: 0,
            totalPackMoney: 0
        })
        LIBrary.eventproxy.emit('resetBuyItems',true)
    }

    totalProductMoney(money){
        this.setState({
            totalProductMoney: money
        }, () => this.getDetail())
    }

    totalPackMoney(money){
        this.setState({
            totalPackMoney: money
        }, () => this.getDetail())
    }

    getDetail(){
        let detail = {}
        detail.product_ids = []
        detail.pack_ids = []
        Zepto('.buyItem').map((index, dom) => {
            let productId = Zepto(dom).find('input[name=product_id]').val()
            let packId = Zepto(dom).find('input[name=pack_id]').val()
            productId || productId == 0 ? detail.product_ids.push(productId) : ''
            packId || packId == 0 ? detail.pack_ids.push(packId) : ''
            detail.money = this.state.totalPackMoney + this.state.totalProductMoney
        })
        this.props.getDetail(detail)
    }

    render() {
        let totalMoney = ((this.state.totalPackMoney + this.state.totalProductMoney)/100).toFixed(2)
        return(
            <div className='shoppingCart'>
                <ul className='shoppingCartList'>
                    <li>商品名称</li>
                    <li>单价</li>
                    <li>数量</li>
                    <li>小计</li>
                    <li>操作</li>
                </ul>
                <div className='shoppingCartDetail'>
                    <ProductsItem
                        cateId={this.state.cateId}
                        product={this.state.product}
                        totalproductMoney={(money) => this.totalProductMoney(money)}/>
                    <MealItems
                        cateId={this.state.cateId}
                        product={this.state.product}
                        totalPackMoney={(money) => this.totalPackMoney(money)}/>
                </div>
                <p className='cartTotalPrice'>
                    <span onClick={() => this.resetdetails()} className='empty'>清空</span>
                    <span className='totalPrice'>总金额：</span>
                    <span>{ totalMoney }</span>
                </p>
            </div>
        )
    }
}
