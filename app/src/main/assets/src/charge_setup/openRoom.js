/**
 * Created by lixinfeng on 2017/4/24.
 */

import React from 'react'
import Title from '../room_setup/title'
import RoomTableMsg from './roomTableMsg'
import ConsumeType from './consumeType'
import ROOMCOM from './roomCommon'

export default class OpenRoom extends React.Component {
    constructor(props) {
        super(props)
        this.state={
            roomMsg:props.location.state.roomMsg,
            tableObj:{},
            wineObj:{},
            sellType:ROOMCOM.RSELLTYPES[0]
        }
        console.log("openRoom's roomMsg is :"+JSON.stringify(this.state.roomMsg))
    }

    shouldComponentUpdate(nextProps,nextState) {
        if (this.state.tableObj== nextState.tableObj){
            return false
        }
        return true
    }

    getTableObj(obj) {
        this.setState({
            tableObj:obj
        })
        console.log("开台时:"+JSON.stringify(obj))
    }

    render() {
        const data = this.state
        return (
            <div className="openRoom roomController">
                <Title/>
                <div className='openRoomDown'>
                    <RoomTableMsg data={{
                    roomMsg:data.roomMsg,
                    tableObj:data.tableObj,
                    wineObj:data.wineObj
                    }} />
                    <ConsumeType data={{roomMsg:data.roomMsg,sellType:data.sellType}}
                             getTableObj={(obj)=>this.getTableObj(obj)}
                             />
                </div>
            </div>
        )
    }
}
