import React from 'react'
import LIBrary from '../lib/util'
import ROOMCOM from './roomCommon'
import {REQUEST_URLS} from './setting'
import {PRINRNOTES, HANDLE} from '../lib/printNote'

export default class BackOrder extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            product: [],
            money: 0,
            payMd:1,
            numbers: []
        }
        this.roomId = props.msg.rmId
        this.rtId = props.msg.rtId
        this.payType = props.msg.payType
        this.orderNo = props.msg.orderNo
    }

    componentDidMount() {
        this.getPacks(this.orderNo)
    }

    incrasement(e) {
        let tr = Zepto(e.target).parents('tr')
        let value = parseInt(tr.find('em').html())
        let count = parseInt(tr.attr('data-count'))
        if(value >= count){
            return
        }
        tr.find('em').html(value+1)
        let money = this.state.money +
        parseInt(tr.attr('data-price'))
        this.setState({
            money: money
        })
    }

    decrasement(e) {
        let tr = Zepto(e.target).parents('tr')
        let value = tr.find('em').html()
        if(parseInt(value) == 0){
            return
        }
        tr.find('em').html(value-1)
        let money = this.state.money -
        parseInt(tr.attr('data-price'))
        this.setState({
            money: money
        })
    }

    getPacks(params) {
        LIBrary.fetch(REQUEST_URLS.orderProduct +
        '?order_no=' + params, {
            method: 'get'
        }, (res) => {
            let product = HANDLE.combineProduct(res.list)
            this.setState({
                product: product,
                orderNo: params
            })
        })
    }

    cirfimBack(e) {
        const product = Zepto('em.product')
        let itemPack = []
        let itemProduct = []
        let value = {}
        product.map((index, values) => {
            let item = {}
            item.count = parseInt(Zepto(values).html())
            item.product_id = parseInt(Zepto(values).attr('data-productId'))
            itemProduct.push(item)
        })

        value.pay_md = this.state.payMd
        value.money = this.state.money
        value.rt_id = this.rtId
        value.room_id = this.roomId
        value.order_no = this.orderNo
        value.products = itemProduct

        LIBrary.fetch(REQUEST_URLS.orderProduct, {
            method: 'POST',
            body: JSON.stringify(value)
        }, (res) => {
            PRINRNOTES.AFSingleBill(this.orderNo, res.detail.bill_no)
        })
    }

    templateCount(param) {
        return param['product_id'] ?  <section>
            <span onClick={(e) => this.decrasement(e)}> - </span>
            <em className='product' data-productId={param['product_id']}>0 </em>
            <span onClick={(e) => this.incrasement(e)}> + </span>
        </section> : <section> 未开通 </section>
    }

    templateCates(param) {
        if (param.length <= 0) {
            return
        }
        let items = []
        let values = []
        this.state.product.map((value, index) => {
            let payState = value['pay_state'] == 0 ? '未付' :(
            value['pay_state'] == '1' ? '已付' : '退单')
            items.push(
                <tr key={index} data-count={value['count']} data-price={ value['price'] }>
                    <td>{value['name']}</td>
                    <td>{value['unit']}</td>
                    <td>{LIBrary.Fix2(value['price'])}</td>
                    <td>{value['count']}</td>
                    <td>{value['create_time']}</td>
                    <td>{payState}</td>
                    <td>{LIBrary.Fix2(value['money'])}</td>
                    <td>
                        {this.templateCount(value)}
                    </td>
                </tr>
            )
        })
        return items
    }

    render() {
        return (
            <div className="roomState roomController addRoombox">
                <h1>退单</h1>
                <table>
                    <thead>
                        <tr>
                            <td>商品名称</td>
                            <td>单位</td>
                            <td>单价（元）</td>
                            <td>数量</td>
                            <td>下单时间</td>
                            <td>支付状态</td>
                            <td>金额（元）</td>
                            <td>退单数量</td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.templateCates(this.state.product)}
                    </tbody>
                </table>
                <p> 付款方式：{ROOMCOM.RPAYMDS[this.state.payMd-1]} <span></span>总计： {LIBrary.Fix2(this.state.money)}</p>
                <button onClick={(e) => this.cirfimBack(e)}> 确认 </button>
            </div>
        )
    }
}
