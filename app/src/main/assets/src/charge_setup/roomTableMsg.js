/**
 * Created by lixinfeng on 2017/5/3.
 */

import React from 'react'
import LIBrary from '../lib/util'
import ROOMCOM from './roomCommon'
import moment from  'moment'
import RoomCard from './roomCard'
import DetailCard from './detailCard'
import CustomTable from './customTable'

export default class roomTableMsg extends React.Component {
    constructor(props) {
        super(props)
        this.getUseTime = this.getUseTime.bind(this)
        this.state = {
            data:props.data,
            roomMsg: props.data.roomMsg || {},
            tableObj: props.data.tableObj || {},
            wineObj: props.data.wineObj || {}
        }
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.data.tableObj!=this.state.tableObj ||
            nextProps.data.wineObj!=this.state.wineObj ||
            nextProps.data.roomMsg!=this.state.roomMsg){
            this.setState({
                data:nextProps.data,
                roomMsg: nextProps.data.roomMsg || {},
                tableObj:nextProps.data.tableObj || {},
                wineObj:nextProps.data.wineObj || {}
            })
        }
    }

    getMoney(obj) {
        let m = 0
        if (Array.isArray(obj)) {
            obj.map((ob)=>{
                m +=ob.money
            })
            return (m/100).toFixed(2)
        } else {
             if (!obj.pack_id) {
                 for (let i in obj.list) {
                     m += obj.list[i].money
                 }
                 m = (m/100).toFixed(2)
             } else {
                 m = obj.price?(obj.price/100).toFixed(2):''
             }
        }
        return m
    }

    getUseTime(st) {
        let cur = moment()
        return st ? cur.diff(moment(st,'HH:mm'),'minutes'):''
    }

    getTime(obj) {
        if (Array.isArray(obj)) {
            return this.state.data.minute
        } else {
            if(!obj.minute) {
                let  totalTime = 0
                for (let i in obj.list) {
                    totalTime += obj.list[i].minute
                }
                return totalTime
            } else {return obj.minute?obj.minute:''}
        }
    }

    //总应收
    getTotalMoney() {
        return this.totalMoney ? this.totalMoney:parseFloat(this.getMoney(this.state.tableObj))+parseFloat(this.getMoney(this.state.wineObj))
    }

    templateRoomDetail() {
        return(<div className="roomDetail">
            <DetailCard title='包房基本信息:' children={this.templateRoomMsg()}/>
            <DetailCard title='房费账单:' children={this.templateRoomCost()} footer={this.templateCardProfit(ROOMCOM.RCOST)}/>
            <DetailCard title='酒水账单:' children={this.templateRoomWine()} footer={this.templateCardProfit(ROOMCOM.RWINE)}/>
            <div className="roomDetail-footer">
                {this.templateRoomProfit()}
            </div>
        </div>)
    }

    templateTableTitle(filedArr,conArr) {
        const wines = []
        LIBrary.times(conArr.length,(index)=>{
            wines.push(<th key={index} className title data-field={filedArr[index]}>{conArr[index]}</th>)
        })
        return wines
    }

    templateRoomMsg() {
        const s_Data = this.state
        let st,ed
        if (!s_Data.data.order_no) {
            if (s_Data.tableObj.st) {
                st = s_Data.tableObj.st
                ed = s_Data.tableObj.ed
            } else if (s_Data.tableObj.list) {
                st = s_Data.tableObj.list[0].st
                ed = s_Data.tableObj.list[s_Data.tableObj.list.length-1].ed
            } else {
                st = ''
                ed = ''}
        } else {
            st = s_Data.data.st
            ed = s_Data.data.ed
        }
        if(!s_Data.data.order_no) {
            return (
                <ul>
                    <li>包房名称:{s_Data.roomMsg.name||''+s_Data.roomMsg.rtName||''}</li>
                    <li className='openRoomTime'>开台时间:{st}</li>
                    <li>使用时长:{this.getUseTime(st)}</li>
                    <li>关台时间:{ed}</li>
                </ul>
            )
        } else {
            return (
                <ul>
                    <li>包房名称:{s_Data.roomMsg.name || '' + s_Data.roomMsg.rtName||''}</li>
                    <li>账单编号:{s_Data.data.order_no||''}</li>
                    <li>房间状态:{ROOMCOM[ROOMCOM.RDESCRIBE + s_Data.data.payState]}</li>
                    <li>支付状态:{ROOMCOM.RPAYTYPES[s_Data.data.payType-1]||''}</li>
                    <li>开台时间:{st || ''}</li>
                    <li>关台时间:{ed || ''}</li>
                    <li>使用时长:{st ? this.getUseTime(st):''}分钟</li>
                    <li>当前消费:{this.getTotalMoney()||''}</li>
                </ul>
            )
        }
    }

    isExpend(obj) {
        if (Array.isArray(obj)) {
            return ROOMCOM.RCOSTDET//明细 可操作
        }
        return ROOMCOM.RCOST //无订单号 不可操作
    }
    
    //房费
    templateRoomCost() {
        return (
            <CustomTable  data={this.state.tableObj}  expend={this.isExpend(this.state.tableObj)} type={ROOMCOM.RCOST}>
                {this.templateTableTitle(ROOMCOM.RCOSTBILL_ENG,ROOMCOM.RCOSTBILL)}
            </CustomTable>
        )
    }

    //酒水
    templateRoomWine() {
        return (
            <div>
                <CustomTable  data={this.state.wineObj} expend={ROOMCOM.RWINE} type={ROOMCOM.RWINE}>
                    {this.templateTableTitle(ROOMCOM.RWINELIST_ENG,ROOMCOM.RWINELIST)}
                </CustomTable>
            </div>
        )
    }

    templateCardProfit(type) {
        let obj
        let totalTime = ''
        if (type==ROOMCOM.RCOST) {
            obj = this.state.tableObj
            totalTime = '(总计时长'+this.getTime(obj)+'分钟)'
        } else {obj = this.state.wineObj}
        return (
            <p>{totalTime}  小计：{this.getMoney(obj)}元
                <span className='receivable'>应收：{this.getMoney(obj)}元</span>
            </p>
        )
    }

    //footer 总应收
    templateRoomProfit() {
        return(<p className='allReceivable'>总应收：{this.getTotalMoney()}元</p>)
    }

    render() {
        return (
            <div className="roomTableMsg  leftCon">
                <RoomCard title=''
                          children={this.templateRoomDetail()}/>
            </div>
        )
    }
}
