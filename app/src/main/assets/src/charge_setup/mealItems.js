import React from 'react'
import {REQUEST_URLS} from './setting'
import LIBrary from '../lib/util'
import ItemsCell from './itemCell'

export default class MealItems extends React.Component {
    constructor(props) {
        super(props)
        this.state={
            pack:'',
            packIds: [],
            packNum: [],
            packs: []
        }
    }

    componentWillMount(){
        LIBrary.eventproxy.on('resetBuyItems',(data) => {
            this.setState({
                pack: '',
                packIds: [],
                packNum: [],
                packs: []
            })
        })
    }

    componentWillReceiveProps(props){
        if(typeof props.product != 'object' || props.cateId >= 0||
            !props.product['pack_id']||
            this.state.packIds.indexOf(props.product['pack_id']) >= 0 ||
            this.state.pack == props.product){
            return
        }
        this.getPack(props.product)
    }

    putNum(num, idx, isPack){
        let packNum = this.state.packNum
        packNum[idx] = num
        this.setState({
            packNum: packNum
        }, ()=> {
            this.totalMoney()
        })
    }

    totalMoney(){
        let totalMoney = 0
        this.state.packNum.map((value, index) => {
            totalMoney += value * this.state.packs[index]['price']
        })
        this.props.totalPackMoney(totalMoney)
    }

    deletePack(index) {
        let packs = LIBrary.removeByIndex(this.state.packs, index)
        let packIds = LIBrary.removeByIndex(this.state.packIds, index)
        let packNum = LIBrary.removeByIndex(this.state.packNum, index)
        this.setState({
            packs: packs,
            packIds: packIds,
            packNum: packNum
        } ,()=> {
            this.totalMoney()
        })
    }

    getPack(product){
        let packs = this.state.packs
        let packIds = this.state.packIds
        let packNum = this.state.packNum
        packs.push(product)
        packIds.push(product['pack_id'])
        packNum.push(1)
        this.setState({
            pack: product,
            packs: packs,
            packIds: packIds,
            packNum: packNum
        },() => {
            this.totalMoney()
        })
    }

    templatePacks(params) {
        let items = []
        LIBrary.times(params.length, (idx) =>{
            let item = params[idx]
            items.push(
                <ItemsCell
                    key={idx}
                    product={ item }
                    putNum={ (count) => this.putNum(count, idx) }
                    deleteProudct={ () => this.deletePack(idx) }/>
            )
        })
        return items
    }

    render() {
        return(
            <ul>
                { this.templatePacks(this.state.packs) }
            </ul>
        )
    }
}
