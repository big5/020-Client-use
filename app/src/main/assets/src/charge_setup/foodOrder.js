
import React from 'react'
import ROOMMON from './roomCommon'
import { REQUEST_URLS } from './setting'
import LIBrary from '../lib/util'
import Title from '../room_setup/title'
import LeftNav from './leftNav'
import Products from './products'
import BuyItems from './buyItems'
import SettleFoods from './settleFoods'
import PRINTMODUL from '../lib/printModul'
import {PRINRNOTES} from '../lib/printNote'

export default class FoodOrder extends React.Component {
    constructor(props) {
        super(props)
        this.state={
            cateId: 0,
            product: '',
            detail: '',
            products: []
        }
        this.roomMsg = props.location.state.roomMsg
    }

    componentDidMount(){
        LIBrary.eventproxy.on('resetBuyItems',() => {
            this.setState({
                product: '',
                products: []
            })
        })
    }

    deleteCates(index) {
        let Products = LIBrary.removeByIndex(this.state.products, index)
        this.setState({
            products: Products
        })
    }

    chooseCates(id) {
        this.setState({
            cateId: id
        })
    }

    chooseProducts(product) {
        this.setState({
            product: product,
            products: this.state.products.concat(product)
        })
    }

    getDetail(params) {
        this.setState({
            detail: params
        })
    }

    print(value) {
        const buyItems = Zepto('div.buyItem')
        let foods = []
        let totalMoney = 0
        buyItems.map((index, obj) => {
            let item = {}
            totalMoney += parseInt(Zepto(obj).find('span.totalMoney').text())
            item.beerCount = parseInt(Zepto(obj).find('input[name=count]').attr('value'))
            item.beerUnitPrice = Zepto(obj).find('span.price').text()
            item.beerName = Zepto(obj).find('span.name').text()
            item.beerSubtotal = Zepto(obj).find('span.totalMoney').text()
            foods.push(item)
        })
        PRINTMODUL.beer = foods
        PRINTMODUL.titleType = '收款单'
        PRINTMODUL.OperationType = '点单'
        PRINTMODUL.roomName = value.room
        PRINTMODUL.originalBill = value.order_no
        PRINTMODUL.shouldIncome = totalMoney
        PRINTMODUL.autograph = 0
        PRINRNOTES.print(PRINTMODUL)
    }

    render() {
        let data = this.state
        let money = data.detail && data.detail['money'] ? data.detail['money'] : ''
        return(
            <div className='openRoom'>
                <Title />
                <div className='openRoomDown'>
                    <div className='leftCon shopping'>
                        <LeftNav cateId={data.cateId} chooseCates={(id) => this.chooseCates(id)}/>
                        <Products cateId={data.cateId } chooseProducts={ (product) => this.chooseProducts(product) }/>
                        <BuyItems product={data.product} cateId={ data.cateId } getDetail={(params) => this.getDetail(params)}/>
                    </div>
                    <div className='rightCon'>
                        <SettleFoods money={ money } roomMsg={ this.roomMsg } settle={(value) => this.settle(value)} print={(value) => this.print(value)}/>
                    </div>
                </div>
            </div>
        )
    }
}
