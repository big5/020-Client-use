/**
 * Created by lixinfeng on 2017/5/16.
 */

import React from 'react'
import LIBrary from '../lib/util'
import { REQUEST_URLS } from './setting'
import Title from '../room_setup/title'
import RoomTableMsg from './roomTableMsg'

export default class ConsumerDetail extends React.Component {
    constructor(props) {
        super(props)
        this.state= {
            roomMsg: props.location.state.roomMsg,
            tableObj: {},
            wineObj: {}
        }
    }

    componentDidMount() {
        const data = this.state
        LIBrary.fetch(REQUEST_URLS.orderDetail+"?order_no="+data.roomMsg.orderNo,{
            method: 'GET'
        }, (res) => {
            let wine = []
            let cost = []
            // let res = this.props.msg
            // console.log("consumeDetail cost componentDidMount data is :"+JSON.stringify(res)+'\n')
            if (res.detail.bills.length) {
                res.detail.bills.map((obj)=>{
                    let s = obj.bill_no.substr(0,2)
                    let len = 0
                    obj.list.map((ob)=>{
                        (ob.pack_id && ob.list && ob.list.length) ? ob.expend = 'expend':''
                        len += 1
                    })
                    obj.len = len
                    switch (s) {
                        case 'KT':
                            cost.push(obj)
                            break
                        case 'DD':
                            wine.push(obj)
                            break
                        case 'XS':
                            cost.push(obj)
                            break
                        default:break
                    }
                })
            }
            // console.log("consumeDetail cost componentDidMount data is :"+JSON.stringify(cost)+'\n',
            // )
            this.setState({
                tableObj: cost,
                st:res.detail.st,
                ed:res.detail.ed,
                minute:res.detail.minute,
                order_no:res.detail.order_no,
                payState:res.detail.state,
                payType:res.detail.pay_type,
                wineObj: wine
            })
        })
    }

    render() {
        const data = this.state
        return(
            <div className="consumerDetail roomController openRoom">
                <Title/>
                <div className='openRoomDown'>
                    <RoomTableMsg data={{
                        roomMsg:data.roomMsg,
                        tableObj:data.tableObj,
                        wineObj:data.wineObj,
                        st:data.st,
                        ed:data.ed,
                        minute:data.minute,
                        order_no:data.order_no,
                        payState:data.payState,
                        payType:data.payType || data.roomMsg.payType
                    }} />
                </div>
            </div>
        )
    }
}

ConsumerDetail.defaultProps={
    msg : {
        "errcode": 200,
        "errmsg": "服务正常",
        "detail": {
            "describe": "123",
            "st": "14:29",
            "update_time": "2017-05-17 15:05:52",
            "room_name": "1101",
            "ed": "17:29",
            "order_no": "2017051614313912",
            "state": 1,
            "prepay": 0,
            "pay_type": 1,
            "room_id": 277,
            "minute": 180,
            "bills": [
                {
                    "describe": "",
                    "real_money": 55000,
                    "pay_md": 1,
                    "rate": 100,
                    "bill_no": "KT2017051614313985",
                    "pay_state": 1,
                    "update_time": "2017-05-16 14:31:39",
                    "list": [
                        {
                            "ed": "17:29",
                            "st": "14:29",
                            "md": 1,
                            "minute": 180,
                            "update_time": "2017-05-16 14:31:39",
                            "fee_id": 299,
                            "money": 23940,
                            "pack_id": 0
                        },
                        {
                            "ed": "14:29",
                            "st": "10:29",
                            "md": 1,
                            "minute": 240,
                            "update_time": "2017-05-16 14:31:39",
                            "fee_id": 299,
                            "money": 32000,
                            "pack_id": 0
                        }
                    ],
                    "money": 55000,
                    "service_type": 1
                },
                {
                    "describe": "",
                    "real_money": 55000,
                    "pay_md": 1,
                    "rate": 100,
                    "bill_no": "XS2017051614313985",
                    "pay_state": 1,
                    "update_time": "2017-05-16 14:31:39",
                    "list": [
                        {
                            "ed": "17:29",
                            "st": "14:29",
                            "md": 1,
                            "minute": 180,
                            "update_time": "2017-05-16 14:31:39",
                            "fee_id": 299,
                            "money": 23940,
                            "pack_id": 0
                        },
                        {
                            "ed": "18:00",
                            "st": "15:00",
                            "price": 20000,
                            "minute": 180,
                            "name": "叽叽歪歪",
                            "pack_id": 131,
                            "list": [
                                {
                                    "pic": "xx",
                                    "price": 10000,
                                    "order": 1,
                                    "update_time": "2017-04-11 18:43:51",
                                    "cate_id": 9,
                                    "spec": "",
                                    "discount": 0,
                                    "product_id": 1,
                                    "state": 1,
                                    "count": 15,
                                    "unit": "箱",
                                    "store_id": 10000040,
                                    "name": "雪花纯生",
                                    "stock": 0
                                },
                                {
                                    "pic": "22",
                                    "price": 2222,
                                    "order": 1,
                                    "update_time": "2017-04-13 19:26:52",
                                    "cate_id": 8,
                                    "spec": "22ml",
                                    "discount": 0,
                                    "product_id": 2,
                                    "state": 2,
                                    "count": 22,
                                    "unit": "瓶",
                                    "store_id": 10000039,
                                    "name": "222",
                                    "stock": 220
                                }
                            ]
                        }
                    ],
                    "money": 55000,
                    "service_type": 1
                },
                {
                    "describe": "123",
                    "real_money": 48200,
                    "pay_md": 1,
                    "rate": 100,
                    "bill_no": "DD2017051614344131",
                    "pay_state": 1,
                    "update_time": "2017-05-16 14:34:41",
                    "list": [
                        {
                            "product_id": 171,
                            "price": 1000,
                            "count": 12,
                            "unit": "瓶",
                            "md": 1,
                            "name": "乐堡",
                            "money": 12000,
                            "update_time": "2017-05-16 14:34:41",
                            "pack_id": 0
                        },
                        {
                            "product_id": 173,
                            "price": 1000,
                            "count": 12,
                            "unit": "瓶",
                            "md": 1,
                            "name": "青岛",
                            "money": 12000,
                            "update_time": "2017-05-16 14:34:41",
                            "pack_id": 0
                        }
                    ],
                    "money": 48100,
                    "service_type": 2
                },
                {
                    "describe": "123",
                    "real_money": 50000,
                    "pay_md": 1,
                    "rate": 100,
                    "bill_no": "DD2017051715055218",
                    "pay_state": 1,
                    "update_time": "2017-05-17 15:05:52",
                    "list": [
                        {
                            "product_id": 176,
                            "price": 500,
                            "count": 12,
                            "unit": "瓶",
                            "md": 1,
                            "name": "燕京",
                            "money": 6000,
                            "update_time": "2017-05-17 15:05:52",
                            "pack_id": 0
                        },
                        {
                            "product_id": 0,
                            "price": 2200,
                            "count": 10,
                            "unit": "",
                            "md": 2,
                            "name": "22",
                            "money": 22000,
                            "update_time": "2017-05-17 15:05:52",
                            "pack_id": 177,
                            "list":[
                                {
                                    "product_id": 171,
                                    "price": 1000,
                                    "count": 12,
                                    "unit": "瓶",
                                    "md": 1,
                                    "name": "乐堡",
                                    "money": 12000,
                                    "update_time": "2017-05-16 14:34:41",
                                    "pack_id": 0
                                },
                                {
                                    "product_id": 172,
                                    "price": 800,
                                    "count": 13,
                                    "unit": "瓶",
                                    "md": 1,
                                    "name": "雪花",
                                    "money": 10400,
                                    "update_time": "2017-05-16 14:34:41",
                                    "pack_id": 0
                                },
                                {
                                    "product_id": 173,
                                    "price": 1000,
                                    "count": 12,
                                    "unit": "瓶",
                                    "md": 1,
                                    "name": "青岛",
                                    "money": 12000,
                                    "update_time": "2017-05-16 14:34:41",
                                    "pack_id": 0
                                }
                            ]
                        }
                    ],
                    "money": 43000,
                    "service_type": 2
                }
            ]
        }
    }
}
