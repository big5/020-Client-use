/**
 * Created by lixinfeng on 2017/4/24.
 */

import React from 'react'

export default class DetailCard extends React.Component {
    constructor(props) {
        super(props)
        this.state={

        }
    }

    render(){
        return(
            <div className="detail-card">
                <div className="detail-card-header">
                    <div className="detail-card-container">
                        <p className="detail-card-title">{this.props.title}</p>
                    </div>
                </div>
                <div className="detail-card-body">
                    <div className="detail-card-container">
                        {this.props.children}
                    </div>
                </div>
                <div className="detail-card-footer">
                    {this.props.footer}
                </div>
            </div>
        )
    }
}
