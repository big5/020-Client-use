/**
 * Created by lixinfeng on 2017/4/14.
 */

import React from 'react'
import LIBrary from '../lib/util'
import {REQUEST_URLS} from './setting'
import Title from '../room_setup/title'
import RoomCollect from './roomCollect'
import RoomMsg from './roomMsg'

export default class RoomMessage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            list: [],
            currentCell: 0,
            num:0
        }
    }

    componentDidMount() {
        this.loadList()
        // let self = this
        // if (!this.timer){
        //     this.timer = setInterval(()=>self.loadList(),50000)
        // }
    }

    componentWillUnmount() {
        this.timer && clearInterval(this.timer)
        this.timer = null
    }

    shouldComponentUpdate(nextProps, nextState) {
        if(this.state.currentCell== nextState.currentCell && this.state.num == nextState.num) {
            return false
        } else {
            return true
        }
    }

    loadList() {
        LIBrary.fetch(REQUEST_URLS.roomIp, {
            method: 'GET'
        }, (res) => {
            this.setState({
                list: res.list,
                num:this.state.num +1
            })
            console.log("roomMessage list is :"+JSON.stringify(res.list))
        })
    }

    updateItem(idx,rmType) {
        let list = this.state.list
        if (list[idx].room_type != rmType) {
            list[idx].room_type = rmType
            this.setState({
                list: list,
                num:this.state.num +1
            })
        }
    }

    changeIndex(idx) {
        this.setState({
            currentCell:idx
        })
    }

    render() {
        const data = this.state
        return (
            <div className="roomState roomController">
                <Title/>
                <div className='downContainer'>
                    <RoomCollect changeIndex={idx=>this.changeIndex(idx)}
                                 list={data.list}
                                 num={data.num}/>
                    <RoomMsg currentCell={data.currentCell}
                             list={data.list}
                             num={data.num}
                             updateItem={(idx,rmType)=>this.updateItem(idx,rmType)}/>
                </div>
            </div>
        )
    }
}
