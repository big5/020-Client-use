/**
 * Created by lixinfeng on 2017/5/22.
 */
const CHECK = {
    "data": {
        "printFrequency": 1,             //打印次数
        "titleType": "小票类型",           //小票类型
        "flowingBill": "流水号",           //流水号
        "orderNumber": "订单号",           //订单号
        "originalBill": "原流水号",        //原流水号
        "netReceivables": "网络订单号",     // 网络订单号
        "roomName": "房台号",               //房台号
        "OperationType": "操作类型",        //操作类型
        "openTime": "2017-5-4 14:35:34",    //开台时间
        "backTime": "2017-5-4 14:36:20",    // 退单时间
        "checkoutTime": "2017-5-4 14:35:37",//结账时间
        "roomPackage": "王子公主嗨起来",       //房间套餐
        "minConsumption": "1000.00",         //最低消费
        "room": [
            {
                "timeInterval": "10:00-12:00",//时段
                "timeLength": "120",        //时长
                "timeUnitPrice": "10",      //时间单价
                "timeSubtotal": "600",      //时段小计
            },
            {
                "timeInterval": "12:00-13:00",
                "timeLength": "60",
                "timeUnitPrice": "10",
                "timeSubtotal": "600",
            }
        ],
        "roomTotalPay": "1260",     //房间总计
        "beer": [
            {
                "beerName": "哈尔滨冰纯",     //酒水
                "beerUnitPrice": "120",     //酒水单价
                "beerCount": "10",          //酒水数量
                "beerSubtotal": "1200",     //酒水小计
            },
            {
                "beerName": "勇闯天涯",
                "beerUnitPrice": "60",
                "beerCount": "10",
                "beerSubtotal": "600",
            }
        ],
        "beerTotalPay": "1260", //酒水总计
        "totalPay": "1260",     //总计
        "hasPayMoney": "500",   //已付
        "advancePayment": "500",//预付
        "discount": "200",      //优惠
        "percent": "10",        //打折
        "rounding": "60",       //抹零
        "shouldIncome": "500",  //应收
        "shouldRefund": "150",  //应退
        "realIncome": "1000",   //实收
        "refund": "0",          //找零
        "payType": 3,           //支付方式
        "payTypeMoney": "500",  //支付方式金额
        "isPayOrRefund": 0,     // 0、不显示  1、支付方式 ; 2、退款方式
        "typePayOrRefund": 3,    //方式  0、不显示 / 1、微信 / 2、支付宝 / 3、POS  / 4、会员卡  / 5、现金
        "reMarks": "小费",        //备注。
        "autograph": 0,        //签名  。  0、不需要 。  1、需要
    }
}
export default CHECK
