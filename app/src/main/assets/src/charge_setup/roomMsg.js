/**
 * Created by lixinfeng on 2017/5/2.
 */
import React from 'react'
import LIBrary from '../lib/util'
import ROOMCOM from './roomCommon'
import RoomCard from './roomCard'
import RoomItems from './roomItems'

export default class RoomMsg extends React.Component {
    constructor(props) {
        super(props)
        this.state={
            list:props.list||[],
            num:props.num || 0,
            currentCell:props.currentCell||0
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.list != this.state.list ||
            this.state.currentCell != nextProps.currentCell ||
            this.state.num != nextProps.num) {
            this.setState({
                list: nextProps.list,
                num: nextProps.num,
                currentCell: nextProps.currentCell
            })
        }
    }
    
    updateItem(idx,rmType) {
        this.props.updateItem(idx,rmType)
    }

    getMsgList() {
        const data = this.state
        if(!this.state.list.length){return []}
        let rtType = data.list[data.currentCell]['room_type']
        let msg
        if (rtType == ROOMCOM.RSTATE_FREE
            || rtType == ROOMCOM.RSTATE_CLEAN
            || rtType == ROOMCOM.RSTATE_DISABLE) {
            msg = [
                data.list[data.currentCell]['name']||'' + data.list[data.currentCell]['rt_name']||'',
                ROOMCOM[ROOMCOM.RDESCRIBE + data.list[data.currentCell]['room_type']]
            ]
        }else if (rtType == ROOMCOM.RSTATE_USING || rtType == ROOMCOM.RSTATE_TIMEOUT){
            msg = [
                data.list[data.currentCell]['name']||''+data.list[data.currentCell]['rt_name']||'',
                ROOMCOM[ROOMCOM.RDESCRIBE+data.list[data.currentCell]['room_type']],
                data.list[data.currentCell]['order_no'],
                ROOMCOM.RPAYTYPES[data.list[data.currentCell]['pay_type']-1],
                data.list[data.currentCell]['st'],
                data.list[data.currentCell]['ed'],
                data.list[data.currentCell]['minute']
            ]
        }
        return msg
    }

    getObjList(data) {
        const objList = []
        for (let obj of data.list) {
            objList.push({
                name:obj['name']||'',
                rtName:obj['rt_name']||'',
                orderNo:obj['order_no']||'',
                payType:obj['pay_type']||'',
                rtId:obj['rt_id']||'',
                rmId:obj['room_id']||''
            })
        }
        return objList
    }

    //获取当前包房
    getCurrentObj() {
        const data = this.state
        if(!data.list.length){return {list:[],rmId:'',rtName:''}}
        const list = ROOMCOM[ROOMCOM.RTYPE+data.list[data.currentCell]["room_type"]]
        return {
            list: list,
            cur:data.currentCell,
            dataList:this.getObjList(data)
        }
    }

    templateTableCol() {
        const cols = []
        const colArr = [1,2]
        LIBrary.times(colArr.length, (index) => {
            cols.push(<col key={index} className={ROOMCOM.RTYPE+"-"+index}/>)
        })
        return cols
    }

    templateTableRow() {
        const rows = []
        if(!this.state.list.length) {return rows}
        let msg = this.getMsgList()
        const msgList = ROOMCOM[ROOMCOM.RMESSAGE + this.state.list[this.state.currentCell]["room_type"]]
        LIBrary.times(msgList.length, (index) => {
            rows.push(<tr key={index}>
                <td>{msgList[index]}</td>
                <td>{msg[index]}</td>
            </tr>)
        })
        return rows
    }

    templateRoomTable() {
        return(
            <div className="roomTable">
                <table>
                    <colgroup>
                        {this.templateTableCol()}
                    </colgroup>
                    <tbody>
                    {this.templateTableRow()}
                    </tbody>
                </table>
            </div>
        )
    }

    render() {
        return(
            <div className="roomMsg rightCon">
                <RoomCard title={<h3 className='rightTitle'>包房信息</h3>} children={this.templateRoomTable()}/>
                <RoomCard title={<h3 className='rightTitle'>包房操作</h3>} >
                    <RoomItems data={this.getCurrentObj()}  updateItem={(idx,rmType)=>this.updateItem(idx,rmType)}/>
                </RoomCard>
            </div>
        )
    }
}
