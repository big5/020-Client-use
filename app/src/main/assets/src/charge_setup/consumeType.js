/**
 * Created by lixinfeng on 2017/5/3.
 */

import React from 'react'
import LIBrary from '../lib/util'
import { REQUEST_URLS } from './setting'
import ROOMCOM from './roomCommon'
import COMMON from  '../lib/common'
import moment from  'moment'
import RoomCard from './roomCard'
import PatternBar from './patternBar'
import { hashHistory } from 'react-router'
import PRINRNOTES from '../lib/printNote'


export default class ConsumeType extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            roomMsg: props.data.roomMsg || {},
            sell:0,
            totalMoney:'',
            products:[]
        }
    }

    componentDidMount() {
        LIBrary.fetch(REQUEST_URLS.roomPack+'?rt_id='+this.state.roomMsg.rtId+'&is_valid=1', {
            method: 'GET'
        }, (res) => {
            this.setState({
                products:res.list
            })
        })
    }

    componentWillReceiveProps(nextProps) {
        // console.log("ConsumeType nextProps data is :"+JSON.stringify(nextProps.data))
        if(nextProps.data!=this.props.data) {
            this.setState({data:nextProps.data})
        }
    }

    shouldComponentUpdate(nextProps,nextState) {
        console.log("nextState money is :"+nextState.totalMoney)
        if (nextState.totalMoney!=this.state.totalMoney) {
            return false
        }
        if (nextState.tableObj!=this.state.tableObj) {
            return false
        }
        return true
    }

    //选择对应套餐---
    selectedPro(n){//是否需要重新定st时间呢？或是先判断st格式是否正确 然后再定
        console.log("selectedPro data products is :"+n)
        const data = this.state
        if (!data.products.length){return}
        this.changeInterval('norcrement',data.products[n].hour*60)
        let st = Zepto('div.consumeType').find("input[name='startTime']").val()
        LIBrary.fetch(REQUEST_URLS.calcPack, {
            method: 'POST',
            body:JSON.stringify({
                "rt_id":data.roomMsg.rtId,
                "room_id":data.roomMsg.rmId,
                "st":st,
                "pack_id":data.products[n].pack_id
            })
        }, (res) => {
            let resObj = res.detail
            LIBrary.fetch(REQUEST_URLS.packPro+"?pack_id="+res.detail.pack_id, {
                method: 'GET'
            }, (res) => {
                resObj['list'] = res.detail.list
                this.setTotalMoney(resObj)
                this.props.getTableObj(resObj)
            })
        })
    }
    //改
    setTotalMoney(obj) {
        if(obj.minute) {
            let  money = 0
            for (let i in obj.list) {
                money += obj.list[i].money
            }
            this.setState({
                tableObj:obj,
                totalMoney:parseFloat(money/100).toFixed(2)
            })
        } else {
            this.setState({
                tableObj:obj,
                totalMoney:obj.price?parseFloat(obj.price/100).toFixed(2):''
            })
        }
    }

    changeInterval(type,val) {
        let interval = Number(Zepto('div.consumeType').find("input[name='timeLength']").val()) || ROOMCOM.RPERIOD_INTERVAL
        switch (type) {
            case 'oncrement':
                interval += ROOMCOM.RPERIOD_INTERVAL
                break
            case 'decrement':
                interval = interval > ROOMCOM.RPERIOD_INTERVAL? interval -= ROOMCOM.RPERIOD_INTERVAL: ROOMCOM.RPERIOD_INTERVAL
                break
            case 'norcrement':
                interval = Number(val)
                break
            default:break
        }
        let st = moment().format('HH:mm')
        let ed = moment(st,'HH:mm').add(interval,'m').format('HH:mm')
        Zepto('div.consumeType').find("input[name='timeLength']").val(interval)
        Zepto('div.consumeType').find("input[name='startTime']").val(st)
        Zepto('div.consumeType').find("input[name='endTime']").val(ed)
    }

    selectedType(n) {
        console.log("selectedType is " + n)
        if (n == this.state.sell) {return}
        this.setState({sell: n})
        Zepto('div.consumeType').find("input[name='endTime']").val('')
        Zepto('div.consumeType').find("input[name='timeLength']").val(ROOMCOM.RPERIOD_INTERVAL)
    }
    //选择时段
    selectedPeriod(n) {
        this.changeInterval('norcrement',ROOMCOM.RPERIOD_DET[n])
        const data = this.state
        let s = Zepto('div.consumeType').find("input[name='startTime']").val()
        let t = Zepto('div.consumeType').find("input[name='endTime']").val()
        if(COMMON.HMTIME.test(t) && COMMON.HMTIME.test(s)) {
            LIBrary.fetch(REQUEST_URLS.calcTime, {
                method: 'POST',
                body:JSON.stringify({
                    "rt_id":data.roomMsg.rtId,
                    "room_id":data.roomMsg.rmId,
                    "st":s,
                    "ed":t
                })
            }, (res) => {
                const obj = {rtName: res.rt_name,roomName: res.room_name,name:res.name,list:res.list,minute:ROOMCOM.RPERIOD_DET[n]}
                this.setTotalMoney(obj)
                this.props.getTableObj(obj)
            })
        }

    }

    getProductsName() {
        const arr = []
        if (this.state.products.length) {
            for (let obj of this.state.products) {
                arr.push(obj.name)
            }
        }
        return arr
    }

    goLast() {
        hashHistory.go(-1)
    }

    goNext() {
        const data = this.state
        let s = Zepto('div.consumeType').find("input[name='startTime']").val()
        let t = Zepto('div.consumeType').find("input[name='endTime']").val()
        console.log("goNext tableObj is "+JSON.stringify(data.tableObj))
        if(COMMON.HMTIME.test(t) && COMMON.HMTIME.test(s)) {
            if(!data.tableObj) {return}
            let msg = {
                "roomMsg": data.roomMsg,
                "st": s,
                "ed": t,
                "sellType": ROOMCOM.RSELLTYPES[data.sell],
                "money": data.totalMoney,
                "settle":1,
                "obj": data.tableObj
            }
            hashHistory.push({
                pathname: 'roomSettle',
                state: {
                    msg: msg
                }
            })
        }
    }

    print() {
    }

    templateRoomSettle() {
        const data = this.state
        const roomTitle = data.roomMsg.name||'' + data.roomMsg.rtName||''
        const list = []
        for (let o of ROOMCOM.RPAYMDS) {
            list.push(o)
        }
        return (
            <div className="consumeType">
                <div>
                    <label htmlFor="">包厢：</label>
                    <p className='boxContain'>{roomTitle}</p>
                </div>
                <div>
                    <label htmlFor="">包厢状态：</label>
                    <PatternBar data={ROOMCOM.RSELLTYPES}  cur={0} selected={n => this.selectedType(n)}/>
                </div>
                <div>
                    <label htmlFor="">{data.sell == 0 ? '选择时长：':'包厢套餐：'}</label>
                    <div className='selectTimeLength'>
                        <div className={'product '+(data.sell? '':'hide')}>
                            <PatternBar data={this.getProductsName()}
                                        isAllow={!data.sell}
                                        selected ={n => this.selectedPro(n)}/>
                        </div>
                        <div className={'calcTime '+ (data.sell ? 'hide':'')}>
                            <PatternBar data={ROOMCOM.RPERIOD}
                                        isAllow={data.sell}
                                        selected={n => this.selectedPeriod(n)}/>
                            <div>
                                <label htmlFor="">时长:</label>
                                <button data-type="decrement" onClick={()=>this.changeInterval('decrement')}>-</button>
                                <input type="text" name='timeLength' disabled="disabled"
                                       defaultValue={ROOMCOM.RPERIOD_INTERVAL} />
                                <span>分钟</span>
                                <button data-type="oncrement" onClick={()=>this.changeInterval('oncrement')}>+</button>
                            </div>
                        </div>
                        <div className='openRoomTime'>
                            <p><span>开台时间</span><br/>
                                <input type="text" name="startTime"
                                       defaultValue={moment().format('HH:mm')}
                                       disabled="disabled"/>
                            </p>
                            <p className='startToEndTime'>至</p>
                            <p><span>关台时间</span><br/>
                                <input type="text" name="endTime"
                                       defaultValue={``}
                                       disabled="disabled"/>
                            </p>
                        </div>
                    </div>
                </div>
                <button onClick={()=>this.goLast()} className='checkOutBtn checkOutBtnBack'>返回上一级</button>
                <button onClick={()=>this.print()} className='checkOutBtn checkOutBtnThreaten'>打单F3</button>
                <button onClick={()=>this.goNext()} className='checkOutBtn checkOutBtnComfirm'>结账F1</button>
            </div>
        )
    }

    render() {
        return(
            <div className="rightCon">
                <RoomCard title={<h3>结账</h3>} children={this.templateRoomSettle()}/>
            </div>
        )
    }
}
