/**
 * Created by lixinfeng on 2017/5/8.
 */
import React from 'react'
import LIBrary from '../lib/util'
import { REQUEST_URLS } from './setting'
import Title from '../room_setup/title'
import RoomTableMsg from './roomTableMsg'
import SettleDetail from './settleDetail'

export default class RoomSettle extends React.Component {
    constructor(props) {
        super(props)
        this.state={
            msg:props.location.state.msg || {},
            roomMsg: props.location.state.roomMsg || {},
            tableObj: {},
            wineObj: {}
        }
        console.log("RoomSettle msg is :"+JSON.stringify(this.state.msg),"RoomSettle roomMsg is :"+JSON.stringify(this.state.roomMsg))
    }

    componentDidMount() {
        const data = this.state
        if(!data.roomMsg || !data.roomMsg.orderNo) { return }
        LIBrary.fetch(REQUEST_URLS.orderDetail+"?order_no="+data.roomMsg.orderNo,{
            method: 'GET'
        }, (res) => {
            let wine = []
            let cost = []
            if (res.detail.bills.length) {
                res.detail.bills.map((obj)=>{
                    let s = obj.bill_no.substr(0,2)
                    let len = 0
                    obj.list.map((ob)=>{
                        (ob.pack_id && ob.list && ob.list.length) ? ob.expend = 'expend':''
                        len += 1
                    })
                    obj.len = len
                    switch (s) {
                        case 'KT':
                            cost.push(obj)
                            break
                        case 'DD':
                            wine.push(obj)
                            break
                        case 'XS':
                            cost.push(obj)
                            break
                        default:break
                    }
                })
            }
            this.setState({
                res : res,
                tableObj: cost,
                st:res.detail.st,
                ed:res.detail.ed,
                minute:res.detail.minute,
                order_no:res.detail.order_no,
                payState:res.detail.state,
                payType:res.detail.pay_type,
                wineObj: wine
            })
        })
    }

    goBack() {
        history.go(-1)
    }

    render() {
        const data = this.state
        let foo = data.roomMsg && data.roomMsg.orderNo
        console.log("data msg obj is :"+JSON.stringify(data.msg))
        return (
            <div className="roomSettle roomController openRoom">
                <Title/>
                <div className='openRoomDown'>
                    <RoomTableMsg data={{
                        roomMsg:foo ? data.roomMsg : data.msg.roomMsg,
                        tableObj:foo ? data.tableObj : data.msg.obj,
                        wineObj:data.wineObj,
                        st:data.st,
                        ed:data.ed,
                        minute:data.minute,
                        order_no:data.order_no,
                        payState:data.payState,
                        payType:data.payType || (foo ? data.roomMsg.payType : '')
                    }} />
                    <SettleDetail data={{
                        "res":foo ? data.res : '',
                        "tableObj":data.msg.obj,
                        "rtId":foo ? data.roomMsg.rtId : data.msg.roomMsg.rtId,
                        "rmId":foo ? data.roomMsg.rmId : data.msg.roomMsg.rmId,
                        "rtName":foo ? data.roomMsg.rtName : data.msg.roomMsg.rtName,
                        "st":foo ? data.st : data.msg.st,
                        "ed":foo ? data.ed : data.msg.ed,
                        "sellType":data.msg.sellType,
                        "packId":data.msg.obj ? data.msg.obj.pack_id||'':'',
                        "money":data.msg.money
                        }} back={()=>this.goBack()}
                    />
                </div>
            </div>
        )
    }
}
