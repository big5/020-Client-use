const REQUEST_URLS = {
    roomType: '/room/type',
    roomIp:'/room/ip',
    calcTime:'/calc/time',
    cates: '/cate',
    products: '/cate/product',
    roomPack: '/room/pack',
    calcPack:'/calc/pack',
    packPro:'/pack/product',
    check: '/room/time/checkout ',
    openTime:'/open/time',
    openPack:'/open/pack',
    pack: '/pack',
    orderProduct: '/order/product',
    orderDetail:'/order/detail',
    orderClose:'/order/close',
    storeAccount: '/store/account'
}

export { REQUEST_URLS }
