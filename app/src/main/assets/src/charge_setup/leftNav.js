import React from 'react'
import {REQUEST_URLS} from './setting'
import LIBrary from '../lib/util'

export default class leftNav extends React.Component {
    constructor(props) {
        super(props)
        this.state={
            cateId: props.cateId,
            cates: []
        }
    }

    componentWillReceiveProps(props){
        this.setState({
            cateId: props.cateId
        })
    }

    componentWillMount() {
        this.getCates()
    }

    getCates() {
        LIBrary.fetch(REQUEST_URLS.cates, {
            method: 'GET'
        }, (res) => {
            this.setState({
                cates: res.list
            })
        })
    }

    chooseCates(idx){
        if(idx == -1){
            this.props.chooseCates(-1)
            return
        }
        let cateId = this.state.cates[idx]['cate_id']
        this.props.chooseCates(cateId)
    }

    templateCates(params) {
        let items = []
        LIBrary.times( params.length, (idx) => {
            items.push(
                <li key={idx}
                    className={(this.state.cateId == params[idx]['cate_id'] ? 'active' : '')}
                    onClick={() => this.chooseCates(idx)}>{ params[idx]['name'] }</li>
            )
        })
        return items
    }

    render() {
        const data = this.state
        return(
            <div className='roomTypeSetupContain'>
                <ul className='roomTypeSetup'>
                    { this.templateCates(data.cates) }
                    <li className={(this.state.cateId <= 0 ? 'active' : '')}
                        onClick={() => this.chooseCates(-1)}> 套餐 </li>
                </ul>
            </div>
        )
    }
}
