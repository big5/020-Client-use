import React from 'react'
import {REQUEST_URLS} from './setting'
import LIBrary from '../lib/util'
import ItemsCell from './itemCell'

export default class BuyItems extends React.Component {
    constructor(props) {
        super(props)
        this.state={
            pdtIds: [],
            productsNum: [],
            products: [],
        }
    }

    componentDidMount(){
        LIBrary.eventproxy.on('resetBuyItems',(data) => {
            this.setState({
                product: '',
                pdtIds: [],
                productsNum: [],
                products: [],
            })
        })
    }

    componentWillReceiveProps(props){
        if(typeof props.product != 'object' || props.cateId == -1 ||
            !props.product['product_id'] ||
            this.state.pdtIds.indexOf(props.product['product_id']) >= 0 ||
            this.state.product == props.product){
            return
        }
        this.getProduct(props.product)
    }

    putNum(num, idx, isPack){
        let productsNum = this.state.productsNum
        productsNum[idx] = num
        this.setState({
            productsNum: productsNum
        }, () => {
            this.totalMoney()
        })
    }

    totalMoney() {
        let totalMoney = 0
        this.state.productsNum.map((value, index) => {
            totalMoney += value * this.state.products[index]['price']
        })
        this.props.totalproductMoney(totalMoney)
    }

    deleteProudct(index) {
        let products = LIBrary.removeByIndex(this.state.products, index)
        let pdtIds = LIBrary.removeByIndex(this.state.pdtIds, index)
        let productsNum = LIBrary.removeByIndex(this.state.productsNum, index)

        this.setState({
            products: products,
            pdtIds: pdtIds,
            productsNum: productsNum
        }, () => {
            this.totalMoney()
        })
    }

    getProduct(product){
        let products = this.state.products
        let pdtIds = this.state.pdtIds
        let productsNum = this.state.productsNum
        products.push(product)
        pdtIds.push(product['product_id'])
        productsNum.push(1)
        this.setState({
            product: product,
            products: products,
            pdtIds: pdtIds,
            itemsNum: productsNum
        }, () => {
            this.totalMoney()
        })
    }

    templateProducts(params) {
        let items = []
        LIBrary.times(params.length, (idx) =>{
            let item = params[idx]
            items.push(
                <ItemsCell
                    key={idx}
                    product={ item }
                    putNum={ (count) => this.putNum(count, idx) }
                    deleteProudct={ () => this.deleteProudct(idx) }/>
            )
        })
        return items
    }

    render() {
        return(
            <ul className='productContainer'>
                { this.templateProducts(this.state.products) }
            </ul>
        )
    }
}
