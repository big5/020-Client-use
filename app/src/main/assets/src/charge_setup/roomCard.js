/**
 * Created by lixinfeng on 2017/4/22.
 */


import React from 'react'

export default class RoomCard extends React.Component {
    constructor(props) {
        super(props)
        this.state={
        }
    }
    render(){
        return(
            <div className="roomCard">
                <div className="roomCard-header roomCard-container">
                    <div className="roomCard-header-title">
                        {this.props.title}
                    </div>
                </div>
                <div className="roomCard-body  roomCard-container">
                    {this.props.children}
                </div>
            </div>
        )
    }
}
