
import React from 'react'
import ROOMMON from './roomCommon'
import { REQUEST_URLS } from './setting'
import LIBrary from '../lib/util'

export default class Products extends React.Component {
    constructor(props) {
        super(props)
        this.state={
            cateId: 0,
            products: []
        }
    }

    componentWillReceiveProps(props){
        if(this.state.cateId == props.cateId ){
            return
        }
        this.getProducts(props.cateId)
    }

    getProducts(cateId){
        if(cateId == -1){
            LIBrary.fetch(REQUEST_URLS.pack,{
                method: 'GET'
            },(res) => {
                this.setState({
                    cateId: cateId,
                    products: res.list
                })
            })
            return
        }
        LIBrary.fetch(REQUEST_URLS.products+'?cate_id='+cateId,{
            method: 'GET'
        },(res) => {
            this.setState({
                cateId: cateId,
                products: res.list
            })
        })
    }

    choose(idx){
        let product = this.state.products[idx]
        this.props.chooseProducts(product)
    }

    templateProducts() {
        let items = []
        LIBrary.times(this.state.products.length, (idx) => {
            let item = this.state.products[idx]
            items.push(
                <li key={idx} onClick={(e) => this.choose(idx)}>
                    <span className='commodityDetail'>{item['name']} </span>
                    <div className='commodityDetailPrice'>
                        <span className='detailPriceCount'>{item['discount'] ? '参与打折' : '不参与打折'}</span>
                        <span className='detailPrice'>{(item['price']/100).toFixed(2)} </span>
                    </div>
                </li>
            )
        })
        return items
    }

    render() {
        const data = this.state
        return(
            <div className='commodity'>
                <ul>
                    {this.templateProducts()}
                </ul>
            </div>
        )
    }
}
