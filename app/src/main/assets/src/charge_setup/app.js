require('./app.scss')
require('../room_setup/app.scss')
require('../charge_setup/app.scss')

import React from 'react'
import ReactDOM from 'react-dom'
import {Router, Route, hashHistory, Link, IndexRedirect} from 'react-router'
import RoomMessage from './roomMessage'
import OpenRoom from './openRoom'
import FoodOrder from './foodOrder'
import RoomSettle from './roomSettle'
import BackOrder from './backOrder'
import ConsumerDetail from './consumerDetail'
import checkOrder from './checkOrder'
ReactDOM.render((
    <Router history={ hashHistory }>
        <Route path='/'>
            <IndexRedirect to='/roomMessage'/>
        </Route>
        <Route path='/roomMessage' component={RoomMessage}/>
        <Route path='/openRoom' component={OpenRoom}/>
        <Route path='/foodOrder' component={FoodOrder}/>
        <Route path='/roomSettle' component={RoomSettle}/>
        <Route path='/backOrder' component={ BackOrder } />
        <Route path='/consumerDetail' component={ ConsumerDetail } />
        <Route path='/checkOrder' component={ checkOrder } />
    </Router>
), document.getElementById('body'))
