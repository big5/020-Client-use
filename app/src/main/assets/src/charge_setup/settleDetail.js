/**
 * Created by lixinfeng on 2017/5/8.
 */

import React from 'react'
import Zepto from 'npm-zepto'
import moment from 'moment'
import LIBrary from '../lib/util'
import ROOMCOM from './roomCommon'
import {PRINRNOTES} from '../lib/printNote'
import PatternBar from './patternBar'
import { REQUEST_URLS } from './setting'
import { hashHistory } from 'react-router'
import PAY from '../lib/payMothed'
import COMMON from '../lib/common'

export default class SettleDetail extends React.Component {
    constructor(props) {
        super(props)
        this.getRealMoney=this.getRealMoney.bind(this)
        this.getRechange=this.getRechange.bind(this)
        this.state={
            tableObj:props.data.tableObj,
            rate:'100',
            cur:0,
            money:props.data.money,
            realMoney: 0,
            payType:1,
            OperationType: 'KT',
            bill_no: '',
            checkout: true,
            order_no: props.order_no || '',
            bill_no: ''
        }
    }

    shouldComponentUpdate(nextProps,nextState) {
        if(this.state.rate!=nextState.rate || this.state.cur != nextState.cur){
            return true
        }return false
    }

    count() {
        const rate = Zepto('input[name=rate]').val()
        this.setState({
            rate:rate
        })
    }

    selectedType(n) {
        if (n == this.state.cur){return}
        this.setState({
            cur:n,
            payType:n+1
        })
    }

    delayPrintNote() {
        PRINRNOTES.AFSingleBill(this.state.orderNo, this.state.billNo)
        window.location.href = './index.html#/roomMessage'
    }

    submit(e) {
        e.preventDefault()
        const method = e.target.name
        if (method == 'back') {
            return this.props.back()
        }
        const form = Zepto('form.consumeType')
        let values = {}
        values['rt_id'] = this.props.data.rtId
        values['room_id'] = this.props.data.rmId
        values.st = this.props.data.st
        values['pay_type'] = Number(this.state.payType)

        //支付状态  现结1  落单后结2
        if (this.state.payType == 1) {
            let idx = parseInt(ROOMCOM.RPAYMDS.indexOf(form.find('select').val())) + 1
            values['pay_md'] = idx ? idx :  1
            form.find('input').map((index, input) => {
                if (values['pay_md'] == 1 && !input.value) {
                    return LIBrary.warnning('请填写完整信息')
                }
                values[input.name] = Number(input.value)
            })
            values['pay_md'] = idx ? idx :  1
            values['real_money'] *= 100
        }
        if (!form.find('textarea').val()) {
            return LIBrary.warnning('请填写备注信息')
        }

        if (this.props.data.sellType==ROOMCOM.RSELLTYPES[0]){
            values.ed = this.props.data.ed
        }else{
            values['pack_id'] = this.props.data.packId
        }
        values['money'] = this.props.data.money
        values['describe'] = form.find('textarea').val()
        console.log("values is :"+JSON.stringify(values))
        switch (method) {
            case 'confirm':
                //确认结账
                this.settle(values, this.commfirPay.bind(this))
                break;
            case  'print':
                // 结账打印
                this.print(values)
                break
            default:break
        }
    }

    settle(obj, callback) {
        const url = this.props.data.sellType ==
        ROOMCOM.RSELLTYPES[0]?REQUEST_URLS.openTime:REQUEST_URLS.openPack
        LIBrary.fetch(url, {
            method: 'POST',
            body:JSON.stringify(obj)
        }, (res) => {
            obj.order_id = res.order_id
            this.setState({
                orderNo: res.detail.order_no,
                billNo: res.detail.bill_no
            }, () => {
                obj['pay_type'] != 1 || (obj['pay_md'] != 2 && obj['pay_md'] != 3 && obj['pay_md'] != 4) ?
                this.delayPrintNote(): ''
                callback(obj)
                print(obj)
            })
        })
    }

    print(vals) {
        if(this.state.checkout){
            PRINRNOTES.AllBill(this.state.order_no, '')
            return
        }
        console.log('print is :',(vals))
        let obj = this.state.tableObj
        if(obj) {
            let dataList = []
            let curTime = moment().format('YYYY-MM-DD HH:mm:ss')
            let param =  {
                "detail":{
                    "describe": vals.describe,
                    "st": vals.st,
                    "ed": vals.ed,
                    "prepay": "",
                    "state": 0,
                    "room_id": vals.room_id,
                    "minute": obj.minute,
                    "room_name": vals.rtName,
                    "pay_type": vals.pay_type,
                    "update_time": curTime,
                    "order_no": "",
                    "bills": [{
                        "describe": vals.describe,
                        "money": vals.money,
                        "pay_md": vals.pay_md,
                        "update_time": curTime,
                        "real_money": vals.real_money,
                        "service_type": 1,
                        "bill_no": "",
                        "pay_state": 1,
                        "rate": vals.rate,
                        "list": [
                        ]
                    }]
                }
            }

            !obj.pack_id ?
            obj.list.map((ob, index) => {
                obj.list[index].pack_id = 0
                dataList.push ({"update_time": curTime,
                                "fee_id": ob.fee_id,
                                "st": ob.st,
                                "md": "",
                                "money": ob.money,
                                "ed": ob.ed,
                                "pack_id": 0,
                                "bill_no": this.state.OperationType+parseInt(1000000*Math.random()),
                                "minute": ob.minute,
                                "list":obj.list
                            })
            }) :
            dataList.push({
                "st": obj.st,
                "money": obj.money,
                "ed": obj.ed,
                "pack_id": obj.pack_id,
                "minute": obj.minute,
                "bill_no": this.state.OperationType+parseInt(1000000*Math.random()),
                "list":obj.list
            })
            param.detail.bills = dataList
            PRINRNOTES.BFSingleBill(param.detail,'')
        }
    }

    getDiscount() {
        return parseFloat(this.props.data.money) - parseFloat(this.state.realMoney)
    }

    getRealMoney() {
        const realMoney = parseFloat(this.state.rate/100*this.props.data.money).toFixed(2)
        this.setState({
            realMoney:realMoney
        })
        return realMoney
    }

    getRechange(e) {
        const money = Zepto(e.target).val()
        let check = (money - this.state.money).toFixed(2) > 0 ? (money - this.state.money).toFixed(2) : 0
        Zepto('.settleDetail  .payNow').find("input[name='check']").val(check)
    }

    commfirPay(values) {
        switch (parseInt(values['pay_md'])){
            case 1:
                JSBridge.test('openBox')
            break;

            case 2:
                this.startGetCode(values, 'wx')
                Zepto('input#paycode').attr('type', 'text')
            break;

            case 3:
                this.startGetCode(values, 'ali')
                Zepto('input#paycode').attr('type', 'text')
            break

            case 4:
            break;

            case 5:
            break
        }
    }

    startGetCode(value, type) {
        let timer = ''
        let code = ''
        Zepto('input#paycode').show()
        Zepto('input#paycode').trigger('focus')
        setTimeout(() => {
            code = Zepto('input#paycode').val()
            Zepto('input#paycode').val('')
            if(code && code.length > 10){
                this.startPay(value, code, type)
                Zepto('input#paycode').hide()
                Zepto('input#paycode').attr('type','text')
                LIBrary.warnning('扫码成功')
                return
            }
            this.startGetCode(value, type)
        }, 1000)
    }

    startPay(value, code, type) {
        let parma = PAY.PAYPARMA
        parma.paytype = type
        parma.ktvid = Zepto('input#storeId').attr('value')
        parma.date = COMMON.TIME.toString()
        parma.paraTotalFee = value['money']
        parma.erpid = this.state.orderNo
        parma.time = COMMON.TIME.toString()
        parma.data.ktv_id = Zepto('input#storeId').attr('value')
        parma.data.paraBody = 'ktv,点单'
        parma.data.paraAuthCode = code
        parma.data.erp_id = this.state.orderNo
        PAY.payMothed(parma, this.delayPrintNote.bind(this))
        LIBrary.countDown()
    }

    templatePayType(params) {
        let items = []
        LIBrary.times(params.length, (idx) => {
            items.push(
                <option key={idx} >{ params[idx] }</option>
            )
        })
        return items
    }

    templatePayContainer(type) {
        return type== 1 ? <div className={this.state.cur?'payNow hide':'payNow'}>
            <p className='payType'><label className='checkPay1 payKind1'>支付方式:</label>
                <select name='pay_md' className='payment'>
                    {this.templatePayType(ROOMCOM.RPAYMDS)}
                </select>
            </p>
            <p className='settleDiscount'><label className='checkPay1 payKind2'>打折: </label><input type='text' name='rate' defaultValue={ this.state.rate } onChange={() => this.count()}/>
                 %<br/><span className='feeDiscount'>*房费已优惠:{()=>this.getDiscount()}</span>
            </p>
            <p className='settleDiscount'><label className='checkPay2 payKind2'>应收: </label><input type='text'  name='money' disabled="disabled" defaultValue={this.getRealMoney()} /></p>
            <p className='settleDiscount'><label className='checkPay1 payKind2'>实收: </label><input type='text'  name='real_money' onChange={(e)=>this.getRechange(e)}/></p>
            <p className='settleDiscount'><label className='checkPay2 payKind2'>找零: </label><input type='text'  name='check' /></p>
        </div> :''
    }

    render() {
        return (
            <div className="settleDetail rightCon">
                <h3>结账</h3>
                <form className='consumeType'>
                    <PatternBar className={this.props.settle?'':'hiden'} data={ROOMCOM.RPAYTYPES} cur={0} selected={n => this.selectedType(n)}/>
                    <div className='editReason'>
                        {this.templatePayContainer(this.state.payType)}
                        <textarea placeholder='备注： 100字内' name='describe' className='remark'></textarea>
                    </div>
                    <button name='back' onClick={(e) => this.submit(e)} className='checkOutBtn checkOutBtnBack'>返回上一级</button>
                    <button name='print' onClick={(e) => this.submit(e)} className='checkOutBtn checkOutBtnThreaten'>打单</button>
                    <button name='confirm' onClick={(e) => this.submit(e)} className='checkOutBtn checkOutBtnComfirm'>确认</button>
                </form>
                <p><label>付款码</label><input id='paycode' type='hidden'/></p>
            </div>
        )
    }
}
