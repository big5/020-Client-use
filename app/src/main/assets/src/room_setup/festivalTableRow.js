/**
 * Created by lixinfeng on 2017/4/12.
 */

import React  from 'react'
import BasicTable from './basicTable'
import Dialog from '../components/dialog'
import moment from 'moment'
import InfiniteCalendar from  'react-infinite-calendar'
import 'react-infinite-calendar/styles.css'
import Zepto from 'npm-zepto'

export default class FestivalTableRow extends React.Component {
    constructor(props) {
        super(props)
        this.getRangeArray=this.getRangeArray.bind(this)
        this.templateDatePicker=this.templateDatePicker.bind(this)
        this.state={
            msg:props.msg,
            holidayList:[],
            day_or_holiday:'',
            isShowDialog:false
        }
    }

    shouldComponentUpdate(nextProps,nextState) {
        return this.state.day_or_holiday=== nextState.day_or_holiday
    }

    componentWillReceiveProps(nextProps){
        if (this.state.msg != nextProps.msg){
            this.setState({
                msg:nextProps.msg
            })
        }
    }

    getRangeArray() {
        const self = this
        new Promise((resolve,reject)=>{
            setTimeout(()=>{
                resolve(Zepto('div.Cal__MonthList__root div.Cal__Month__rows li.Cal__Day__selected').eq(0).attr("data-date"))
            },0)
        }).then((value)=>{
            self.setState({
                day_or_holiday:value
            })
        }).catch((error)=>{
            LIBrary.warnning('时间设置失败')
        })
    }

    templateDatePicker(){
        return (
            <InfiniteCalendar
                width={(window.innerWidth <= 746) ? window.innerWidth : 746}
                height={window.innerHeight - 250}
                className='addRoombox'
                rowHeight={70}
                displayOptions={{
                    layout: 'landscape'
                }}
                minDate={new Date(moment().format('YYYY,MM,DD'))}
                locale={{
                    locale: require('date-fns/locale/zh_cn'),
                    headerFormat: 'YYYY-MM-DD',
                    weekdays: '日_一_二_三_四_五_六'.split('_'),
                    blank: '请选择日期',
                    todayLabel: {
                        long: '今天',
                        short: '今天'
                    }
                }}
                selected={[
                    this.state.day_or_holiday?this.state.day_or_holiday:moment().format('YYYY-MM-DD')
                ]}
                onSelect={this.getRangeArray}
            />
        )
    }

    toggleCalendar(){
        this.setState({
            isShowDialog:!this.state.isShowDialog
        })
        console.log(11111111111+'点击了日期设置'+2222222222)
    }

    render() {
        let data = this.state
        let data_date = (data.msg?data.msg.obj['day_or_holiday']:0) || (data.day_or_holiday?data.day_or_holiday:"设置日期")
        let obj = data.msg.obj.list.length?data.msg.obj:{"day_or_holiday":data.day_or_holiday,"list":[]}
        return (
            <tr className="festivalTableRow">
                <Dialog show={data.isShowDialog} title={''} onHide={
                    (e)=>this.toggleCalendar(e)
                } children={ this.templateDatePicker() } />
                <td>
                    <button onClick={(e)=>this.toggleCalendar(e)}
                            data-date={data_date}>
                        {data_date}
                    </button>
                </td>
                <td colSpan="3">
                    <BasicTable msg={obj.list} nsg={{'day':obj['day_or_holiday'],'rt_id':data.msg.rtId,'md':this.props.param.md}}/>
                </td>
                <td><button onClick={(e)=>this.props.delTableRow(e,data.msg.index)}>删除</button></td>
            </tr>
        )
    }
}

FestivalTableRow.defaultProps={
    param:{
        md:'holiday'
    }
}
