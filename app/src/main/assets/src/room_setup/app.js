require('./app.scss')
import React from 'react'
import ReactDOM from 'react-dom'
import { Router, Route, hashHistory, Link, IndexRedirect } from 'react-router'
import Container from './container'
import Personal from './personal'
import wrongPage from './wrongPage'
ReactDOM.render((
    <Router history={hashHistory}>
        <Route path='/' >
            <IndexRedirect to='room' />
        </Route>
        <Route path='/room' component={ Container }/>
        <Route path='/personal' component={ Personal } />
        <Route path='/wrongPage' component={ wrongPage } />
    </Router>
), document.getElementById('body'))
