import React from 'react'
import LIBrary from '../lib/util'
import { REQUEST_URLS} from './setting'
import Checkbox from '../pdt_setup/checkbox'

export default class AddFoodsCell extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            products: [],
            cates: {},
            packId: ''
        }
    }

    changeChecked(e, index) {
        let product = this.state.products
        product[index]['checked'] = e.target.checked
        this.setState({
            products: product
        })
    }

    changeInput(e, index) {
        let product = this.state.products
        product[index]['count'] = e.target.value
        this.setState({
            products: product
        })
    }

    resetFoods(food) {
        this.props.resetFoods(food)
    }

    componentWillReceiveProps(props) {
        if(this.state.cates == props.cates &&
            this.state.packId == props.packId ){
            return
        }
        if(typeof props.cates == 'object'){this.getCates(props.cates)}
    }

    getCates(params) {
        LIBrary.fetch(REQUEST_URLS.product + '?cate_id=' + params['cate_id'],{
            method: 'GET'
        }, (res) => {
            this.setState({
                cates: params.cates,
                products: res.list,
                packId: params.packId
            })
        })
    }

    addFoods(event){
        event.preventDefault()
        const form = Zepto(event.target)
        let food = []
        let item = {}
        let value = []
        form.find('li').map((index, li) => {
            let checked = Zepto(li).find('input.choosed')[0]
            if(!checked.checked){
                return
            }
            item = this.state.products[index]
            let input = Zepto(li).find('input[type=text]')
            let num = Zepto(li).find('input.num')[0].checked
            item.count = num ? '0' : input.attr('value')
            value.push({count: item.count, product_id: item.product_id})
            food.push(item)
        })
        // 修改套餐，提前发请求
        if(value.length == 0){
            LIBrary.warnning('请选择商品')
            return
        }
        this.resetFoods(food)
    }

    templateFoods(products) {
        let items = []
        LIBrary.times(products.length, (idx) => {
            let checked = products[idx].checked ? true : false
            let count = products[idx]['count'] ? products[idx]['count'] : ''
            items.push(
                <li key={idx} className='drinksProductsAnddrinksOperate'>
                    <div className='drinksProducts'>
                        <span><input type='Checkbox' className='choosed' checked={checked}
                            onChange={(e) => this.changeChecked(e, idx)} /></span>
                        <p className='nameAndSpec'>
                            <span>{products[idx]['name']}</span>
                            <span>{products[idx]['spec']}</span>
                        </p>
                        <span>{(products[idx]['price']/100).toFixed(2) + '/' + products[idx]['unit']}</span>
                    </div>
                    <div className='drinksOperate'>
                        <span><label>数量：</label>
                    <input type='text' placeholder={count ? count : '数量'} defaultValue={count}
                            onInput={(e) => {this.changeInput(e, idx)}}/>
                        </span>
                        <span><input type='Checkbox' className='num'/>不限</span>
                    </div>
                </li>
            )
        })
        if(products.length > 0){
            return items
        }else{
            <li className='drinksProductsAnddrinksOperate'>没有商品</li>
        }
    }

    render() {
        let products = this.state.products &&
                         this.state.products.length > 0 ?
                         this.state.products : []
        console.log('@@@@@@@@', JSON.stringify(products))
        return (
            <div>
                <form onSubmit={ (e) => this.addFoods(e) }>
                    {this.templateFoods(products)}
                    <button type='submit' className='drinksSubmit'> 确定 </button>
                </form>
            </div>
        )
    }
}
