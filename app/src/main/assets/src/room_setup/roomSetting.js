import React from 'react'
import {Link, hashHistory} from 'react-router'

import Zepto from 'npm-zepto'
import LIBrary from '../lib/util'
import {REQUEST_URLS} from './setting'

import RoomNum from './roomNum'
import Dialog from '../components/dialog'
import ImgUpload from '../components/imgUpload'

export default class RoomSetting extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            rtId: this.props.rtId,
            roomInfo: this.props.roomInfo,
            ips: [],
            roomIndex: 0
        }
    }

    changeValue(e) {
        let roomInfo = this.state.roomInfo
        roomInfo[e.target.name] = e.target.value
        this.setState({
            roomInfo: roomInfo
        })
    }

    //获取ip和房间号
    getIPs(parmas) {
        if(typeof parmas != 'object'){
            return
        }
        LIBrary.fetch(REQUEST_URLS.roomIp + '?rt_id=' + parmas['rt_id'], {
            method: 'GET'
        }, (res) => {
            this.setState({ips: res.list, roomInfo: parmas})
        })
    }

    componentWillReceiveProps(props) {
        if(this.state.roomInfo == props.roomInfo){
            return
        }
        this.getIPs(props.roomInfo)
    }

    editRoom(e){
        let flag = false
        let method = Zepto(e.target).attr('data-type')
        const inputs = Zepto('.roomSetup ul').find('input')
        const btn = e.target
        let values = {}
        inputs.map((index, input) => {
            if(Zepto(input).attr('value') == ''){
                return
            }
            if((input.name == 'max_man' && isNaN(Zepto(input).attr('value'))) ||
                (input.name == 'min_man' && isNaN(Zepto(input).attr('value')))){
                flag = true
                str = '请输入正确人数'
                return
            }
            values[input.name] = Zepto(input).attr('value')
        })
        if(flag){
            LIBrary.warnning(str)
            return
        }
        values.rt_id = this.state.roomInfo['rt_id']
        values.name = this.state.roomInfo['name']
        delete(values['policy'])
        delete(values['signature'])
        delete(values['file'])
        LIBrary.fetch(REQUEST_URLS.roomType,{
            method: 'PUT',
            body: JSON.stringify(values)
        },(res) => {
            this.getIPs(values)
        })
    }

    render() {
        let data = this.state
        let img = data.roomInfo && data.roomInfo['pic'] ? data.roomInfo['pic'] : ''
        let maxMan = data.roomInfo && data.roomInfo['max_man'] ? data.roomInfo['max_man'] : ''
        let minMan = data.roomInfo && data.roomInfo['min_man'] ? data.roomInfo['min_man'] : ''
        let rtId = data.roomInfo && data.roomInfo['rt_id'] ? data.roomInfo['rt_id'] : ''
        return (
            <div className='roomSetup'>
                <h1 className='setuptitle'>包房设置
                    <span className='cursor' onClick={ (e) => this.editRoom(e) } data-type='put'>保存</span>
                </h1>
                <ul>
                    <li>已有包房：</li>
                    <li>
                        <RoomNum rooms={ this.state.ips } rtId={ rtId }/>
                    </li>
                    <li>
                        包房额定人数：
                        <input type='text' name='min_man' value={ minMan }  onChange={(e) => this.changeValue(e)}/>
                        --
                        <input type='text' name='max_man' value={ maxMan } onChange={(e) => this.changeValue(e)}/>
                        人
                    </li>
                    <li className='upLoadImg'>
                        <ImgUpload src={ img } remark={'图片jpg格式，最佳尺寸1080*1120'} />
                    </li>
                </ul>
            </div>
        )
    }
}
