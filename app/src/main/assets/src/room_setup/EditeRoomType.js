import React from 'react'
import LIBrary from '../lib/util'
import {REQUEST_URLS} from './setting'

import Dialog from '../components/dialog'

export default class leftNav extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            roomInfo: props.roomInfo,
            type: 'add',
            showDialog: false
        }
    }

    componentWillReceiveProps(props) {
        if (this.state.roomInfo == props.roomInfo) {
            return
        }
        this.setState({roomInfo: props.roomInfo})
    }

    addRoom(type) {
        this.setState({
            showDialog: !this.state.showDialog,
            type: type,
        })
    }

    addRoomType(list) {
        this.props.addRoomType(list)
    }

    deleteRoom() {
        this.props.deleteRoom()
    }

    putRoom(list) {
        this.props.putRoom(list)
    }

    //新增房间类型
    editBtn(type) {
        if (type == '') {
            return
        }
        if(type === 'add'){
            return <p>
                <button name='POST' onClick={(e) => this.editRoom(e)}>完成</button>
            </p>
        }
        if(type == 'put'){
            return <p >
               <button name='DELETE' onClick={(e) => this.editRoom(e)}>删除</button>
               <button name='PUT' onClick={(e) => this.editRoom(e)}>修改</button>
           </p>
        }
    }

    editRoom(event) {
        event.preventDefault()
        const form = Zepto(event.target).parents('form')
        const method = event.target.name.toLowerCase()
        let form_data = new Object()
        form.find('input').map((index, input) => {
            if (input.value == '') {
                return
            }
            form_data[input.name] = input.value
        })

        if(!form_data['name']){
            LIBrary.warnning('请输入房型名称')
            return
        }

        switch (method) {
            case 'post':
                form_data = {
                    'names': [form_data['name']]
                }
                LIBrary.fetch(REQUEST_URLS.roomType, {
                    method: method,
                    body: JSON.stringify(form_data)
                }, (res) => {
                    this.addRoomType(res.list[0])
                })
                break;

            case 'delete':
                LIBrary.fetch(REQUEST_URLS.roomType + '?rt_id=' + form_data['rt_id'], {
                    method: method
                }, (res) => {
                    this.deleteRoom()
                })
                break;

            case 'put':
                LIBrary.fetch(REQUEST_URLS.roomType, {
                    method: method,
                    body: JSON.stringify(form_data)
                }, (res) => {
                    this.putRoom(form_data['name'])
                })
                break;
        }
        this.setState({showDialog: false})
    }

    templateEditRooms(data) {
        let rtId = data && data['rt_id'] ? data['rt_id'] : ''
        let name = data && data['name'] ? data['name'] : ''
        let text = this.state.type == 'put' ? '修改房型' : '新建房型'
        return (
            <div className='addRoombox'>
                <form >
                    <h1>{ text }</h1>
                    <input name='rt_id' type='hidden' defaultValue={this.state.type == 'put' ? rtId : ''}/>
                    <input name='name' type='text' maxLength='16' defaultValue={this.state.type == 'put' ? name : ''}/>
                    <span className='limitNum'>
                        限16个字符
                    </span>
                    {this.editBtn(this.state.type)}
                </form>
            </div>
        )
    }

    render() {
        let data = this.state.roomInfo
        return (
            <ol className='EditeRoomType'>
                <div className="newRoom" onClick={(e) => { this.addRoom('add') }}>
                    <span>+</span>
                    <span>新建房型</span>
                </div>
                <div className="newRoom" onClick={(e) => { this.addRoom('put') }}>
                    <span className='editRoom'></span>
                    <span>修改房型</span>
                </div>
                <Dialog show={this.state.showDialog} onHide={(e) => this.addRoom()} children={this.templateEditRooms(data)} title={''}/>
            </ol>
        )
    }
}
