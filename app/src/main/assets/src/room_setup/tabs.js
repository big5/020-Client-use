/**
 * Created by lixinfeng on 2017/3/29.
 */

import React from 'react'
import LIBrary from '../lib/util'
import { REQUEST_URLS } from './setting'
import COMMON from '../lib/common'
import BasicTable from './basicTable'

export default class Tabs extends React.Component {
    constructor(props) {
        super(props)
        this.loadList=this.loadList.bind(this)
        this.state = {
            tabIndex:0,
            tabList:[],
            clearMsg:props.clearMsg
        }
    }

    componentWillReceiveProps(nextProps) {
        // console.log("tabs nextProps is :"+JSON.stringify(nextProps))
        if (this.state.clearMsg!=nextProps.clearMsg) {
            if (nextProps.clearMsg.clear) {
                this.setState({
                    tabList: []
                })
            } else {
                if (this.state.clearMsg.rtId != nextProps.clearMsg.rtId) {
                    // console.log(this.state.clearMsg.rtId,this.state.tabIndex)
                    this.loadMDData(this.state.tabIndex, nextProps.clearMsg.rtId)
                }
                this.setState({
                    clearMsg: nextProps.clearMsg
                })
                //点击太快导致二个 setState冲突  state下相应的属性无变化
                console.log("----------clearMsg------render--------------------------------")
            }
        }
    }

    loadMDData(day,rt_id){
        console.log("day is:"+day)
        LIBrary.fetch(REQUEST_URLS.roomFee+"?rt_id="+rt_id+"&md="+this.state.clearMsg.md+"&day_or_holiday="+(parseInt(day)+1), {
            method: 'GET'
        }, (res) => {
            // console.log("获取:"+JSON.stringify(res))
            this.setState({
                tabList:res.list,
                tabIndex:day
            })
        })
    }

    handleClick(e,index){
        if (index!=this.state.tabIndex){
            this.props.changeIndex({"index":index+1,"day":this.props.days[index]})
            this.loadMDData(index,this.state.clearMsg.rtId)
        }
    }

    isCurrent(index) {
        return index == this.state.tabIndex ? 'active': ''
    }

    loadList(){
        let tabItem = []
        let tabList = this.props.days
        LIBrary.times(tabList.length, (index) => {
            tabItem.push(<li key={index} onClick={(e)=>this.handleClick(e,index)}  className={this.isCurrent(index)+` `+`weekday`}>{tabList[index]}设置<span></span></li>)
        })
        return tabItem;
    }

    render() {
        let data = this.state
        return (
            <div className="tabs">
                <ul className="weekdays">
                    {this.loadList()}
                </ul>
                <BasicTable msg={data.tabList} nsg={{'day':data.tabIndex+1,'rt_id':data.clearMsg.rtId,'md':data.clearMsg.md}}/>
            </div>
        )
    }
}

Tabs.defaultProps={
    days:COMMON.WEEKS
}
