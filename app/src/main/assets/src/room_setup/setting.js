const REQUEST_URLS = {
    getIdentify: '/store/code',
    roomType: '/room/type',
    roomIp: '/room/ip',
    roomPack: '/room/pack',
    roomFee:'/room/fee',
    cate: '/cate',
    product: '/cate/product',
    pack:'/pack',
    packDetail:'/pack/product',
    storeAccount: '/store/account',
    backProduct: '/order/back/product'
}

export {REQUEST_URLS}
