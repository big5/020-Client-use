import React from 'react'
import LIBrary from '../lib/util'
import COMMON from '../lib/common'
import {REQUEST_URLS} from './setting'
import AddFoods from './addFoods'
import Dialog from '../components/dialog'

export default class PackDetail extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            foods: [],
            packId: 0,
            showDialog: false
        }
    }

    componentWillReceiveProps(props) {
        this.getFoods(props.packId)
    }

    deleteFood(e) {
        const productId = Zepto(e.target).parent().attr('data-productId')
        const index = Zepto(e.target).parent().index()
        LIBrary.fetch(REQUEST_URLS.packDetail + '?pack_id=' +
        this.state.packId + '&product_id=' + productId, {
            method: 'delete'
        }, (res) => {
            let foods = LIBrary.removeByIndex(this.state.foods, index)
            this.setState({foods: foods})
        })
        e.cancelBubble = true;
        e.stopPropagation();
    }

    showDialog() {
        this.setState({
            showDialog: !this.state.showDialog
        })
    }

    resetFoods(food) {
        this.packhasFood(food)
    }

    packhasFood(food) {
        let arr = this.state.foods
        let flag = true
        let productId = []
        arr.map((food, idx) => {
            productId.push(food['product_id'])
        })
        food.map((value, index) => {
            let idx = productId.indexOf(value['product_id'])
            if (idx >= 0) {
                arr[idx] = value
                return
            }
            arr.push(value)
        })
        if (!this.state.packId) {
            return
        }
        LIBrary.fetch(REQUEST_URLS.packDetail +
        '?pack_id=' + this.state.packId, {
            method: 'post',
            body: JSON.stringify(arr)
        }, (res) => {
            this.setState({foods: arr})
        })
    }

    getFoods(packId) {
        if (parseInt(packId) >= 0) {
            if (this.state.packId == packId) {
                return
            }
            LIBrary.fetch(REQUEST_URLS.packDetail +
            '?pack_id=' + packId, {
                method: 'get'
            }, (res) => {
                this.setState({
                    meal: res.detail,
                    foods: res.detail.list,
                    packId: packId
                })
            })
        } else {
            setTimeout(() => {
                this.setState({packId: packId})
            }, 1000)
        }
    }

    templateFoods(parmas) {
        let items = []
        LIBrary.times(parmas.length, (idx) => {
            let item = parmas && parmas[idx] ?
                parmas[idx] : ''
            let name = item && item['name'] ?
                item['name'] : ''
            let spec = item && item['spec'] ?
                item['spec'] : ''
            let unit = item && item['unit'] ?
                item['unit'] : ''
            let price = item && item['price'] ?
                (item['price'] / 100).toFixed(2) : ''
            let count = item && item['count'] > 0 ? item['count'] :
                (item['count'] == 0 ? '不限' : '')
            let productId = item && (item['product_id'] ||
                item['product_id'] == 0) ? item['product_id'] : ''
            items.push(
                <li key={idx} data-count={count} data-productId={productId}>
                    <div onClick={(e) => this.showDialog(e)}>
                        <span className='nameAndspec'>
                            <span>{name}</span>
                            <span>{spec}ml</span>
                        </span>
                        <span className='priceAndunit'>
                            <span>{price}元/</span>
                            <span>{unit}</span>
                        </span>
                        <span>{'数量' + '' + count}</span>
                    </div>
                    <span onClick={(e) => this.deleteFood(e)} className='delete'></span>
                </li>
            )
        })
        return items
    }

    render() {
        return (
            <div className='selectDrinks'>
                <div>
                    <label>选择酒水：</label>
                    <ul className='foods'>
                        {this.templateFoods(this.state.foods)}
                    </ul>
                </div>
                <div style={{ display: this.state.foods &&
                        this.state.foods.length == 0 ?
                        'block' : 'none' }}
                className='addMealDetailBtn'>
                    <p className='checkoutDrinks'>
                        <span onClick={(e) => this.showDialog(e)} className='addtime addMeal'>+ 添加</span>
                    </p>
                </div>
                <Dialog show={this.state.showDialog} title={''}
                    onHide={() => this.showDialog()}
                    children={
                        <AddFoods
                             packId = { this.state.packId}
                             foods = {this.state.foods}
                             resetFoods = {(food) => this.resetFoods(food)} />}/>
            </div>
        )
    }
}
