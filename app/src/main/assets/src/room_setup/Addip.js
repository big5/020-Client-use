import React from 'react'
import Zepto from 'npm-zepto'

import LIBrary from '../lib/util'
import COMMON from '../lib/common'

import {REQUEST_URLS} from './setting'

export default class Addip extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            count: 4,
            isShowDialog: false
        }
    }

    componentWillReceiveProps(props) {
        if(this.state.rtId == props.rtId){
            return
        }
        this.setState({
            rtId: props.rtId
        })
    }

    addRoom(list){
        this.props.addRoom(list)
    }

    inputDelete(event) {
        Zepto(event.target).parent().find('input').val()
    }

    addIpNum(){
        this.setState({
            count: this.state.count + 2
        })
    }

    addIP(event) {
        event.preventDefault()
        const rt_id = Zepto(event.target).data('id')
        let form_data = []
        let checkarr = []
        let flag = false
        let str = ''
        Zepto(event.target).find('input').map((index, input) => {
            let obj = {}
            let text = input.value.replace(/ /g, '')
            if (text == '') {
                return
            }

            if (checkarr.indexOf(text) >= 0) {
                flag = true
                str = '请勿提交相同信息'
                return
            }

            if (input.name == 'ip' &&
            !COMMON.IPREG.test(text)) {
                flag = true
                str = '请输入正确ip值'
            }

            if(input.name == 'mac' &&
            !COMMON.MACREG.test(text)){
                flag = true
                str = '请输入正确mac值'
            }

            if (input.name == 'name') {
                obj.name = text
                form_data.push(obj)
            }

            if (input.name == 'ip') {
                form_data[parseInt(index / 2)].ip = text
                form_data[parseInt(index / 2)].mac = ''
            }

            if(input.name == 'mac') {
                form_data[parseInt(index / 2)].mac = text
                form_data[parseInt(index / 2)].ip = ''
            }

            checkarr.push(text)
        })

        if (checkarr.length < 2) {
            LIBrary.warnning('请提交房间名，mac值或ip值')
            return
        }

        if (flag) {
            LIBrary.warnning(str)
            return
        }
        LIBrary.fetch(REQUEST_URLS.roomIp, {
            method: 'POST',
            body: JSON.stringify([
                {
                    rt_id: rt_id,
                    list: form_data
                }
            ])
        }, (res) => {
            //返回 数据
            this.addRoom(res.list)
        })
    }

    templateEditIps() {
        const data = this.state
        return (
            <div className='ipSetup'>
                <form onSubmit={(event) => { this.addIP(event) }}
                data-id={data.rtId}>
                    <h1>ip 设置</h1>
                    <ul>
                        {this.templateIps()}
                    </ul>
                    <p className='add' onClick={(e) => this.addIpNum()}>
                        <span>新增房间</span>
                    </p>
                    <button type='submit' className='cursor'>保存</button>
                </form>
            </div>
        )
    }

    //添加设置
    templateIps(arr) {
        let items = []
        let text = COMMON.TYPEIP == 'IP' ? 'IP地址' : 'MAC地址'
        let input = COMMON.TYPEIP =='IP'?
        <input type='text' name='ip' className='ipNo' placeholder='0.0.0.0'/> :
        <input type='text' name='mac' className='ipNo' placeholder='00 00 00 00 00 00'/>

        LIBrary.times(this.state.count, (idx) => items.push(
            <li key={idx}>
                <input type='text' name='name' className='roomNo' placeholder='请输入房间号'/>
                <label>{text}</label>
                {input}
                <span className='delete hide' onClick={(e) => this.inputDelete(e, idx)}></span>
            </li>
        ))
        return items
    }

    render() {
        return(
            <div>
                {this.templateEditIps()}
            </div>
        )
    }
}
