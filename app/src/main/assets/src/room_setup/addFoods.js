import React from 'react'
import Zepto from 'npm-zepto'
import LIBrary from '../lib/util'
import {REQUEST_URLS} from './setting'

import AddFoodsCell from './AddFoodsCell'

export default class addFoods extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            currentCate: 0,
            cates: []
        }
        this.packId = this.props.packId
    }

    componentDidMount() {
        LIBrary.fetch(REQUEST_URLS.cate, {
            method: 'GET'
        },(res) => {
            this.setState({
                cates: res.list
            })
        })
    }

    choose(event, index) {
        this.setState({
            currentCate: index
        })
    }

    resetFoods(food) {
        this.props.resetFoods(food)
    }

    templateFoods() {
        let items = []
        LIBrary.times(this.state.cates.length, (idx) => {
            items.push(
                <li key={idx} className={this.state.currentCate == idx ? 'active' : ''}
                onClick={(e) => this.choose(e, idx)} >{ this.state.cates[idx]['name'] }</li>
            )
        })
        return items
    }

    render() {
        let style = {display: 'none'}
        if(!(this.state.cates.length >= 1 &&
        typeof this.state.cates[0] == 'object')){
            style.display = 'block'
        }
        return (
            <div className='includeDrinks'>
                <h1>包含酒水</h1>
                <p className='hide' style={style}>
                    没有酒水，<a href='../pdt_setup/index.html#/product'>去设置</a>
                </p>
                <div className='includeDrinksDetail'>
                    <ul className='drinksDategory'>
                        {this.templateFoods()}
                    </ul>
                    <div>
                        <AddFoodsCell
                            packId={ this.props.packId }
                            cates={this.state.cates[this.state.currentCate]}
                            resetFoods={(food) => this.resetFoods(food) }/>
                    </div>
                </div>
            </div>
        )
    }
}
