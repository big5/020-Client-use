/*
 * @Author: anchen
 * @Date:   2017-03-27 08:59:08
 * @Last Modified by:   anchen
 * @Last Modified time: 2017-05-23 14:26:26
 */

'use strict';

import React from 'react'
import Tabs from './tabs'
import LIBrary from '../lib/util'
import {REQUEST_URLS} from './setting'

export default class Days extends React.Component {
    constructor(props) {
        super(props)
        this.state= {
            defaultDay:{"day":"周一",
                        "index":1},
            rtId: props.rtId,
            clear:false
        }
    }

    componentWillReceiveProps(props) {
        if (props.rtId==='' || this.state.rtId != props.rtId) {
            this.setState({rtId: props.rtId})
        }
    }

    clearTable(){
        LIBrary.fetch(REQUEST_URLS.roomFee+"?rt_id="+this.state.rtId+"&md="+this.props.param.md+"&day_or_holiday="+this.state.defaultDay.index, {
            method: 'DELETE'
        }, (res) => {
            this.setState({
                clear:true
            })
            console.log("清空table:"+JSON.stringify(res))
        })
    }
    changeIndex(d){
        if (d!=this.state.defaultDay) {
            this.setState({
                defaultDay: d,
                clear:false
            })
        }
    }
    render() {
        const  data = this.state
        return (
            <div className="days card clearfix">
                <div className="card-container">
                    <div className="card-header">
                        <h1 className = "card-header-title setuptitle">日常计费设置</h1>
                        <p className="openingHours">店铺营业时间：00:00--23:59</p>
                        <button className="clear" onClick={()=>this.clearTable()}>清空{data.defaultDay.day}设置</button>
                    </div>
                </div>
                <div className="card-body">
                    <Tabs  changeIndex={d=>this.changeIndex(d)} clearMsg={{'rtId':data.rtId,'clear':data.clear,'md':this.props.param.md}}/>
                </div>
                <div className='timewarning'>*时段设置没有覆盖全部营业时间</div>
            </div>
        )
    }
}

Days.defaultProps={
    param:{
        md:"day"
    }
}
