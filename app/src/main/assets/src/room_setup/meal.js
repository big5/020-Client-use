 'use strict';
import React from 'react'

import LIBrary from '../lib/util'
import {REQUEST_URLS} from './setting'
import MealCells from './mealCells'

export default class Meal extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            rtId: '',
            meals: [],
            add: false,
            cates: [],
            itemState:false
        }
    }

    componentWillReceiveProps(props) {
        if(this.state.rtId == props.rtId){
            return
        }
        this.getMeals(props.rtId)
    }

    getMeals(rtId) {
        LIBrary.fetch(REQUEST_URLS.roomPack + '?rt_id=' + rtId, {
            method: 'GET'
        }, (res) => {
            this.setState({
                rtId: rtId,
                meals: res.list
            })
        })
    }
    
    toggleItem() {
        console.log('meal is :'+111111)
        this.setState({
            itemState:!this.state.itemState
        })
    }

    render() {
        let data = this.state
        let rtId = data.rtId ? data.rtId : ''
        return (
            <div className="room_meal card clearfix">
                <div className="card-container" >
                    <div className="card-header" onClick={()=>this.toggleItem()}>
                        <h1 className="setuptitle">包房计费设置</h1>
                    </div>
                    <div className={data.itemState?"card-body ":"card-body hide"}>
                        <MealCells
                            rtId={ rtId }
                            meals={ data.meals }
                        />
                    </div>
                </div>
            </div>
        )
    }
}
