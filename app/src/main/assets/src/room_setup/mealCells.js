'use strict';
import React from 'react'

import LIBrary from '../lib/util'
import COMMON from '../lib/common'
import {REQUEST_URLS} from './setting'
import MealSingCell from './mealSingCell'
import AddMealForm from './addMealForm'

export default class MealCell extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            rtId: '',
            meals:[],
            putMeal: false,
            showDialog: false,
            cates: [],
            currentMeal: 0,
            add:true
        }
    }

    componentWillReceiveProps(props){
        if(this.state.meals == this.props.meal){
            return
        }
        this.setState({
            rtId: props.rtId,
            meals: props.meals,
            add: true
        })
    }

    showDialog() {
        this.setState({
            showDialog: !this.state.showDialog
        })
    }

    addMeal() {
        if(!this.state.add){
            return
        }
        this.setState({
            meals: this.state.meals.concat(['']),
            add:false
        })
    }

    putMeal(index) {
        this.setState({
            putMeal: !this.state.putMeal,
            currentMeal: index
        })
    }

    savePutMeal(value, index) {
        let meals = LIBrary.removeByIndex(this.state.meals, this.state.meals.length-1)
        this.setState({
            meals: meals.concat(value),
            currentMeal: index,
            putMeal: false
        })
    }

    cancelAddMeal(index){
        let meals = this.state.meals
        if(index ==meals.length-1 &&
            meals[index] == '' ){
            meals = LIBrary.removeByIndex(meals, index)
        }
        this.setState({
            add: true,
            meals: meals,
            currentMeal: index,
            putMeal: !this.state.putMeal
        })
    }

    deleteMeal(index) {
        let arr = LIBrary.removeByIndex(this.state.meals, index)
        this.setState({
            meals: arr,
        })
    }

    templateMeals(parmas) {
        let items = []
        let length = parmas && parmas.length > 0 ? parmas.length : 0
        LIBrary.times(length, (idx) => {
            let item = parmas[idx]
            items.push(
                <div className='' key={ idx }>
                    <div style={{ display: ( item && item['name'] ? 'block' : 'none' ) }}>
                        <MealSingCell
                            currentMeal={ idx }
                            rtId={ this.state.rtId }
                            meal={ item }
                            deleteMeal={(index) => this.deleteMeal(index)}
                            putMeal={() => this.putMeal(idx)}/>
                    </div>
                    <div style={{ display: (!item || item['name'] =='' ? 'block' : (idx == this.state.currentMeal && this.state.putMeal ? 'block' : 'none'))}}>
                        <AddMealForm
                            currentMeal={ idx }
                            meal={ item }
                            rtId={ this.state.rtId }
                            savePutMeal = {(value, index) => this.savePutMeal(value, idx) }
                            cancelAddMeal={() => this.cancelAddMeal(idx)}/>
                    </div>
                </div>
            )
        })
        return items
    }

    render() {
        let data = this.state
        return (
            <div className="roomMeal">
                {this.templateMeals(data.meals)}
                <button onClick={ (e) => { this.addMeal(e) }} className='addtime addMeal'>+ 添加套餐 </button>
            </div>
        )
    }
}
