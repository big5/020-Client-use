import React from 'react'
import LIBrary from '../lib/util'

import Dialog from '../components/dialog'
import AddIp from './Addip'
import EditIp from './editIp'

export default class roomNum extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            rtId: '',
            ips: [],
            type: 'put',
            showDialog: false
        }
    }

    componentWillReceiveProps(props){
        if(this.state.rooms == props.rooms){
            return
        }
        this.setState({
            rtId: props.rtId,
            ips: props.rooms
        })
    }

    showDialog(type){
        this.setState({
            showDialog: !this.state.showDialog
        })
    }

    addRoom(list) {
        this.setState({
            ips: this.state.ips.concat(list),
            showDialog: !this.state.showDialog
        })
    }

    render() {
        const data = this.state
        return (
            <div>
                <EditIp ips={ data.ips } rtId={ data.rtId } children={<span onClick={(e) => {this.showDialog()}}> + </span>}/>
                <Dialog show={ data.showDialog }
                    title=''
                    children={ <AddIp rtId={this.state.rtId} addRoom={ (list) => this.addRoom(list) }/> }
                    onHide={(e) => {this.showDialog()}}/>
            </div>
        )
    }
}
