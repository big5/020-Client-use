/**
 * Created by lixinfeng on 2017/3/30.
 */

'use strict';

import React  from 'react'
import LIBrary from '../lib/util'
import MenuItem from './menuItem'
export default class DropDownButton extends React.Component {
    constructor(props) {
        super(props)
        this.loadOptions=this.loadOptions.bind(this)
        this.toggleDropDown=this.toggleDropDown.bind(this)
        this.state={
            data:props.data,
            msg:props.msg,
            curIndex:0,
            open:false
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log("123")
    }

    curSelector(t){
        this.props.onSelectItem(t)
        // console.log("curSelector is :"+this.state.data[t])
        this.setState({
            msg:this.state.data[t],
            open:!this.state.open,
            curIndex:t
        })
    }

    toggleDropDown(){
        const open = this.state.open
        this.setState({
            open:!open
        })
    }

    componentWillReceiveProps(nextProps){
        this.setState({
            msg:nextProps.msg
        })
    }

    loadOptions(){
        const optionItems = []
        const optionList = this.state.data
        LIBrary.times(optionList.length, (index) => {
            optionItems.push(<MenuItem key={index} eventIndex={index} curSelector={(t)=>this.curSelector(t)} value={optionList[index]}/>)
        })
        return optionItems;
    }

    render(){
        //class open 显示menu列表  反之隐藏
        const  data = this.state
        return(
            <div className={data.open?"dropdown open ":"dropdown "} >
                <button id="dLabel" type="button" data-toggle="dropdown"  onClick={this.toggleDropDown}>
                    {data.msg?data.msg:data.data[0]}
                    <span className="caret"></span>
                </button>
                <ul className="dropdown-menu" aria-labelledby="dLabel" >
                    {this.loadOptions()}
                </ul>
            </div>
        )
    }
}

DropDownButton.ProtoTypes={
    onSelectItem:React.PropTypes.func
}
