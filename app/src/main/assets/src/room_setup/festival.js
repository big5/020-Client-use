/*
* @Author: anchen
* @Date:   2017-03-27 08:59:08
* @Last Modified by:   anchen
* @Last Modified time: 2017-04-21 17:38:06
*/

'use strict';

import React from 'react'
import FestivalTable from './festivalTable'
import LIBrary from '../lib/util'
import {REQUEST_URLS} from './setting'

export default class Festival extends React.Component {
    constructor(props) {
        super(props)
        this.getList = this.getList.bind(this)
        this.state={
            itemState:false,
            dataList:[],
            rtId:props.rtId
        }
    }
    
    componentDidMount() {
        this.state.rtId ? this.loadList(this.state.rtId):''
    }
    
    loadList(rtId) {
        LIBrary.fetch(REQUEST_URLS.roomFee+"?rt_id="+rtId+"&md="+this.props.param.md, {
            method: 'GET'
        }, (res) => {
            console.log("获取holiday:"+JSON.stringify(res))
            this.setState({
                dataList:this.getList(res.list)
            })
        })
    }
    
    componentWillReceiveProps(props) {
        if (props.rtId==='' || this.state.rtId != props.rtId) {
            this.setState({rtId: props.rtId})
            this.loadList(props.rtId)
        }
    }

    getList(arr) {
        let indexArr = []
        let dayArr = []
        let finalArr = []
        for (let i  in  arr) {
            dayArr.push(arr[i]['day_or_holiday'])
        }
        for (let  j in dayArr) {
            if (indexArr.indexOf(dayArr[j])==-1) {
                indexArr.push(dayArr[j])
            }
        }
        for (let m in indexArr) {
            let finArr = []
            let obj = {"day_or_holiday":indexArr[m]}
            for (let n in dayArr){
                if (indexArr[m] == dayArr[n]){
                    finArr.push(arr[n])
                }
            }
            obj.list = finArr
            finalArr.push(obj)
        }
        return finalArr
    }

    clearTable() {
        LIBrary.fetch(REQUEST_URLS.roomFee + "?rt_id=" + this.state.rtId + "&md=" + this.props.param.md, {
            method: 'DELETE'
        }, (res) => {
            console.log("DELETE all holiday :" + JSON.stringify(res))
            this.setState({
                dataList:[]
            })
        })
    }

    toggleItem() {
        this.setState({
            itemState:!this.state.itemState
        })
    }

    render() {
        //card-Title 控制card-Item显示隐藏
        const data = this.state
        return (
            <div className="festival card clearfix">
                <div className="card-container" >
                    <div className="card-header" onClick={()=>this.toggleItem()}>
                        <h1 className="card-header-title setuptitle">节日计费设置</h1>
                        <button className={data.itemState?"clear ":"clear  hide"} onClick={()=>this.clearTable()}>清空节日设置</button>
                    </div>
                    <div className={data.itemState?"card-body ":"card-body hide"}>
                        <FestivalTable res={{'rtId':data.rtId?data.rtId:"","list":data.dataList}}/>
                    </div>
                </div>
            </div>
        )
    }
}
Festival.defaultProps={
    param:{
        md:'holiday'
    }
}


