import React from 'react'
import LIBrary from '../lib/util'
import {REQUEST_URLS} from './setting'
import Dialog from '../components/dialog'
import COMMON from '../lib/common'

export default class editIp extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            ips: [],
            rtId: '',
            currentIps: 0,
            showDialog: false
        }
    }

    componentWillReceiveProps(props){
        if(this.state.ips == props.ips &&
            this.state.rtId == props.rtId){
            return
        }
        this.setState({
            ips: props.ips,
            rtId: props.rtId
        })
    }

    showDialog(){
        this.setState({
            showDialog: !this.state.showDialog
        })
    }

    chooseRoom(index){
        this.setState({
            currentIps: index,
            showDialog: true
        })
    }

    editIp(event) {
        event.preventDefault()
        const obj = event.target
        const method = (event.target.name).toLowerCase()
        const form = Zepto(event.target).parents('form')
        let url = REQUEST_URLS.roomIp
        let form_data = {}
        let flag = false
        let str = ''
        let data = {
            method: method
        }
        form.find('input').map((index, input) => {
            let text = input.value.replace(/ /g, '')
            if(text == ''){
                return
            }

            if(input.name == 'ip' &&
            !COMMON.IPREG.test(text)){
                flag = true
                str = '请输入正确ip值'
            }

            if(input.name == 'mac' &&
            !COMMON.MACREG.test(text)){
                flag = true
                str = '请输入正确mac值'
            }

            form_data[input.name] = text
        })
        if(flag){
            LIBrary.warnning(str)
            return
        }
        if(method == 'delete'){
            url += '?room_id='+form_data['room_id']
        }
        if(method == 'put'){
            data.body = JSON.stringify(form_data)
        }
        LIBrary.fetch(url, data, (res) => {
            let ips = this.state.ips
            if(method == 'delete'){
                ips = LIBrary.removeByIndex(ips, this.state.currentIps)
            }
            if(method == 'put'){
                ips[this.state.currentIps]['name'] = form_data['name']
                ips[this.state.currentIps]['ip'] = form_data['ip']
                ips[this.state.currentIps]['mac'] = form_data['mac']
            }
            this.setState({
                showDialog: false,
                ips: ips
            })
        })
    }

    templateRooms() {
        let items = []
        LIBrary.times(this.state.ips.length, (idx) => {
            items.push(
                <span onClick={(e) => {this.chooseRoom(idx)}} key={idx}> { this.state.ips[idx]['name'] } </span>
            )
        })
        return items
    }

    templateEditIps(index) {
        let data = this.state.ips[index] ? this.state.ips[index] : ''
        let roomId = this.state.ips.length > 0 && data['room_id'] ? data['room_id'] : ''
        let name = this.state.ips.length > 0 && data['name'] ? data['name'] : ''
        let Ip = this.state.ips.length > 0 && data['ip'] ? data['ip'] : ''
        let rtId = this.state.rtId ? this.state.rtId : ''
        let Mac = this.state.ips.length > 0 && data['mac'] ? data['mac'] : ''
        let input = COMMON.TYPEIP == 'IP' ? <p><label className='roomNamePopleft'>房间ip值：</label>
        <input name='ip' maxLength='12' defaultValue={ Ip } className='roomNamePopright'/></p> :
            <p><label className='roomNamePopleft'>房间mac值：</label>
            <input name='mac' maxLength='16' defaultValue={ Mac } className='roomNamePopright'/></p>
        return(
            <div className="ipSetup">
                <form>
                    <h1>修改房型</h1>
                    <input type='hidden' name='rt_id' defaultValue={rtId} />
                    <input type='hidden' name='room_id' defaultValue={ roomId }/>
                    <label className='roomNamePopleft'>房间名：</label>
                    <input name='name' defaultValue={ name } className='roomNamePopright'/>
                    {input}
                    <button type='submit' name='DELETE' onClick= {(e) => { this.editIp(e) }} className='roomNamePopdelete'>删除</button>
                    <button type='submit' name='PUT' onClick= {(e) => this.editIp(e) } className='roomNamePopdelete'>修改</button>
                </form>
            </div>
        )
    }
    render() {
        let data = this.state
        return (
            <div>
                {this.templateRooms()}
                {this.props.children}
                <Dialog show={ data.showDialog }
                     title={''}
                     onHide={(e) => this.showDialog()}
                     children={this.templateEditIps(this.state.currentIps)}
                />
            </div>
        )
    }
}
