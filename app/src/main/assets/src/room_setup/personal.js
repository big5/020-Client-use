import React from 'react'
import Title from './title'
import LIBrary from '../lib/util'
import {REQUEST_URLS} from './setting'
import COMMON from '../lib/common'
import md5 from 'MD5'

export default class IPSetup extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            store: '',
            clickable: false,
            isChangePwd: false,
            isChangePhone: false,
            identify: '获取验证码'
        }
    }

    componentDidMount() {
        this.getInformation()
    }

    changePwd(e) {
        this.setState({isChangePwd: !this.state.isChangePwd})
    }

    logout() {
        localStorage.clear()
        window.location.href = '../init_setup/index.html#/login'
    }

    changePhone() {
        this.setState({isChangePhone: !this.state.isChangePhone})
    }

    changeValue(e) {
        let store = this.state.store
        store[e.target.name] = e.target.value
        this.setState({store: store})
    }

    getInformation() {
        LIBrary.fetch(REQUEST_URLS.storeAccount, {
            method: 'get'
        }, (res) => {
            this.setState({store: res.store})
        })
    }

    getIdentify() {
        LIBrary.fetch(REQUEST_URLS.getIdentify+'?phone=' + Zepto('#phone').attr('value'),{
            method: 'GET'
        }, (res) => {
            this.countDown()
        })
    }

    countDown() {
        let number = 120
        let timer = ''
        this.setState({identify: number, clickable: false})
        timer = setInterval(() => {
            if (number <= 0 || isNaN(this.state.identify)) {
                self.setState({identify: '获取验证码', clickable: true})
                clearInterval(timer)
                return
            }
            this.setState({
                identify: number--
            })
        }, 1000)
    }

    submitPwd(e) {
        e.preventDefault()
        const form = Zepto(e.target)
        let value = {}
        let flag = false
        let str = ''
        form.find('input').map((index, input) => {
            if (input.value == '') {
                flag = true
                str = '请输入完整信息'
                return
            }
            if (input.name == 'password2' && input.value != value['newpasswd']) {
                flag = true
                str = '请输入两次相同密码'
                return
            }
            if (input.name == 'newpasswd' || input.name == 'passwd') {
                value[input.name] = md5(input.value)
                return
            }
        })

        LIBrary.fetch(REQUEST_URLS.storeAccount, {
            method: 'put',
            body: JSON.stringify(value)
        }, (res) => {

        })
    }

    submitPhone(e) {
        e.preventDefault()
        const form = Zepto(e.target)
        let value = {}
        let flag = false
        let str = ''
        form.find('input').map((index, input) => {
            if (input.value == '') {
                return
            }
            if (input.name == 'phone' && !COMMON.PHONEREG.test(input.value)) {
                flag = true
                str = '请输入正确手机号'
                return
            }
            value[input.name] = input.value
        })

        if (flag) {
            LIBrary.warnning(str)
            return
        }

        LIBrary.fetch(REQUEST_URLS.storeAccount, {
            method: 'put',
            body: JSON.stringify(value)
        }, (res) => {
            localStorage.clear()
            window.location.href = '../init_setup/index.html#/login'
        })
    }

    showInput(e) {
        let item = {}
        let input = Zepto(e.target).parent().find('input')
        item[input.attr('name')] = input.attr('value')
        LIBrary.fetch(REQUEST_URLS.storeAccount, {
            method: 'put',
            body: JSON.stringify(this.state.store)
        }, (res) => {})
    }

    render() {
        let store = this.state.store
        let name = store && store.name ? store.name : ''
        let address = store && store.address ? store.address : ''
        let st = store && store.st ? store.st : ''
        let ed = store && store.ed ? store.ed : ''
        let phone = store && store.phone ? store.phone : ''
        let style = { display: this.state.isChangePwd ? 'block' : 'none' }
        let style2 = { display: this.state.isChangePhone ? 'block' : 'none' }
        return (
            <div>
                <Title/>
                <div className='accountManagement'>
                    <h1 className=''>账号管理</h1>
                    <p>店铺名称：
                        <input ref='text' name='name' value={name} onChange={(e) => this.changeValue(e)} className='personInput'/>
                        <span onClick={(e) => this.showInput(e)}>修改</span>
                    </p>
                    <p>店铺地址：
                        <input type='text' name='address' value={address} onChange={(e) => this.changeValue(e)} className='personInput'/>
                        <span onClick={(e) => this.showInput(e)}>修改</span>
                    </p>
                    <p>营业时间：
                        <input type='time' name='st' value={st} onChange={(e) => this.changeValue(e)} className='accountTime'/> -- <input type='time' name='ed' value={ed} onChange={(e) => this.changeValue(e)} className='accountTime accountTime2'/>
                        <span onClick={(e) => this.showInput(e)}>修改</span>
                    </p>
                    <div>绑定手机：
                        <input type='text' value={phone} placeholder={phone} readOnly id='phone' className='personInput'/>
                        <span onClick={(e) => this.changePhone(e)}>修改</span>
                        <form style={style2} onSubmit={(e) => this.submitPhone(e)}>
                            <p>新手机号：<input type='text' name='phone' maxLength='11'/></p>
                            <p>验证码：<input type='text' name='code' maxLength='6'/>
                                <span onClick={() => this.getIdentify()}>{this.state.identify}</span>
                            </p>
                            <button>确认</button>
                        </form>
                    </div>
                    <div>登录密码：************
                        <span onClick={(e) => this.changePwd()}>修改</span>
                        <form style={style} onSubmit={(e) => this.submitPwd(e)}>
                            <input type='hidden' defaultValue={phone}/>
                            <p>旧密码：<input type='password' minLength='8' maxLength='12' name='passwd' placeholder='请输入旧密码'/></p>
                            <p>新密码：<input type='password' minLength='8' maxLength='12' name='newpasswd' className='password1' placeholder='请输入新密码'/></p>
                            <p>确认密码：<input type='password' minLength='8' maxLength='12' name='password2' className='password2' placeholder='再次确认密码'/></p>
                            <button >确认</button>
                        </form>
                    </div>

                    <button onClick={(e) => this.logout(e)}>
                        退出登录</button>
                </div>
            </div>
        )
    }
}
