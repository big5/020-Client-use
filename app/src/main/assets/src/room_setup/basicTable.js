/**
 * Created by lixinfeng on 2017/3/29.
 */
import React  from 'react'
import LIBrary from '../lib/util'
import TableRow from './tableRow'
import { REQUEST_URLS } from './setting'

export default class BasicTable extends React.Component {
    constructor(props) {
        super(props)
        this.loadTable=this.loadTable.bind(this)
        this.operateTableRow=this.operateTableRow.bind(this)
        this.state={
            msg:props.msg,
            nsg:props.nsg
        }
    }

    componentWillReceiveProps(nextprops) {
        if (JSON.stringify(this.state.msg) != JSON.stringify(nextprops.msg)||
            JSON.stringify(this.state.nsg) != JSON.stringify(nextprops.nsg)){
            console.log("it's deferent msg&nextprops.msg",JSON.stringify(nextprops.nsg))
            this.setState({
                msg:nextprops.msg,
                nsg:nextprops.nsg
            })
        }
    }

    operateTableRow(e,type,index) {
        let rowArr =[]
        for (let x of this.state.msg) {
            rowArr.push(x)
        }
        switch (type) {
            case 'add':
                if (!rowArr.length){
                    rowArr.push({'st': '00:00', 'ed': '00:00', 'fee': 10000})
                    this.setState({
                        msg:rowArr
                    })
                }else if(rowArr.length && rowArr[rowArr.length-1]['fee_id']){
                    const ed = rowArr[rowArr.length-1]['ed']
                    rowArr.push({'st': ed, 'ed': ed, 'fee': 10000})
                    this.setState({
                        msg:rowArr
                    })
                }
                break
            case 'del':
                console.log("del")
                if (rowArr[index]['fee_id']) {
                    LIBrary.fetch(REQUEST_URLS.roomFee+"?fee_id="+rowArr[index]['fee_id'], {
                        method: 'DELETE'
                    }, (res) => {
                        rowArr.splice(index, 1)
                        this.setState({
                            msg:rowArr
                        })
                    })
                }
                break
            case 'update':
                console.log("update")
                LIBrary.fetch(REQUEST_URLS.roomFee+"?rt_id="+this.state.nsg.rt_id+"&md="+this.state.nsg.md+"&day_or_holiday="+this.state.nsg.day, {
                    method:'GET'
                }, (res) => {
                    this.setState({
                        msg:res.list
                    })
                })
                break
            default:
                break
        }
    }

    loadTable() {
        let tabRowItems = []
        let data = this.state
        let tabRowList = data.msg
        if(!tabRowList.length){return}
        LIBrary.times(tabRowList.length, (index) => {
            tabRowItems.push(<TableRow key={index}  msg={{'rowObj':tabRowList[index],'eventIndex':index,'rtId':data.nsg.rt_id,'day':data.nsg.day,'md':data.nsg.md}}  operateTableRow={(e,type,index)=>this.operateTableRow(e,type,index)}/>)
        })
        return tabRowItems
    }


    render() {
        return (
            <div className="basicTable">
                <div className="table-header">
                    <table>
                        <colgroup>
                            <col className/>
                            <col className/>
                            <col className/>
                        </colgroup>
                        <thead>
                            <tr>
                                <th title data-field="time">时段</th>
                                <th title data-field="price">价格(元/小时)</th>
                                <th title data-field="do">操作</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div className="table-body">
                    <table>
                        <colgroup>
                            <col className/>
                            <col className/>
                            <col className/>
                        </colgroup>
                        <tbody>
                        {this.loadTable()}
                        <tr>
                            <td colSpan="3" className='roomTableAddTime'>
                                <button className="addtime" onClick={(e)=>this.operateTableRow(e,"add")}>+ 添加时段</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}
