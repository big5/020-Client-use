import React from 'react'
import LIBrary from '../lib/util'
import Dialog from '../components/dialog'
import COMMON from '../lib/common'
import {REQUEST_URLS} from './setting'
export default class Title extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            warnning: '',
            showDialog: false,
            time: '',
            countTime: 20,
            type: 0,
            store: {}
        }
        this.title = document.title
    }

    getInformation() {
        LIBrary.fetch(REQUEST_URLS.storeAccount, {
            method: 'get'
        }, (res) => {
            this.setState({store: res.store})
        })
    }

    time() {
        let date = new Date()
        let hour = date.getHours() >= 10 ? date.getHours() : '0' + date.getHours()
        let minute = date.getMinutes() >= 10 ? date.getMinutes() : '0' + date.getMinutes()
        // let second = date.getSeconds() >= 10 ? date.getSeconds() : '0' + date.getSeconds()
        let string = COMMON.TIME.substring(0,11)+ hour + ':' + minute
        this.setState({time: string})
    }

    warnning(str) {
        this.setState({warnning: str})
        setTimeout(() => {
            this.setState({warnning: ''})
        }, 2000)
    }

    showNav() {
        this.setState({
            type: 1,
            showDialog: !this.state.showDialog
        })
    }

    showDialog() {
        if( !this.state.type){
            return
        }
        this.setState({
            type: 0,
            showDialog: !this.state.showDialog
        })
    }

    cateDown() {
        this.setState({
            showDialog: true,
            countTime:this.state.countTime - 1
        })
        if(this.state.countTime <=0) {
            this.setState({
                countTime: 20,
                showDialog: false,
            },() => {
                this.warnning('支付时间过长，请重新支付')
            })
            return
        }
        setTimeout(() => {
            this.cateDown()
        }, 1000);
    }
    //支付倒计时
    templateCateDown() {
        return <div>{this.state.countTime}</div>
    }

    componentDidMount() {
        LIBrary.eventproxy.on('warnMsg', (str) => {
            this.warnning(str)
        });
        LIBrary.eventproxy.on('countDown', (str) => {
            this.cateDown()
        })
        LIBrary.eventproxy.on('closeCountDown', (str) => {
            this.state.time = 0
        })
        if (!this.timer) {
            this.timer = setInterval(() => this.time(), 60000)
        }
        this.time()
        this.getInformation()
    }

    templateNav() {
        return (
            <div className='homeConfiguration'>
                <div>
                    <a href='../room_setup/index.html#/room' className='roomSet'></a>
                    <a href='../pdt_setup/index.html#/product' className='drinksSet'></a>
                    <a href='../fm_setup/index.html#/financial' className='financialSet'></a>
                    {/* <a href='../charge_setup/index.html#/foodOrder' className='collectMoney'></a> */}
                    <a href='../charge_setup/index.html#/settleOrders' className='orderVerification'></a>
                </div>
            </div>
        )
    }

    templateDialogCon(type) {
        return type == 0 ?
        this.templateCateDown() :
        this.templateNav()
    }

    render() {
        const data = this.state
        let style = {display: (data.warnning ? 'block' : 'none')}
        return (
            <div className="title">
                <input type='hidden' id='storeId' defaultValue={this.state.store.store_id} />
                <a href='../room_setup/index.html#/personal'>个人中心</a>
                <div className='titleTitle'>
                    <strong>{this.title}</strong>
                    <span className='titleTime'>
                        {this.state.time}
                    </span>
                </div>
                <div className="titleRight">
                    <span onClick={(e) => this.showNav()} className='titleHomePage'></span>
                    <span onClick={() =>{location.reload()}} className='reload'>重载</span>
                    <a className='titleCashier' href='../charge_setup/index.html#/roomMessage'>收银</a>
                </div>
                <p style={style} className='failWarning'>{data.warnning}</p>
                <Dialog show={this.state.showDialog} title={''}
                    onHide={() => this.showDialog()}
                    children={this.templateDialogCon(this.state.type)}/>
            </div>
        )
    }
}
