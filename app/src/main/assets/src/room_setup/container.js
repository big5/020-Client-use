import React from 'react'
import {Link} from 'react-router'
import {REQUEST_URLS} from './setting'
import LIBrary from '../lib/util'
import Title from './title'
import LeftNav from './leftNav'
import RoomSetting from './roomSetting'
import Days from './days'
import Festival from './festival'
import Meal from './meal'
export default class roomTabs extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            roomInfo: '',
        }
    }

    getRoomInfo(value) {
        this.setState({
            roomInfo: value
        })
    }

    render() {
        let data = this.state
        let rtId =  typeof data.roomInfo == 'object' ? data.roomInfo['rt_id'] : ''
        return (
            <div className="room">
                <Title />
                <div className="downRoomSetup">
                    <LeftNav getRoomInfo= { (list) => this.getRoomInfo(list) }/>
                    <div className='setup'>
                        <RoomSetting roomInfo={ data.roomInfo }/>
                        <Days rtId={ rtId } />
                        <Festival rtId={ rtId } />
                        <Meal rtId={ rtId } />
                    </div>
                </div>
            </div>
        )
    }
}
