'use strict';
import React from 'react'

import LIBrary from '../lib/util'
import COMMON from '../lib/common'
import {REQUEST_URLS} from './setting'

export default class MealSingSell extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            rtId: this.props.rtId,
            meal:this.props.meal,
            currentMeal: this.props.currentMeal
        }
    }

    componentWillReceiveProps(props){
        if(this.state.meal == props.meal &&
            this.state.meal == props.currentMeal){
            return
        }
        this.setState({
            rtId: props.rtId,
            meal: props.meal,
            currentMeal: props.currentMeal
        })
    }

    putMeal(index) {
        this.props.putMeal(index)
    }

    deleteMeal(index) {
        this.props.deleteMeal(index)
    }

    deleteMeal(index) {
        LIBrary.fetch(REQUEST_URLS.roomPack + '?rt_id=' + this.state.rtId+
            '&pack_id=' + this.state.meal['pack_id'], {
            method: 'DELETE'
        }, (res) => {
            this.props.deleteMeal(index)
        })
    }

    render() {
        let data = this.state.meal
        return (
            <div className='edited'>
                <ul>
                    <li>
                        <p>{data['name']}</p>
                        <p>有效期：{data['st']} 至 {data['ed']}</p>
                    </li>
                    <li>
                        {(data['price'] / 100).toFixed(2)}元
                    </li>
                    <li>
                        <span>上架</span>
                        <span>下架</span>
                        <span onClick={() => { this.deleteMeal(this.state.currentMeal)}} >删除</span>
                        <span onClick={() => { this.putMeal(this.state.currentMeal) }} >修改</span>
                    </li>
                </ul>
            </div>
        )
    }
}
