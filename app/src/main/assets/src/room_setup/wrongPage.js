import React from 'react'
import Title from './title'

export default class Wrong extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    backUp() {
        history.back(-1)
    }

    render() {
        const data = this.state
        return (
            <div className="wrong">
                <Title />
            <button onClick={() => this.backUp()} className='back'>返回上一级</button>
            </div>
        )
    }
}
