/**
 * Created by lixinfeng on 2017/3/30.
 */

import React from 'react'
import DropDownButton from './select'
import LIBrary from '../lib/util'
import { REQUEST_URLS } from './setting'
import COMMON from '../lib/common'
import Zepto from 'npm-zepto'

export default class TableRow extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            msg: props.msg,
            st: props.msg.rowObj.st || '',
            ed: props.msg.rowObj.ed || ''
        }
    }

    componentWillReceiveProps(nextprops) {
        if (this.state.msg != nextprops.msg) {
            // console.log("TableRow's deferent msg",JSON.stringify(nextprops.msg))
            this.setState({
                msg:nextprops.msg,
                st: props.msg.rowObj.st || '',
                ed: props.msg.rowObj.ed || ''
            })
        }
    }

    dropDown(n,type) {
        switch (type) {
            case 'dropFirst':
                this.setState({st: COMMON.TIMES[n]})
                break
            case 'dropLast':
                this.setState({ed: COMMON.TIMES[n]})
                break
            default:break
        }
    }

    showfee(e) {
        Zepto(e.target).hide()
        Zepto(e.target).next("input").show().val(Zepto(e.target).text())
    }

    changeValue(e) {
        Zepto(e.target).hide()
        Zepto(e.target).prev().text(e.target.value).show()
    }

    changeNumber(e) {
        const val = Zepto(e.target).val()
        if(!COMMON.NUMBER.test(val)) {
            Zepto(e.target).val(val.substring(0,5))
            return LIBrary.warnning('只能输入0~5位整数')
        }
    }

    operateRow(e, type) {
        const $tr = $(e.target).parent().parent()
        let saveValue = $tr.find("input").val()
        let modifyValue = $tr.parent().find("input").val()
        const st = this.state.st
        const ed = this.state.ed
        switch (type) {
            case "modify":
                if(!COMMON.NUMBER.test(modifyValue)) {
                    return LIBrary.warnning("请输入0~5整数")
                }
                modifyValue *= 100
                LIBrary.fetch(REQUEST_URLS.roomFee, {
                    method: "PUT",
                    body: JSON.stringify({fee_id: this.state.msg.rowObj.fee_id, st, ed, fee: modifyValue})
                }, (res) => {
                })
                break;
            case "save":
                let saveObj
                if(!COMMON.NUMBER.test(saveValue)) {
                    return LIBrary.warnning("请输入0~5整数")
                }
                saveValue *=100
                switch (this.state.msg.md) {
                    case "day":
                        saveObj = [{
                                day: this.state.msg.day,
                                list: [{st, ed, fee: saveValue}]
                                }]
                        break;
                    case "holiday":
                        saveObj = [{
                                holiday: this.state.msg.day,
                                list: [{st, ed, fee: saveValue}]
                                }]
                        break;
                    default:
                        break;
                }
                console.log("this state msg rtId is :"+this.state.msg.rtId)
                LIBrary.fetch(REQUEST_URLS.roomFee+'?rt_id='+this.state.msg.rtId,{
                    method: "POST",
                    body: JSON.stringify(saveObj)
                }, (res) => {
                    this.props.operateTableRow(e, "update")
                })
                break;
            default:
                break
        }
    }

    componentWillReceiveProps(nextProps) {
        if (JSON.stringify(this.state.msg) !== JSON.stringify(nextProps.msg)) {
            this.setState({msg: nextProps.msg})
        }
    }

    shouldComponentUpdate(nProps, nState) {
        if (this.state.st !== nState.st || this.state.ed !== nState.ed) {
            return false
        }
        return true
    }

    render() {
        const data = this.state
        const fee = data.msg.rowObj.fee
            ? Number(data.msg.rowObj.fee / 100)
            : ""
        return (
            <tr className="tableRow">
                <td>
                    <DropDownButton msg={data.msg.rowObj.st} data={COMMON.TIMES} onSelectItem={n => this.dropDown(n,'dropFirst')}/>
                    ----
                    <DropDownButton msg={data.msg.rowObj.ed} data={COMMON.TIMES} onSelectItem={n => this.dropDown(n,'dropLast')}/>
                </td>
                <td>
                    <form action="">
                        <span onClick={e => this.showfee(e)} className='festivalPrice'>{fee}</span>
                        <input type="number" defaultValue={fee}
                               hidden="hidden"
                               placeholder={"请输入"}
                               onBlur={e=>this.changeValue(e)}
                               onChange={e=>this.changeNumber(e)}/>
                    </form>
                </td>
                <td >
                    <div className={data.msg.rowObj.fee_id
                        ? "rowSave"
                        : "rowSave hidden"}>
                        <button onClick={e => this.props.operateTableRow(e, "del", data.msg.eventIndex)}>删除</button>
                        <button className="change" onClick={(e, type) => this.operateRow(e, "modify")}>修改</button>
                    </div>
                    <button className={data.msg.rowObj.fee_id
                        ? "rowChange hidden"
                        : "rowChange"} onClick={(e, type) => this.operateRow(e, "save")}>保存</button>
                </td>
            </tr>
        )
    }
}

