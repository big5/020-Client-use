/**
 * Created by lixinfeng on 2017/4/7.
 */
'use strict';

import React  from 'react'
import LIBrary from '../lib/util'
import FestivalTableRow from './festivalTableRow'
import {REQUEST_URLS} from './setting'

export default class FestivalTable extends React.Component {
    constructor(props){
        super(props)
        this.loadTable=this.loadTable.bind(this)
        this.addDate=this.addDate.bind(this)
        this.curList=this.curList.bind(this)
        this.state={
            rtId:props.res.rtId,
            msg:props.res.list,
            isShowDialog:false
        }
    }

    componentWillReceiveProps(nextProps){
        if (this.props.res != nextProps.res){
            this.setState({
                rtId:nextProps.res.rtId,
                msg:nextProps.res.list
            })
        }
    }

    /*添加日期*/
    addDate() {
        let newArr = this.curList()
        newArr.push({
            day_or_holiday:'',
            list:[]
        })
        this.setState({
            msg:newArr
        })
    }

    curList() {
        let arr = []
        let msgArr = this.state.msg
        for (let i in msgArr){
            arr.push(msgArr[i])
        }
        return arr
    }

    delTableRow(e,n) {
        /*待优化 减少不必要请求 edit*/
        let rowArr = this.curList()
        let data = this.state
        const self = this
        if(data.msg[n].day_or_holiday){
            LIBrary.fetch(REQUEST_URLS.roomFee + "?rt_id=" + data.rtId + "&md=" + this.props.param.md + "&day_or_holiday=" + data.msg[n].day_or_holiday, {
                method: 'DELETE'
            }, (res) => {
                console.log("DELETE holiday :" + JSON.stringify(res))
                delRow()
            })
        }else{
            delRow()
        }
        function delRow() {
            rowArr.splice(n,1)
            self.setState({
                msg:rowArr
            })
        }
    }

    /*这里动态加载列表 tr中嵌套table  BasicTable组件*/
    loadTable(){
        const tabRowItems = []
        const tabRowList = this.state.msg
        if(!tabRowList.length){return}
        console.log("tabRowList is :"+JSON.stringify(tabRowList),tabRowList.length)
        LIBrary.times(tabRowList.length, (index) => {
            tabRowItems.push(<FestivalTableRow
                key={index}
                msg={{"obj":tabRowList[index],
                    "rtId":this.state.rtId,
                    "index":index}}
                delTableRow={(e,n)=>this.delTableRow(e,n)}
            />)
        })
        return tabRowItems
    }

    render() {
        return (
            <div className="festivalTable">
                <div className="table-header">
                    <table>
                        <colgroup>
                            <col className/>
                            <col className/>
                            <col className/>
                            <col className/>
                            <col className/>
                        </colgroup>
                        <thead>
                            <tr>
                                <th className="tableTitle" title data-field="date">日期</th>
                                <th className="tableTitle" title data-field="time">时段</th>
                                <th className="tableTitle" title data-field="price">价格(元/小时)</th>
                                <th className="tableTitle" title data-field="do">操作</th>
                                <th className="tableTitle" title data-field="del">删除</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div className="table-body">
                    <table>
                        <colgroup>
                            <col className/>
                            <col className/>
                            <col className/>
                            <col className/>
                            <col className/>
                        </colgroup>
                        <tbody>
                            {this.loadTable()}
                            <tr>
                                <td colSpan="5">
                                    <button onClick={this.addDate} className='addtime'>+ 添加日期</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

FestivalTable.defaultProps={
    param:{
        md:'holiday'
    }
}
