import React from 'react'
import LIBrary from '../lib/util'
import COMMON from '../lib/common'
import {REQUEST_URLS} from './setting'
import PackDetail from './packDetail'
import Dialog from '../components/dialog'

export default class addMealForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            cates: props.cates,
            currentMeal: props.currentMeal,
            rtId: props.rtId,
            meal: props.meal
        }
    }

    componentWillReceiveProps(props){
        if(this.state.meal == props.meal &&
            this.state.rtId == props.rtId &&
            this.state.cates == props.cates &&
            this.state.currentMeal == props.currentMeal){
            return
        }
        this.setState({
            currentMeal: props.currentMeal,
            rtId: props.rtId,
            meal: props.meal,
            cates: props.cates
        })
    }

    savePutMeal(value) {
        this.props.savePutMeal(value)
    }

    cancelAddMeal() {
        this.props.cancelAddMeal()
    }

    chooseWeeks(event) {
        const obj = event.target
        Zepto(obj).hasClass('active') ?
        Zepto(obj).removeClass('active') :
        Zepto(obj).addClass('active')
    }

    editroomMeal(event) {
        event.preventDefault()
        const form = Zepto(event.target)
        let method = ''
        let form_data = {}
        let weeks = []
        let list = []
        form.find('input').map((index, input) => {
            if (input.value == '') {
                return
            }
            form_data[input.name] = input.value
        })
        form.find('ul.foods').find('li').map((index, li) => {
            list.push({
                product_id: Zepto(li).attr('data-productId'),
                count: Zepto(li).attr('data-count') == '不限' ? 0 :Zepto(li).attr('data-count')
            })
        })
        form.find('button.weeks').map((index, btn) => {
            if(Zepto(btn).hasClass('active')){
                weeks.push(index+1)
            }
        })
        form_data.day = weeks.toString()
        if (isNaN(form_data['price'])) {
            LIBrary.warnning('请输入价格')
            return
        }
        if (LIBrary.exDateRange(form_data['start_date'], form_data['end_date']) > 0) {
            LIBrary.warnning('开始日期不能大于结束日期')
            return
        }
        form_data.price *= 100
        form_data.state = 1
        form_data.list = list
        method = !isNaN(this.state.meal['pack_id'])? 'put' : 'post'
        LIBrary.fetch(REQUEST_URLS.roomPack + '?rt_id=' + this.state.rtId, {
            method: method,
            body: JSON.stringify(form_data)
        }, (res) => {
            if(method == 'post'){
                this.savePutMeal(res.detail)
            }
            if(method == 'put'){
                this.savePutMeal(form_data)
            }
        })
    }

    templateweekday() {
        let items = []
        LIBrary.times(COMMON.WEEKS.length, (idx) => {
            items.push(
                <button key={idx} type='button'
                    className={'cursor' + ' ' +
                        'weeks' + ' '+
                        ( this.props.meal &&
                        this.props.meal['day'].indexOf(idx+1) >= 0 ?
                        'active' : '' )}
                    onClick={(e) => { this.chooseWeeks(e) }}>
                    {COMMON.WEEKS[idx]}
                </button>
            )
        })
        return items
    }

    render() {
        let data = this.state
        let datas = this.state.meal
        let name = datas && datas['name'] ? datas['name'] : ''
        let packId = datas && datas['pack_id'] ? datas['pack_id'] : ''
        let startDate = datas && datas['start_date'] ? datas['start_date'] : ''
        let endDate = datas && datas['end_date'] ? datas['end_date'] : ''
        let price = datas && datas['price'] ? (datas['price']/100).toFixed(2) : ''
        let hour = datas && datas['hour'] ? datas['hour'] : ''
        let st = datas && datas['st'] ? datas['st'] : ''
        let ed = datas && datas['ed'] ? datas['ed'] : ''
        return(
            <div className='editing'>
                <div>
                    <form onSubmit={(e) => this.editroomMeal(e)}>
                        <p>
                            <span>正在编辑...</span>
                            <button onClick={ () => this.cancelAddMeal(this.state.currentMeal)} type='button'> 取消 </button>
                            <button type='submit'>保存</button>
                        </p>
                        <p className='mealName'>
                            <label>套餐名称：</label>
                            <input type='text' name='name' defaultValue={ name }/>
                            <input type='text' name='pack_id' defaultValue={ packId } hidden/>
                        </p>
                        <p>
                            <label>有效期：</label>
                            <input type='date' name='start_date' defaultValue={ startDate }/>
                            --
                            <input type='date' name='end_date' defaultValue={ endDate }/>
                        </p>
                        <p className='mealPrice'>
                            <label>价格：</label>
                        <input type='text' name='price' defaultValue={ price }/>
                        </p>
                        <p>
                            <label>日期类别：</label>
                            {this.templateweekday()}
                        </p>
                        <p>
                            <label>欢唱时段：</label>
                            <input type='text' name='hour' defaultValue={ hour}/>
                        </p>
                        <p>
                            <label>可使用时段：</label>
                            <input type='time' name='st' defaultValue={st}/>
                            --
                            <input type='time' name='ed'
                                defaultValue={ ed }/>
                        </p>
                        <PackDetail packId={ packId } cates={ this.state.cates }/>
                    </form>
                </div>
            </div>
        )
    }
}
