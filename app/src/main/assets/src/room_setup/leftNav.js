import React from 'react'
import LIBrary from '../lib/util'
import {REQUEST_URLS} from './setting'
import Dialog from '../components/dialog'
import EditeRoomType from './EditeRoomType'

export default class leftNav extends React.Component {
    constructor(props) {
        super(props)
        this.state ={
            rooms: [],
            index: 0,
            type: 'add',
            showDialog: false
        }
    }

    componentWillMount(){
        setTimeout(() => {
            LIBrary.fetch(REQUEST_URLS.roomType, {
                method: 'GET'
            }, (res) => {
                this.setState({rooms: res.list})
                this.props.getRoomInfo(res.list[0])
            })
        }, 2000)
    }

    addRoom(type) {
        let arr = this.state.rooms
        this.setState({
            rooms: arr.concat([type])
        })
    }

    deleteRoom(){
        let arr = this.state.rooms
        arr = LIBrary.removeByIndex(arr, this.state.index)
        if(arr.length == 0){
            location.reload(true)
            return
        }
        this.setState({
            rooms: arr,
            index: 0
        })
    }

    putRoom(value) {
        let arr = this.state.rooms
        arr[this.state.index]['name'] = value
        this.setState({
            rooms: arr
        })
    }

    chooseRoom(index) {
        if(index == this.state.index){
            return
        }
        this.setState({
            index: index
        })
        this.props.getRoomInfo(this.state.rooms[index])
    }

    templateRooms() {
        let items = []
        LIBrary.times(this.state.rooms.length, (idx) => {
            items.push(
                <li key={idx} data-id={this.state.rooms[idx]['rt_id']}
                     className={(idx == this.state.index ? 'active': '') + ' ' + 'cursor'}
                     onClick={(e) => {this.chooseRoom(idx)}}>
                    {this.state.rooms[idx]['name']}
                </li>
            )
        })
        return items
    }

    render() {
        return(
            <div className="roomTypeSetupContain">
                <ol className='roomTypeSetup'>
                    {this.templateRooms()}
                </ol>
                <EditeRoomType roomInfo={ this.state.rooms[this.state.index]}
                    deleteRoom={() => this.deleteRoom()}
                    putRoom={(value) => this.putRoom(value)}
                    addRoomType={(value) => this.addRoom(value)}/>
            </div>
        )
    }
}
