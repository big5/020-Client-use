/**
 * Created by lixinfeng on 2017/4/1.
 */

import React  from 'react'

export default class MenuItem extends React.Component {
    constructor(props) {
        super(props)
        this.selected=this.selected.bind(this)
    }

    selected() {
        this.props.curSelector(this.props.eventIndex)
    }

    render() {
        return(
            <li className="menuItem"  onClick={this.selected}>{this.props.value}</li>
        )
    }
}
