import fetch from 'isomorphic-fetch'
import COMMON from '../../src/lib/COMMON.js'
let LIBrary = LIBrary || {}

LIBrary.fetch = (reqUrl, args, callback, isCached = false) => {
    reqUrl += localStorage.getItem('token') ?
            (reqUrl.indexOf('?') > 0 ?
            '&token=' + localStorage.getItem('token') :
            '?token=' + localStorage.getItem('token')) : ''
    args['mode'] = 'cors'
    fetch(COMMON.URL + reqUrl, args).then((res) => {
        return res.json()
    }).then((data) => {
        callback(data)
    }).catch(err => {
        alert(err)
    })
}

export default LIBrary
