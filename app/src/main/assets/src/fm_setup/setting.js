const REQUEST_URLS = {
    roomType: '/room/type',
    revenue:'/trade/stat/revenue',
    winePro:'/trade/stat/product'
}

export { REQUEST_URLS }
