/**
 * Created by lixinfeng on 2017/5/26.
 */

import React from 'react'
import LIBrary from '../lib/util'
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table'
import 'bootstrap/dist/css/bootstrap.css'
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css'

export class FMCard extends React.Component {
    render() {
        return(
            <div className="fmCard">
                {this.props.children}
            </div>
        )
    }
}


export class FMHeader extends React.Component{
    render () {
        return (
            <div className="fmHeader card-item">
                <div className="card-item-container">
                    {this.props.children}
                </div>
            </div>
        )
    }
}


export class FMTab extends React.Component{
    render () {
        return (
            <div className="fmTabCon card-item">
                <div className="card-item-container">
                    {this.props.children}
                </div>
            </div>
        )
    }
}


export class FMShow extends React.Component{
    constructor(props) {
        super(props)
        this.state = {
            des : props.des || {}
        }
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.des != this.state.des ) {
            this.setState({
                des : nextProps.des
            })
        }
    }

    templageChildren() {
        let items = []
        const titles = this.props.titles
        const desc = this.state.des
        let count = 0
        if (!this.state.des) { return }
        for (let des in desc) {
            items.push(<li  key={count}><span>{titles[count]}</span><br/><span>{desc[des]}</span></li>)
            count ++
        }
        return items
    }

    render () {
        return (
            <div className="show card-item">
                <div className="card-item-container">
                    <ul className="show-Menu">
                        {this.templageChildren()}
                    </ul>
                </div>
            </div>
        )
    }
}

FMShow.PropTypes={
    data:React.PropTypes.array
}



export class FMTable extends React.Component{
    constructor(props) {
        super(props)
        this.state = {
            titles:props.titles || [],
            titlesEng:props.titlesEng || [],
            options:props.options || {},
            products:props.products || [{aa:111,bb:222,cc:123},{aa:111,bb:222,cc:123}]
        }
    }

    templageTableRow () {
        let items = []
        const titles = this.state.titles
        const titlesEng = this.state.titlesEng
        if (!titles || !titles.length) { return }
        titles.map((title,index) => {
            index == 0 ?
                items.push(<TableHeaderColumn key={index} dataField={titlesEng[index]} isKey>{title}</TableHeaderColumn>) :
                items.push(<TableHeaderColumn key={index} dataField={titlesEng[index]} >{title}</TableHeaderColumn>)
        })
        return items
    }

    templageTable() {
        if (!this.state.products || !this.state.products.length) { return }
        return (
            <BootstrapTable data={ this.props.products }>
                {this.templageTableRow()}
            </BootstrapTable>
        )
    }

    render () {
        return (
            <div className="fmTable card-item">
                <div className="card-item-container">
                    {this.templageTable()}
                </div>
            </div>
        )
    }
}

FMTable.PropTypes = {
    titles:React.PropTypes.array
}


export default { FMCard, FMHeader, FMShow, FMTab, FMTable }
