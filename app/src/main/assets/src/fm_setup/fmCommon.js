/**
 * Created by lixinfeng on 2017/5/24.
 */

const FMCOMMON = {
    TABBAR_TITLE:['营收分析','账单查询','网络订单','提现管理'],
    REVENUE_TITLE:'营业统计',
    WINE_TITLE:'酒水统计',
    WINE_SORT:['按件数排行','销售额排行'],
    
    BILL_TITLE:'账单查询',
    FINANCE_TITLE:'财务查询',
    REVENUES:['总营业额','总房费收入','酒水收入'],
    
    REVENUE_TABS:['今天','昨天','近7天','近30天'],
    REVENUE_TABS_ENG:['today','yesterday','7days','30days'],
    BILL_TABS:['今天','昨天','上周','上个月'],
    //酒水排行
    WINE_TBS:['排行','商品名称','分类','销售数量(件)','销售额(元)'],
    WINE_TBS_ENG:['rank','name','cls','count','money'],
    //账单查询
    BILL_TBS:['序号','账单号','房号','总计','优惠','抹零','实收','结账时间'],
    BILL_TBS_ENG:['idx','order_no','room_num','money','seal_money','free_money','real_money','update_time'],
    //网络订单 账单查询
    INTERNET_TBS:['序号','订单号','支付金额','客户信息','账单号','房号','房型','订单类型'],
    INTERNET_TBS_ENG:['idx','bill_no','money','user_msg','order_no','room_no','room_type','bill_type'],
}

export default FMCOMMON
