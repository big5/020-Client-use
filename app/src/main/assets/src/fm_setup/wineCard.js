/**
 * Created by lixinfeng on 2017/5/26.
 */

import React from 'react'
import TabBar from './tabBar'
import FMCOMMON from './fmCommon'
import { REQUEST_URLS } from './setting'
import { FMCard, FMHeader, FMTab, FMTable } from './fmCard'

export default class WineCard extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            options:{

            },
            products:[]
        }
    }

    componentDidMount() {

    }

    loadData(str) {
        LIBrary.fetch(REQUEST_URLS.winePro+"?"+str, {
            method: 'GET'
        }, (res) => {
            this.setState({
                products : res.list
            })
        })
    }

    render() {
        return (
            <FMCard>
                <FMHeader>
                    <h2>{this.props.title}</h2>
                </FMHeader>
                <FMTab>
                    <select name="filter" id="filter">
                        <option value="">筛选分类</option>
                        <option value="">123</option>
                        <option value="">234</option>
                    </select>
                    <TabBar data={FMCOMMON.WINE_SORT}
                            defaultStyle= {`wineCard`}/>
                </FMTab>
                <FMTable options= {this.state.options}/>
            </FMCard>
        )
    }
}

WineCard.defaultProps = {
    title : FMCOMMON.WINE_TITLE
}
