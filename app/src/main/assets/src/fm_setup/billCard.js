/**
 * Created by lixinfeng on 2017/5/26.
 */
import React from 'react'
import {FMCard,FMHeader,FMShow,FMTab,FMTable} from './fmCard'

export default class BillCard extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    componentDidMount() {

    }

    templageSelectElement() {
        return(
            <div>
                <div className>
                    <select name="" id="sel-payType">
                        <option value="">支付方式</option>
                    </select>
                    <select name="" id="sel-roomType">
                        <option value="">房型</option>
                    </select>
                    <select name="" id="sel-roomId">
                        <option value="">房台号</option>
                    </select>
                </div>
                <TabBar data={FMCOMMON.REVENUE_TABS}/>
                <div className="float-right">
                    <label htmlFor="date-range">日期:</label>
                    <div id="date-range">
                        <select name="date" id="">
                            <option value="">2017年4月20日</option>
                        </select>
                        <span></span>
                        <select name="date" id="">
                            <option value="">2017年4月25日</option>
                        </select>
                    </div>
                    <button className="find" >find</button>
                </div>
            </div>
        )
    }

    render() {
        return (
            <FMCard>
                <FMHeader>
                    <h2>{this.props.title}</h2>
                </FMHeader>
                <FMTab>
                    {this.templageSelectElement()}
                </FMTab>
                <FMShow />
            </FMCard>
        )
    }
}
