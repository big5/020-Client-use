/**
 * Created by lixinfeng on 2017/5/25.
 */

import React from 'react'
import {REQUEST_URLS} from './setting'
import LIBrary from '../lib/util'
import FMCOMMON from  './fmCommon'
import TabBar from './tabBar'
import Zepto from 'npm-zepto'
import { FMCard, FMHeader, FMShow, FMTab } from './fmCard'

export default class RevenueCard extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            revenue : {},
            curIndex : 0
        }
        this.startDate = ''
        this.endDate = ''
    }
    
    componentDidMount() {
        this.loadData('day='+FMCOMMON.REVENUE_TABS_ENG[this.state.curIndex])
    }
    
    loadData(str) {
        LIBrary.fetch(REQUEST_URLS.revenue+"?"+str, {
            method: 'GET'
        }, (res) => {
            this.setState({
                revenue : res.revenue
            })
        })
    }
    
    findData() {
        this.loadData('start_day='+this.startDate+'&'+'end_day='+this.endDate)
    }
    
    changeDate(e) {
        if (e.target.getAttribute('name') == 'start_date') {
            this.startDate = Zepto(e.target).val()
        } else { this.endDate = Zepto(e.target).val() }
        console.log(this.startDate,this.endDate)
    }
    
    selectedIndex(idx) {
        this.state.curIndex = idx
        this.loadData('day='+FMCOMMON.REVENUE_TABS_ENG[idx])
    }
    
    templageSelectElement() {
        return (
            <div className="float-right">
                <div id="date-range">
                    <label >日期:</label>
                    <input type="date" name="start_date" onChange={e=>this.changeDate(e)}/>
                    <span>-</span>
                    <input type="date" name="end_date" onChange={e=>this.changeDate(e)}/>
                </div>
                <button className="find" onClick={()=>this.findData()} >查询</button>
            </div>
        )
    }
    
    render() {
        return (
            <FMCard>
                <FMHeader>
                    <h2>{this.props.title}</h2>
                    <a className={this.props.href+' '+'titleTime'}  href="#">导出报表</a>
                </FMHeader>
                <FMTab>
                    <TabBar data={FMCOMMON.REVENUE_TABS}/>
                    {this.templageSelectElement()}
                </FMTab>
                <FMShow titles={FMCOMMON.REVENUES} des={this.state.revenue}/>
            </FMCard>
        )
    }
}

RevenueCard.defaultProps= {
    title: FMCOMMON.REVENUE_TITLE,
    herf: 'export'
}

