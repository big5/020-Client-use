require('./app.scss')
require('../room_setup/app.scss')

import React from 'react'
import ReactDOM from 'react-dom'
import {Router, Route, hashHistory, Link, IndexRedirect} from 'react-router'
import financial from './financial'

ReactDOM.render((
    <Router history={hashHistory}>
        <Route path='/' component={financial}>
            <IndexRedirect to='financial' />
        </Route>
        <Route path='/financial' component={financial}/>
    </Router>
), document.getElementById('body'))
