/**
 * Created by lixinfeng on 2017/5/24.
 */
import React from 'react'
import LIBrary from '../lib/util'

export default class TabBar extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            curIndex:props.curIndex || 0,
            defaultStyle:props.defaultStyle || 'default'
        }
    }
    
    selectItem(idx) {
        this.props.selectedIndex(idx)
        if(idx != this.state.curIndex) {
            this.setState({
                curIndex:idx
            })
        }
    }
    
    templageChildren() {
        let items = []
        const list = this.props.data
        LIBrary.times(list.length, (index) => {
            items.push(<li key={index} className={`tabBarItem ` + this.state.defaultStyle}><a onClick={(index)=>this.selectItem(index)} className="item">{list[index]}</a></li>)
        })
        return  items
    }
    
    render () {
        return (
            <ul className="TabBar">
                {this.templageChildren()}
            </ul>
        )
    }
}

