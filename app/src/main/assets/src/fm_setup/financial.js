/**
 * Created by lixinfeng on 2017/4/14.
 */

import React from 'react'
import TabBar from './tabBar'
import FMCOMMON from './fmCommon'
import Title from '../room_setup/title'
import BillCard from './billCard'
import OrderCard from './orderCard'
import WineCard from './wineCard'
import RevenueCard from './revenueCard'

export default class Financial extends React.Component {
    constructor(props) {
        super(props)
        this.state={
            tabBarIndex:0
        }
    }
    
    templageChildren() {
        let card
        switch (this.state.tabBarIndex) {
            case 0:
                card = (<div><RevenueCard/><WineCard/></div>)
                break
            case 1:
                card = (<BillCard/>)
                break
            case 2:
                card = (<OrderCard/>)
                break
            case 3:
                card = (<WineCard/>)
                break
            default: break
        }
        return card
    }
    
    selectedIndex(idx) {
        this.setState({tabBarIndex:idx})
    }
    
    render () {
        return (
            <div className="financial">
                <Title />
                <div className="leftNav">
                    <TabBar data={FMCOMMON.TABBAR_TITLE}  selectedIndex={(idx)=>this.selectedIndex(idx)}/>
                </div>
                <div className="content">
                    <div className="content-container">
                        {this.templageChildren()}
                    </div>
                </div>
            </div>
        )
    }
}

