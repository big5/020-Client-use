import React from 'react'
import JSBridge from '../lib/bridge'
import Zepto from 'npm-zepto'
import {REQUEST_URLS} from './setting'
// import testPrintDate from './testprint'
import getTest from './getTest'
import TestAll from './testAll'
import PRINRNOTES from '../lib/printNote'
export default class Financial extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            clikeAble: true
        }
    }

    filter(data) {
        return (data / 100).toFixed(2)
    }

    printNotes(type) {
        switch (type) {
            case 'KT':
                PRINRNOTES.AFSingleBill('2017051614572237', 'KT2017051614572244')
                break;
            case 'DD':
                PRINRNOTES.AFSingleBill('2017051614572237', 'DD2017051714042966')
                break;
            case 'settle':
                PRINRNOTES.AllBill('2017051614572237')
                break;

        }
    }

    testDemo(type) {
        let data = {}
        switch (type) {
            case 'showSplitScreen':
                data = {
                    'display': true,
                    'content': 'www.baidu.com'
                }
        }
        JSBridge.test(type, data)
    }

    handleRoom(type) {
        let parma = {
            "mac": "001122334455", //mac地址
            "command": "056", //操作类型 (开台：056、续台：057、关台：058、关机：059、重启：060)
            "time": 60, //时长（倒计时开台，一直开台，续台时间）
            "type": 1, //开台类型
        }
        parma.mac = Zepto('#mac').val().replace(/ /g, '')
        parma.time = parseInt(Zepto('#time').val())
        parma.type = parseInt(Zepto('#type').val())
        switch (type) {
            case 'open':
                parma.command = '056'
                break
            case 'continue':
                parma.command = '057'
                break
            case 'close':
                parma.command = '058'
                break
            case 'shutDown':
                parma.command = '059'
                break
            case 'reOpen':
                parma.command = '060'
        }
        JSBridge.test('open', parma)
    }

    render() {
        let show = this.state.isshow ? 'block' : 'none'
        return (
            <div className="test" style={{
                marginTop: '100px',
                textAlign: 'center'
            }}>
                <input id='mac' placeholder='mac地址'/>
                <input id='time' placeholder='开台时间'/>
                <input id='type' placeholder='开台类型'/>
                <button onClick={(e) => this.testDemo('openBox')}>开钱箱</button>
                <button onClick={(e) => this.getPayCode('startPayCode')}>开启扫描</button>
                <button onClick={(e) => this.testDemo('getPayCode')}>读取扫码支付类容</button>
                <button onClick={(e) => this.testDemo('endPayCode')}>扫码支付完毕</button>
                <button onClick={(e) => this.testDemo('showSplitScreen')}>分屏</button>
                {/* 打印小票 */}
                <h1>打印测试</h1>
                <button onClick={(e) => this.printNotes('KT')}>开台</button>
                <button onClick={(e) => this.printNotes('DD')}>点单</button>
                <button onClick={(e) => this.printNotes('TD')}>退单</button>
                <button onClick={(e) => this.printNotes('XS')}>续时</button>
                <button onClick={(e) => this.printNotes('YD')}>预定</button>
                <button onClick={(e) => this.printNotes('ZT')}>转台</button>
                <button onClick={(e) => this.printNotes('YF')}>预付</button>
                <button onClick={(e) => this.printNotes('settle')}>结账</button>
                <h1>机顶盒测试</h1>
                <button onClick={(e) => this.handleRoom('open')}>开台</button>
                <button onClick={(e) => this.handleRoom('reOpen')}>重启</button>
                <button onClick={(e) => this.handleRoom('shutDown')}>关机</button>
                <button onClick={(e) => this.handleRoom('continue')}>续时</button>
                <button onClick={(e) => this.handleRoom('close')}>关台</button>
                <button onClick={(e) => this.show()}>3333</button>
            </div>
        )
    }

}
