const getTest = {
    "detail": {
        "describe": "123",
        "pay_type": 1,
        "update_time": "2017-05-17 15:05:52",
        "prepay": 0,
        "order_no": "2017051614313912",
        "state": 1,
        "room_id": 277,
        "minute": 180,
        "room_name": "1101",
        "st": "14:29",
        "ed": "17:29",
        "bills": [
            {
                "describe": "",
                "update_time": "2017-05-16 14:31:39",
                "rate": 100,
                "real_money": 30000,
                "pay_state": 1,
                "pay_md": 1,
                "bill_no": "KT2017051614313985",
                "money": 30000,
                "service_type": 1,
                "list": [
                    {
                        "update_time": "2017-05-16 14:31:39",
                        "fee_id": 299,
                        "st": "14:29",
                        "price": 20000,
                        "money": 20000,
                        "unit": "",
                        "count": 1,
                        "md": 2,
                        "pack_id": 95,
                        "name": "杨枝玉露",
                        "product_id": 0,
                        "list": []
                    }, {
                        "money": 9600,
                        "pack_id": 0,
                        "unit": "\u74f6",
                        "md": 1,
                        "money": 23940,
                        "ed": "17:29",
                        "pack_id": 0,
                        "minute": 180
                    }
                ]
            },
            {
                "describe": "123",
                "update_time": "2017-05-17 15:05:17",
                "rate": 100,
                "real_money": 30000,
                "pay_state": 1,
                "pay_md": 1,
                "bill_no": "DD2017051715051792",
                "money": 28600,
                "service_type": 2,
                "list": [
                    {
                        "name": "333",
                        "update_time": "2017-05-17 15:05:17",
                        "unit": "",
                        "product_id": 0,
                        "list": [],
                        "price": 3300,
                        "money": 6600,
                        "md": 2,
                        "count": 2,
                        "pack_id": 176
                    },
                    {
                        "name": "22",
                        "update_time": "2017-05-17 15:05:17",
                        "unit": "",
                        "product_id": 0,
                        "list": [],
                        "price": 2200,
                        "money": 22000,
                        "md": 2,
                        "count": 10,
                        "pack_id": 177
                    },
                    {
                        "name": "洛阳宫",
                        "update_time": "2017-05-17 15:05:52",
                        "unit": "瓶",
                        "product_id": 174,
                        "price": 1500,
                        "money": 15000,
                        "md": 1,
                        "count": 10,
                        "pack_id": 0
                    }
                ]
            }
        ]
    }
}

export default getTest
