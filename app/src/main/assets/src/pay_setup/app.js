require('./app.css')
import React from 'react'
import ReactDOM from 'react-dom'
import {Router, Route, hashHistory, Link, IndexRedirect} from 'react-router'
import WechatPay from './wechatPay'
import PosPay from './posPay'
import AliPay from './aliPay'
ReactDOM.render((
    <Router history={hashHistory}>
        <Router path='/' >
            <IndexRedirect to='wechatPay' />
        </Router>
        <Router path='/aliPay' component={AliPay} />
        <Router path='/posPay' component={PosPay} />
        <Router path='/wechatPay' component={WechatPay} />
    </Router>
), document.getElementById('body'))
