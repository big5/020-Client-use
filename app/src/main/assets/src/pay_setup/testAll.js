const testAll = {
           "titleKTLeft": "左头标题",
           "titleKT": "右头标题",
           "titleType": "票据类型",
           "titleAD": "票据AD",
           "titleSK": "票据SK",
           "roomName": "房台",               //房台号
           "OperationType": "操作类型",        //操作类型
           "printFrequency": 1,                //打印次数
           "openTime": "2017-5-4 14:35:34",    //开台时间
           "backTime": "2017-5-4 14:36:20",    // 退单时间
           "checkoutTime": "2017-5-4 14:35:37",//结账时间
           "roomPackage": "王子公主嗨起来",       //房间套餐
           "originalBill": "票据类型",         //原始单号
           "room": [
               {
                   "timeInterval": "10:00-12:00",//时段
                   "timeLength": "120",        //时长
                   "timeUnitPrice": "10",      //时间单价
                   "timeSubtotal": "600",      //时段小计
               },
               {
                   "timeInterval": "12:00-13:00",
                   "timeLength": "60",
                   "timeUnitPrice": "10",
                   "timeSubtotal": "600",
               }
           ],
           "beer": [
               {
                   "beerName": "哈尔滨冰纯",    //酒水
                   "beerUnitPrice": "120",     //酒水单价
                   "beerCount": "10",          //酒水数量
                   "beerSubtotal": "1200",     //酒水小计
               },
               {
                   "beerName": "勇闯天涯",
                   "beerUnitPrice": "60",
                   "beerCount": "10",
                   "beerSubtotal": "600",
               }
           ],
           "totalPay": "1260",     //总计
           "hasPayMoney": "500",   //已付
           "discount": "200",      //优惠
           "percent": "10",        //打折
           "rounding": "60",       //抹零
           "shouldIncome": "500",  //应收/应退
           "realIncome": "1000",   //收款/退款
           "refund": "0",          // 找零
           "payType": 3,           //支付方式
           "payTypeMoney": "500",  //支付方式金额。
           "netReceivables": "112541204156",   //网络收款单号。
           "reMarks": "小费",        //备注。
   }
