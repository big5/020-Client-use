/**
 * Created by Administrator on 2017-05-03.
 */
function requireAll(requireContext) {
    return requireContext.keys().map(requireContext);
}

var modules = requireAll(require.context("./test", true, /.+\.spec\.js?$/));

module.exports = modules