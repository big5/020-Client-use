var webpack = require('webpack')
var path = require('path')

module.exports = {
    entry: {
        init_setup: './src/init_setup/app.js',
        room_setup: './src/room_setup/app.js',
        pdt_setup: './src/pdt_setup/app.js',
        fm_setup: './src/fm_setup/app.js',
        charge_setup: './src/charge_setup/app.js',
        pay_setup: './src/pay_setup/app.js'
    },
    output: {
        path: './',
        filename: './src/[name]/bundle.js'
    },
    module: {
        // preLoaders: [
        //     {
        //         test: /\.js$/,
        //         exclude: /node_modules/,
        //         loader: 'eslint-loader'
        //     }
        // ],
        loaders: [
            {
                test: /\.css$/,
                loader: 'style!css'
            }, {
                test: /\.js$/,
                loader: 'jsx'
            }, {
                test: /\.(png|jpg|eot|svg|ttf|woff|woff2)$/,
                loader: 'url?limit=1000000&name=images/[name].[ext]&emitFile=false'
            }, {
                test: /\.scss$/,
                loader: 'style!css!sass'
            }, {
                test: /\.js|jsx$/,
                exclude: /node_modules/,
                loader: 'babel'
            }, {
                test: /\.json$/,
                loader: 'json'
            }
        ]
    },
    externals: {
        'react/addons': true,
        'react/lib/ExecutionEnvironment': true,
        'react/lib/ReactContext': true
    },
    resolve: {
        root: './src',
        extensions: ['', '.js', '.json', '.scss']
    },
    // eslint: {
    //     configFile: './.eslintrc'
    // },
    plugins: [new webpack.HotModuleReplacementPlugin()]
}
