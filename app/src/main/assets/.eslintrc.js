module.exports = {
    "env": {
        "browser": true,
        "es6": true,
        "node": true
    },
    "extends": "airbnb",
    "rules": {
        "max-len": [1, 120, 2, {ignoreComments: true}],
        "prop-types": [2]
    },
    "parser": "babel-eslint",
    "plugins": [
        "react"
    ]
}
