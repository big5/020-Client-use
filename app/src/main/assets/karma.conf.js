// Karma configuration
// Generated on Wed May 03 2017 10:53:07 GMT+0800 (CST)
var webpackConfig = require('./webpack.config');
webpackConfig.devtool = 'inline-source-map';
module.exports = function(config) {
    config.set({
        // client: { mocha: { timeout : 60000 } },

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '',

        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: [ "mocha", "chai", "sinon", "sinon-chai" ],
        webpack: webpackConfig,
        webpackServer: {
            noInfo: true
        },
        plugins: [
            "karma-chrome-launcher",
            "karma-chai",
            "karma-mocha",
            "karma-sourcemap-loader",
            "karma-webpack",
            "karma-mocha-reporter",
            "karma-sinon",
            "karma-sinon-chai"
        ],
        // list of files / patterns to load in the browser
        files: [
           "test.webpack.js"
        ],

        // list of files to exclude
        exclude: ['*.json'],

        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {'test.webpack.js': [ 'webpack', 'sourcemap' ]},

        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: ['mocha'],

        // web server port
        port: 9876,

        // enable / disable colors in the output (reporters and logs)
        colors: true,

        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,

        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: false,

        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['Chrome'],
        customLaunchers: {
            ChromeHeadless: {
                base: 'Chrome',
                flags: ['--headless', '--disable-gpu', '--remote-debugging-port=9222', 'http://0.0.0.0:9876/']
            }
        },
        browserNoActivityTimeout: 60000,
        captureTimeout:60000,
        browserDisconnectTimeout:2000,
        browserDisconnectTolerance:0,
        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: true,

        // Concurrency level
        // how many browser should be started simultaneous
        concurrency: Infinity
    })
}



