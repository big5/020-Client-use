/**
  * Created by Administrator on 2017-05-03.
  */
/**
  * Created by Administrator on 2017-05-02.
  */
import 'babel-polyfill'
import fetch from 'isomorphic-fetch'
import LIBrary from '../lib/util.js'
import {REQUEST_URLS} from '../../src/init_setup/settings.js'
import md5 from 'MD5'
const TESTCOMMON = require('../testDatas/initData.json')
//无论房间名是否添加都成功
describe('添加包房类型', () => {
    it('添加单个房型，成功', (done) => {
        LIBrary.fetch(REQUEST_URLS.roomType + "?token=" + TESTCOMMON.token, {
            method: 'POST',
            body: JSON.stringify({names: ['111']})
        }, (res) => {

            expect(res.errcode).to.be.equal(200)
            done()
        })
    })
    it('添加多个房型，成功', (done) => {
        LIBrary.fetch(REQUEST_URLS.roomType + "?token=" + TESTCOMMON.token, {
            method: 'POST',
            body: JSON.stringify({
                names: ['大包', '中包', '小包']
            })
        }, (res) => {
            expect(res.errcode).to.be.equal(200)
            done()
        })
    })
    it.skip('添加空房型，失败', (done) => {
        LIBrary.fetch(REQUEST_URLS.roomType + "?token=" + TESTCOMMON.token, {
            method: 'POST',
            body: JSON.stringify({names: []})
        }, (res) => {
            expect(res.errcode).to.be.equal(50001)
            done()
        })
    })

})

describe('获取所有包房类型', () => {
    it('包房数量不为空，正确', (done) => {
        LIBrary.fetch(REQUEST_URLS.roomType + "?token=" + TESTCOMMON.token, {
            method: 'GET'
        }, (res) => {
            expect(res.errcode).to.be.equal(200)
            done()
        })
    })
    it.only('包房数量为空，正确', (done) => {
        LIBrary.fetch(REQUEST_URLS.roomType + "?token=" + TESTCOMMON.pureToken, {
            method: 'GET'
        }, (res) => {

    it('包房数量为空，正确', (done) => {
        LIBrary.fetch(REQUEST_URLS.roomType + "?token=" + COMMON.pureToken, {method: 'GET'}, (res) => {
            expect(res.list.length).to.be.equal(0)
            done()
        })
    })
})

//无论有无包房，均能删除成功
describe('删除包房类型',()=> {
    it('包房类型不存在，删除失败',(done)=>{
        LIBrary.fetch(REQUEST_URLS.roomType+"?token="+COMMON.pureToken+"&rt_id="+COMMON.roomTypeId,{method:'DELETE'},(res)=>{
            expect(res.errcode).to.be.equal(50001)
            done()
        })
    })

    it('包房类型存在，删除成功',(done)=>{
        LIBrary.fetch(REQUEST_URLS.roomType+"?token="+COMMON.token+"&rt_id="+36,{method:'DELETE'},(res)=>{
            expect(res.errcode).to.be.equal(200)
            done()
        })
    })

})

//第一个case对应的包房类型id应该跟名称一致，在json中设置，第二个case名称需要和中包不一样
describe('修改包房类型',()=>{
    it('名称不发生变化，修改失败',(done)=>{
        LIBrary.fetch(REQUEST_URLS.roomType+'?token='+COMMON.token,{method:'PUT',body:JSON.stringify({rt_id:COMMON.roomTypeId,name:COMMON.roomTypeNam,pic:COMMON.roomTypePic, min_man:COMMON.roomTypeMinMan,max_man:COMMON.roomTypeMaxMan})},
            (res)=>{
            expect(res.errcode).to.be.equal(200)
            done()
        })
    })

    it('名称发生变化，修改成功',(done)=>{
        LIBrary.fetch(REQUEST_URLS.roomType+'?token='+COMMON.token,{method:'PUT',body:JSON.stringify({rt_id:COMMON.roomTypeId,name:"中包",pic:COMMON.roomTypePic, min_man:COMMON.roomTypeMinMan,max_man:COMMON.roomTypeMaxMan})},
            (res)=>{
            expect(res.errcode).to.be.equal(200)
            done()
        })
    })

    it('名称变成空，修改失败',(done)=>{
        LIBrary.fetch(REQUEST_URLS.roomType+'?token='+COMMON.token,{method:'PUT',body:JSON.stringify({rt_id:COMMON.roomTypeId,name:"",pic:COMMON.roomTypePic, min_man:COMMON.roomTypeMinMan,max_man:COMMON.roomTypeMaxMan})},
            (res)=>{
            expect(res.errcode).to.be.equal(200)
            done()
        })
    })
})

//运行该用例前，请检查rt_id下是否有包房，在intidata.json中修改id
describe('获取X房型下的所有包房列表',()=>{
    it('该房型下没有包房，获取成功',(done)=>{
        LIBrary.fetch('/room/ip'+'?token='+COMMON.token+'&rt_id='+357,{method:'GET'},(res)=>{
            expect( res.list.length).to.be.equal(0)
            done()
        })
    })

    it('该房型下有包房，获取成功',(done)=>{
        LIBrary.fetch('/room/ip'+'?rt_id='+COMMON.roomTypeId+'&token='+"DroImPxMhY1CF4GXW_bLMQjlFs0O8WIQ",{method:'GET'},(res)=>{
        expect(res.list.length).to.be.not.equal(0)
        done()
        })
    })
})

//包房添加的数据只能在该段代码中修改
describe('包房添加',()=>{
    it('添加多个包房',(done)=>{
        LIBrary.fetch('/room/ip'+"?token="+COMMON.token,{method:'POST',body:JSON.stringify([{
            "rt_id":358 ,
             "list":[
                { "name": "101", "ip": "192.168.0.1" },
                { "name": "102", "ip": "192.168.0.4" },
                { "name": "103", "ip": "192.168.0.15" }
             ]
            }])},(res)=>{
            expect(res.errcode).to.be.equal(200)
            done()
        })
    })

    it('添加空包房',(done)=> {
        LIBrary.fetch('/room/ip' + "?token=" + COMMON.token,{ method: 'POST', body: JSON.stringify([])},(res) =>{
            expect(res.errcode).to.be.equal(50001)
            done()
        })
    })

    it('添加单个包房',(done)=>{
        LIBrary.fetch('/room/ip'+"?token="+COMMON.token,{method:'POST',body:JSON.stringify([{
        "rt_id":358 ,
        "list":[
            { "name": "1011", "ip": "192.168.0.2" }
        ]
        }])},(res)=>{
            expect(res.errcode).to.be.equal(200)
            done()
        })
    })
})

//删除已存在包房时，在initData.json中把room_id改成已存在的数据
describe('删除包房',()=> {
    it('包房不存在，删除失败',(done)=>{
        LIBrary.fetch('/room/ip'+"?token="+COMMON.token+"&room_id="+COMMON.roomId,{method:'DELETE'},(res)=>{
            expect(res.errcode).to.be.equal(10001)
            done()
        })
    })

    it('包房存在，删除成功',(done)=>{
        LIBrary.fetch('/room/ip'+"?token="+COMMON.token+"&room_id="+36,{method:'DELETE'},(res)=>{
            expect(res.errcode).to.be.equal(200)
            done()
        })
    })
})

describe('修改房型',() => {
    it('该房间已经不存在，修改失败，' ,(done) =>{
        LIBrary.fetch('/room/ip'+"?token="+COMMON.token,{method:'PUT',body:JSON.stringify({room_id:COMMON.roomId,name:COMMON.roomName,ip:COMMON.roomIp,room_type:COMMON.roomState})},(res)=>{
            expect(res.errcode).to.be.equal(50001)
            done()
        })
    })

it('该房间存在，修改成功，' ,(done) =>{
    LIBrary.fetch('/room/ip'+"?token="+COMMON.token,{method:'PUT',body:JSON.stringify({room_id:COMMON.roomId,name:COMMON.roomName,ip:COMMON.roomIp,room_type:COMMON.roomState})},(res)=>{
        expect(res.errcode).to.be.equal(200)
        done()
        })
    })
})

//该部分的数据在该单元中修改
describe("计费设置添加",()=>{
    it("计费存在空数据",(done)=>{
        LIBrary.fetch('/room/fee'+'?rt_id='+COMMON.roomTypeId+'&token='+COMMON.token,{method:'POST',body:JSON.stringify([])},(res)=>{
            expect(res.errcode).to.be.equal(50001)
            done()
        })
    })

    it("计费数据有效,添加成功",(done)=>{
        LIBrary.fetch('/room/fee'+'?rt_id='+COMMON.roomTypeId+'&token='+COMMON.token,{method:'POST',body:JSON.stringify([{
            "day": 4,
            "list": [{"st": "00:00","ed" : "00:00", "fee": 7500}]
        }])},(res)=>{
            expect(res.errcode).to.be.equal(200)
            done()
        })
    })

    it("节假日计费",(done)=>{
        LIBrary.fetch('/room/fee'+'?rt_id='+COMMON.roomTypeId+'&token='+COMMON.token,{method:'POST',body:JSON.stringify([{
            "holiday": "2017-05-10",
            "list": [{"st": "00:00","ed" : "00:00", "fee": 7500}]
        }])},(res)=>{
            expect(res.errcode).to.be.equal(200)
            done()
        })
    })
})
//清空计费方式前，请检查对应的rt_id下是否有该种计费方式，数据请在initData.json中修改
//清空所有计费方式，不会写
describe('清空计费设置',()=>{
    it('清空某一天的计费设置',(done)=>{
        LIBrary.fetch('/room/fee'+'?rt_id='+COMMON.roomTypeId+'&md=day&day_or_holiday='+COMMON.feeDay+'&token='+COMMON.token,{method:'DELETE'},(res)=>{
            expect(res.errcode).to.be.equal(200)
            done()
        })
    })

    it('清空节假日的计费设置',(done)=>{
        LIBrary.fetch('/room/fee'+'?rt_id='+COMMON.roomTypeId+'&md=day&day_or_holiday='+COMMON.holDay+'&token='+COMMON.token,{method:'DELETE'},(res)=>{
            expect(res.errcode).to.be.equal(200)
            done()
        })
    })

    it('清空不存在的计费设置',(done)=>{
        LIBrary.fetch('/room/fee'+'?rt_id='+COMMON.roomTypeId+'&md=day&day_or_holiday='+COMMON.wrongDay+'&token='+COMMON.token,{method:'DELETE'},(res)=>{
            expect(res.errcode).to.be.equal(200)
            done()
        })
    })

    it('清空不存在的节假日的计费设置',(done)=>{
        LIBrary.fetch('/room/fee'+'?rt_id='+COMMON.roomTypeId+'&md=day&day_or_holiday='+COMMON.wrongHolDay+'&token='+COMMON.token,{method:'DELETE'},(res)=>{
            expect(res.errcode).to.be.equal(200)
            done()
        })
    })

    it('清空所有计费方式',(done)=>{
        LIBrary.fetch('/room/fee'+'?rt_id='+COMMON.roomTypeId+'&md=day&day_or_holiday='+COMMON.wrongHolDay+'&token='+COMMON.token,{method:'DELETE'},(res)=>{
            expect(res.errcode).to.be.equal(10001)
            done()
        })
    })
})

describe('删除单个计费方式',()=>{
    it('删除不存在的计费方式',(done)=>{
        LIBrary.fetch('/room/fee'+'?fee_id='+COMMON.feeId+'&token='+COMMON.token,{method:'DELETE'},(res)=>{
            expect(res.errcode).to.be.equal(200)
            done()
        })
    })

    it('删除不存在的计费方式',(done)=>{
        LIBrary.fetch('/room/fee'+'?fee_id='+199+'&token='+COMMON.token,{method:'DELETE'},(res)=>{
            expect(res.errcode).to.be.equal(200)
            done()
        })
    })
})
//数据请在initData.json中修改
describe('修改某条计费设置',()=>{
    it('数据不发生变化，修改失败',(done)=>{
        LIBrary.fetch('/room/fee'+'?token='+COMMON.token,{method:'PUT',body:JSON.stringify({fee_id: COMMON.feeId, st: COMMON.testSt, ed: COMMON.testEd, fee:COMMON.fee})},(res)=>{
            expect(res.errcode).to.be.equal(50001)
            done()
        })
    })

    it('数据发生变化，修改成功',(done)=>{
        LIBrary.fetch('/room/fee'+'?token='+COMMON.token,{method:'PUT',body:JSON.stringify({fee_id: COMMON.feeId, st: COMMON.testSt, ed: COMMON.testEd, fee: 85000})},(res)=>{
            expect(res.errcode).to.be.equal(200)
            done()
        })
    })
})

describe('获取计费设置',()=>{
    it('获取存在的日常计费设置',(done)=>{
        LIBrary.fetch('/room/fee?rt_id='+COMMON.roomTypeId+'&md=day&day_or_holiday='+COMMON.feeDay+'&token='+COMMON.token,{method:'GET'},(res)=>{
            expect(res.list.length).to.be.not.equal(0)
        done()
        })
    })

    it('获取不存在的节假日计费设置',(done)=>{
        LIBrary.fetch('/room/fee?rt_id='+COMMON.roomTypeId+'&md=holiday&day_or_holiday='+COMMON.wrongHolDay+'&token='+COMMON.token,{method:'GET'},(res)=>{
            expect(res.list.length).to.be.equal(0)
            // expect(res.errcode).to.be.equal(200)
            done()
        })
    })

})
