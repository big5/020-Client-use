import 'babel-polyfill'
import fetch from 'isomorphic-fetch'
import LIBrary from '../lib/util'
import {REQUEST_URLS} from '../../src/init_setup/settings.js'
import md5 from 'MD5'
const TESTCOMMON = require('../testDatas/initData.json')

describe('获取验证码', () => {
    it.skip('正确发送验证码', (done) => {
        LIBrary.fetch(REQUEST_URLS.getIdentify + '?phone=' + TESTCOMMON.testPhone, {
            method: 'GET'
        }, (res) => {
            expect(res.errcode).to.be.equal(200)
            done()
        })
    })
    
    it.skip('不能获取验证码', (done) => {
        LIBrary.fetch(REQUEST_URLS.getIdentify + '?phone=' + TESTCOMMON.testWrongPhone, {
            method: 'GET'
        }, (res) => {
            expect(res.errcode).to.be.equal(50001)
            done()
        })
    })

})
//短信验证部分建议单独进行测试
describe('短信发送', () => {
    it.skip('账号注册，匹配发送短信', (done) => {
        LIBrary.fetch(REQUEST_URLS.verifyIdengify + '?phone=' + TESTCOMMON.testPhone + '&code=' + TESTCOMMON.testCode, {
            method: 'GET'
        }, (res) => {
            expect(res.errcode).to.be.equal(200)
            done()
        })
    })
    it.skip('账号注册，不匹配发送短信', (done) => {
        LIBrary.fetch(REQUEST_URLS.verifyIdengify + '?phone=' + TESTCOMMON.testWrongPhone + '&code=' + TESTCOMMON.testCode, {
            method: 'GET'
        }, (res) => {
            expect(res.errcode).to.be.equal(50001)
            done()
        })
    })
})

//注册接口只对手机号进行了验证
describe('注册接口测试', () => {
    it('手机号已存在,失败', (done) => {
        LIBrary.fetch(REQUEST_URLS.register, {
            method: 'POST',
            body: JSON.stringify({
                phone: TESTCOMMON.testPhone,
                passwd: TESTCOMMON.testPassWord,
                name: TESTCOMMON.testStoreName,
                address: TESTCOMMON.testStoreAddress,
                code: TESTCOMMON.testCode,
                st: TESTCOMMON.testSt,
                ed: TESTCOMMON.testEd,
                manager: TESTCOMMON.testManager
            })
        }, (res) => {
            let data = res.errcode
            expect(data).to.be.equal(50001)
            done()
        })
    });
    //每次注册需要重新填写手机号
    it.skip('注册成功', (done) => {
        LIBrary.fetch(REQUEST_URLS.register, {
            method: 'POST',
            body: JSON.stringify({
                phone: TESTCOMMON.testPhone,
                passwd: TESTCOMMON.testPassWord,
                name: TESTCOMMON.testStoreName,
                address: TESTCOMMON.testStoreAddress,
                code: TESTCOMMON.testCode,
                st: TESTCOMMON.testSt,
                ed: TESTCOMMON.testEd,
                manager: TESTCOMMON.testManager
            })
        }, (res) => {
            let data = res.errcode
            expect(data).to.be.equal(200)
            done()
        })

    });
});

describe('登录接口测试', () => {
    it('账号密码正确', (done) => {
        LIBrary.fetch(REQUEST_URLS.login + '?phone=' + TESTCOMMON.testPhone + '&passwd=' + md5(TESTCOMMON.testPassWord), {
            method: 'GET'
        }, (res) => {
            let data = res.errcode
            expect(data).to.be.equal(200)
            done()
        })
    });
    it('手机号未注册', (done) => {
        LIBrary.fetch(REQUEST_URLS.login + '?phone=' + TESTCOMMON.testWrongPhone + '&passwd=' + md5(TESTCOMMON.testPassWord), {
            method: 'GET'
        }, (res) => {
            let data = res.errcode
            expect(data).to.be.equal(50001)
            done()
        })
    })
    it('手机号正确，密码错误', () => {
        LIBrary.fetch(REQUEST_URLS.login + '?phone=' + TESTCOMMON.testPhone + '&passwd=' + md5(TESTCOMMON.testWrongPwd), {
            method: 'GET'
        }, (res) => {
            let data = res.errcode
            expect(data).to.be.equal(50001)
        })
    })
    it('手机号、密码均错误', () => {
        LIBrary.fetch(REQUEST_URLS.login + '?phone=' + TESTCOMMON.testPhone + '&passwd=' + md5(TESTCOMMON.testWrongPwd), {
            method: 'GET'
        }, (res) => {
            let data = res.errcode
            expect(data).to.be.equal(50001)
        })
    })
});
//修改密码前需要调用发验证码接口发送验证码后，修改code
describe('修改密码', () => {
    it.skip('密码更改成功', (done) => {
        LIBrary.fetch(REQUEST_URLS.resetPwd, {
            method: 'POST',
            body: JSON.stringify({phone: TESTCOMMON.testPhone, passwd: TESTCOMMON.testWrongPwd, code: TESTCOMMON.testNewCode})
        }, (res) => {
            expect(res.errcode).to.be.equal(200)
            done()
        })
    })
    it('验证码错误，修改密码失败', () => {
        LIBrary.fetch(REQUEST_URLS.resetPwd, {
            method: 'POST',
            body: JSON.stringify({phone: TESTCOMMON.testPhone, passwd: TESTCOMMON.testWrongPwd, code: TESTCOMMON.testCode})
        }, (res) => {
            expect(res.errcode).to.be.equal(50001)
            done()
        })
    })

})
