import fetch from 'isomorphic-fetch'
import COMMON from './COMMON'
import eventproxy from 'eventproxy'
let LIBrary = LIBrary || {}
LIBrary.eventproxy = new eventproxy()

LIBrary.fetch = (reqUrl, args, callback, isCached = false) => {
    reqUrl += localStorage.getItem('token') ?
            (reqUrl.indexOf('?') > 0 ?
            '&token=' + localStorage.getItem('token') :
            '?token=' + localStorage.getItem('token')) : ''
    args['mode'] = 'cors'

    fetch(COMMON.URL + reqUrl, args).then((res) => {
        return res.json()
    }).then((data) => {
        callback(data)
    }).catch(err => {
        alert(err)
    })
}

LIBrary.times = (i, callback, l = i) => {
    if (i === 0) return
    callback(l - i)
    LIBrary.times(i - 1, callback, l)
}

LIBrary.removeByIndex = (arr, index) => {
    let item = []
    arr.map((value, i) => {
        if (i != index) {
            item.push(arr[i])
        }
    })
    return item
}

LIBrary.changeByIndex = (arr, index, val) => {
    let item = []
    arr.map((value, i) => {
        if (i == index) {
            arr[i] = val
        }
        item.push(arr[i])
    })
    return item
}

LIBrary.checkByValue = (arr, val) => {
    let flag = false
    arr.map((value, i) => {
        if (value = val) {
            flag = true
        }
    })
    return flag
}

LIBrary.creatArray = (len, num) => {
    let arr = [];
    for (let i = 0; i < len; i++) {
        arr.push(num)
    }
    return arr
}

//判断时间大小
LIBrary.exDateRange = (sDate1, sDate2) => {
    var iDateRange;
    if (sDate1 != "" && sDate2 != "") {
        var startDate = sDate1.replace(/-/g, "/");
        var endDate = sDate2.replace(/-/g, "/");
        var S_Date = new Date(Date.parse(startDate));
        var E_Date = new Date(Date.parse(endDate));
        iDateRange = (S_Date - E_Date) / 86400000;
    }
    return iDateRange;
}

LIBrary.warnning = (str) => {
    LIBrary.eventproxy.emit('warnMsg',str)
}

export default LIBrary
