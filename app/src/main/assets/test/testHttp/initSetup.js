import LIBrary from '../../src/testLIBs/util.js'
import {REQUEST_URLS} from '../../src/init_setup/settings.js'
import md5 from 'MD5'
const TESTCOMMON = require('./testData.json')

describe('登录接口测试', () => {
    it('账号密码正确', (done) => {
        LIBrary.fetch(REQUEST_URLS.login + '?phone=' + TESTCOMMON.testPhone + '&passwd=' + md5(TESTCOMMON.testPassWord), {
            method: 'GET'
        }, (res) => {
            let data = res.errcode
            expect(data).to.be.equal(200)
            done()
        })
    });
    it('电话号码未注册', (done) => {
        LIBrary.fetch(REQUEST_URLS.login + '?phone=' + TESTCOMMON.testWrongPhone + '&passwd=' + md5(TESTCOMMON.testPassWord), {
            method: 'GET'
        }, (res) => {
            let data = res.errcode
            expect(data).to.be.equal(50001)
            done()
        })
    })
    it('密码错误', (done) => {
        LIBrary.fetch(REQUEST_URLS.login + '?phone=' + TESTCOMMON.testWrongPhone + '&passwd=' + md5(TESTCOMMON.testWrongPwd), {
            method: 'GET'
        }, (res) => {
            let data = res.errcode
            expect(data).to.be.equal(50001)
            done()
        })
    })
});

describe('短信发送', () => {
    let flag = true
    if (flag) {
        return
    }
    it('账号注册，匹配发送短信', (done) => {
        LIBrary.fetch(REQUEST_URLS.verifyIdengify + '?phone=' + TESTCOMMON.testPhone, {
            method: 'GET'
        }, (res) => {
            if (typeof res == 'object') {
                expect(res.errcode).to.be.equal(200)
            }
            done()
        })
    })
})

describe('注册接口测试', () => {
    it('注册成功', () => {
        LIBrary.fetch(REQUEST_URLS.register, {
            method: 'POST',
            body: JSON.stringify({phone: TESTCOMMON.testPhone, passwd: TESTCOMMON.testPassWord, name: TESTCOMMON.testStoreName, address: TESTCOMMON.testStoreAddress, code: TESTCOMMON.testCode})
        }, (res) => {
            let data = res.errcode
            expect(data).to.be.equal(50001)
        })
    })
})
