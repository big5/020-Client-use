import React,{ Component } from 'react'
import { mount, render } from 'enzyme'
import TestUtils from 'react-addons-test-utils'

describe('<Days/>', () => {
    it('clear week', () => {
        const wrapper = mount( <Days/> )
        let clearButton = wrapper.find('button.clear');
        expect(clearButton.length).to.be.equal(1)
    })
})
