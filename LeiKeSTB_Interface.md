一：第三方广播

	Post  udp2007
	1）FIND_BOX_MESSAGE：通知机顶盒广播机顶盒信息


二：机顶盒广播信息

	Post  udp2007
	1）	LEIKE_OPEN_API FROM CLOUD12PLUS MAC:%s NO:%d ROOMNAME:%s udp_end;
		Mac:房台mac地址  01 02 03 04 06 07
		NO:组名
		Roomname：房台名字

	2）	LEIKE_OPEN_API FROM CLOUD12PLUS MAC:%s ISFANGTAI:%d REMAININGTIME:%d TYPE:%d
		Mac：机顶盒mac地址
		ISFANGTAI：是否购买了房台
		TYPE:开台类型
			TYPE == 3
				关机或者重启
			TYPE == 2
			一直开台；REMAININGTIME:已经开台的时间
		TYPE  == 0
			倒计时开台；REMAININGTIME:房台剩余时间


三：第三方发给机顶盒消息

    接口: encrypt_operation
    方法: HTTP+POST
    参数:
        ip: 房台IP
        id:操作ID(开台：056、续台：057、关台：058、关机：059、重启：060)
	*********************************************
	***********机顶盒需要开启房台功能************
	***注意事项：所有数据放到url中一起发送出去***
	*********************************************
    示例:
	1）开台
		http://192.168.1.100:2007/encrypt_operation?authority=2&passkey=&op=ID:056 LKCONTROLROOM_START TIME:%d TYPE:%d
		TYPE：开台类型
		（1）	倒计时开台LKCONTROLROOM_START TIME:开台时间 TYPE：1
		（2）	一直开台LKCONTROLROOM_START TIME:60 TYPE：2
	2）续台
		 http://192.168.1.100:2007/encrypt_operation?authority=2&passkey=&op=ID:057 LKCONTROLROOM_MORETIME TIME:%d
		   （1）LKCONTROLROOM_MORETIME TIME:续台时间
	3）关台
		"http://192.168.1.100:2007/encrypt_operation?authority=2&passkey=&op=ID:058 LKCONTROLROOM_CLOSEROOM"
	4）关机
		"http://192.168.1.100:2007/encrypt_operation?authority=2&passkey=&op=ID:059 LKCONTROLROOM_SHUTDOWN"
	5）重启
		"http://192.168.1.100:2007/encrypt_operation?authority=2&passkey=&op=ID:060 LKCONTROLROOM_RESTART"



    